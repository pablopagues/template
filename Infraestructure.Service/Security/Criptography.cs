﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infrastructure.Service.Security
{
  /// <summary>
  /// Classe usada para gerar Hashes SHA1 de senhas.
  /// </summary>
  public class Criptography
  {
    /// <summary>
    /// Valor randômico usado para embaralhar ainda mais os hashes gerados.
    /// </summary>
    public static string sal
    {
      get { return "Ê MACARENA!!1"; }
      
      private set { sal = value; }
    }

    /// <summary>
    /// Gera um hash do parâmetro de entrada.
    /// </summary>
    /// <param name="str">String da qual o hash será gerado</param>
    /// <returns>String contendo um hash codificado em Base64</returns>
    public static string HashCode(string str)
    {
      try
      {
        string rethash = "";

        // Instancia os codificadores
        System.Security.Cryptography.SHA1 hash = System.Security.Cryptography.SHA1.Create();
        System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();
        
        // Concatena o sal com a string a ser codificada
        byte[] combined = encoder.GetBytes(Criptography.sal + str);
        
        // Calcula o hash da string
        byte[] hashValue = hash.ComputeHash(combined);

        // Converte pra representação em hexadecimal
        foreach ( byte b in hashValue )
          rethash += String.Format("{0:x2}", b);

        // Retorna a string em hexa
        return rethash;
      }
      catch (Exception ex)
      {
        throw new Exception("Error in HashCode : " + ex.Message);
      }
    }

  }
}
