﻿using System;
using System.Drawing;
using System.Linq;
using Infrastructure.Service.Tools.Strings;

namespace Infrastructure.Service.Tools.Drawing
{
    public class ColorProvider
    {
        public static Color GetColor(string nome)
        {
            Color cor;

            try
            {
                nome = nome.RemoverAcentos();

                char x = nome.ToUpper().ElementAt(0);
                char y = nome.ToUpper().ElementAt(1);
                char z = nome.ToUpper().ElementAt(2);

                int r = ((x - 64) * 10) - 5;
                int g = ((y - 64) * 10) - 5;
                int b = ((z - 64) * 10) - 5;

                cor = Color.FromArgb(r, g, b);
            }
            catch (Exception e)
            {
                cor = Color.Black;
            }

            return cor;
        }

    }
}
