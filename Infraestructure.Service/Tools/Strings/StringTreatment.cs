﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Service.Tools.Strings
{
    public static class StringTreatment
    {
        /// <summary>
        /// Remove acentos presentes numa string. Útil para padronização de dados
        /// importados.
        /// </summary>
        /// <param name="texto">Texto contendo acentos</param>
        /// <returns>Texto sem acentos</returns>
        public static string RemoverAcentos(this string texto)
        {
            string s = texto.Normalize(NormalizationForm.FormD);

            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(s[k]);
                }
            }
            return sb.ToString();
        }
    }
}
