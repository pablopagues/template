﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using CrystalDecisions.Shared;
using System.Data;
using System.Collections;
using System.Diagnostics;
using Infrastructure.Service.Controllers.Providers.Interfaces;

namespace Infrastructure.Service.Controllers.Results
{
  /// <summary>
  /// Retorna um relatório Crystal Report em formato PDF para o usuário
  /// </summary>
  public class PdfReportResult : ViewResult
  {
    #region Classes auxiliares de população de dados dos relatórios
    /// <summary>
    /// Interface para popular os relatórios com os dados
    /// </summary>
    private interface IReportData
    {
        void fillReport(ReportDocument document);
    }

    private class ReportDataset : IReportData
    {
        DataSet dataset;
        IEnumerable enumerable;
        IDictionary<string, object> parameters;

        public ReportDataset(DataSet dataset, IDictionary<string, object> parameters)
        {
            this.dataset = dataset;
            this.parameters = parameters ?? new Dictionary<string, object>();
        }

        public ReportDataset(IEnumerable enumerable, IDictionary<string, object> parameters)
        {
            this.enumerable = enumerable;
            this.parameters = parameters ?? new Dictionary<string, object>();
        }

        public void fillReport(ReportDocument document)
        {
            if (enumerable != null && dataset == null)
            {
                document.SetDataSource(enumerable);
                return;
            }

            document.SetDataSource(dataset);

            // Definimos os parametros do relatório, se houver
            foreach (var parametro in parameters)
                document.SetParameterValue(parametro.Key, parametro.Value);
        }
    }

    private class SubReportDatasets : IReportData
    {
        IDictionary<string, DataSet> datasets;
        IDictionary<string, IEnumerable> enumerables;
        IDictionary<string, IDictionary<string, object>> parameters;

        public SubReportDatasets(IDictionary<string, DataSet> datasets, IDictionary<string, IDictionary<string, object>> parameters)
        {
            this.datasets = datasets;
            this.parameters = parameters ?? new Dictionary<string, IDictionary<string, object>>();
        }

        public SubReportDatasets(IDictionary<string, IEnumerable> enumerables, IDictionary<string, IDictionary<string, object>> parameters)
        {
            this.enumerables = enumerables;
            this.parameters = parameters ?? new Dictionary<string, IDictionary<string, object>>();
        }

        private void fillReport<TData>(
            ReportDocument document,
            IDictionary<string, TData> data,
            IDictionary<string, IDictionary<string, object>> parameters)
        {
            foreach (KeyValuePair<string, TData> subDataset in data)
                document.Subreports[subDataset.Key].SetDataSource(subDataset.Value);

            // Definimos os parametros dos sub-relatórios, se houver
            foreach (var subReportParameter in parameters)
            {
                if (subReportParameter.Value != null)
                {
                    foreach (var parametro in subReportParameter.Value)
                        document.Subreports[subReportParameter.Key].SetParameterValue(parametro.Key, parametro.Value);
                }
            }
        }

        public void fillReport(ReportDocument document)
        {
            if (datasets != null)
            {
                fillReport(document, datasets, parameters);
                return;
            }

            fillReport(document, enumerables, parameters);
        }
    }

    #endregion

    /// <summary>
    /// Nome do arquivo Crystal Report
    /// </summary>
    string reportFileName { get; set; }

    /// <summary>
    /// Classe que encapsula os dados do relatório.
    /// </summary>
    IReportData reportData { get; set; }

    /// <summary>
    /// Classe que encapsula os dados dos sub-relatórios.
    /// </summary>
    IReportData subReportsData { get; set; }

    /// <summary>
    /// Nome do arquivo a ser retornado para o usuário.
    /// </summary>
    public string returnFilename { get; set; }

    /// <summary>
    /// Mapeador de caminhos
    /// </summary>
    IPathMapper pathMapper { get; set; }

    /// <summary>
    /// Este tipo de resultado retorna um relatório Crystal Report em formato PDF para o usuário
    /// </summary>
    /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
    /// <param name="reportDataSet">Dataset usado pelo relatório</param>
    public PdfReportResult(IPathMapper pathMapper, string reportFileName, DataSet reportDataSet)
        : this(pathMapper, reportFileName, reportDataSet, null)
    { }

    /// <summary>
    /// Este tipo de resultado retorna um relatório Crystal Report em formato PDF para o usuário
    /// </summary>
    /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
    /// <param name="reportDataSet">Dataset usado pelo relatório</param>
    /// <param name="parameters">Parametros usados pelo relatório</param>
    public PdfReportResult(IPathMapper pathMapper, string reportFileName, DataSet reportDataSet, IDictionary<string, object> parameters)
	{
      if (string.IsNullOrEmpty(reportFileName))
        throw new ArgumentException("Relatório não informado", "reportFileName");

      if (reportDataSet == null)
        throw new ArgumentException("DataSet não informado", "reportDataSet");

      this.reportData = new ReportDataset(reportDataSet, parameters);
      this.reportFileName = reportFileName;
      this.pathMapper = pathMapper;
	}

    /// <summary>
    /// Este tipo de resultado retorna um relatório Crystal Report em formato PDF para o usuário
    /// </summary>
    /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
    /// <param name="reportEnumerable">Dataset usado pelo relatório</param>
    public PdfReportResult(IPathMapper pathMapper, string reportFileName, IEnumerable reportEnumerable)
      : this(pathMapper, reportFileName, reportEnumerable, null)
    { }

    /// <summary>
    /// Este tipo de resultado retorna um relatório Crystal Report em formato PDF para o usuário
    /// </summary>
    /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
    /// <param name="reportDataSet">Dataset usado pelo relatório</param>
    /// <param name="parameters">Parametros usados pelo relatório</param>
    public PdfReportResult(IPathMapper pathMapper, string reportFileName, IEnumerable reportEnumerable, IDictionary<string, object> parameters)
    {
        if (string.IsNullOrEmpty(reportFileName))
            throw new ArgumentException("Relatório não informado", "reportFileName");

        if (reportEnumerable == null)
            throw new ArgumentException("DataSet não informado", "reportEnumerable");

        this.reportData = new ReportDataset(reportEnumerable, parameters);
        this.reportFileName = reportFileName;
        this.pathMapper = pathMapper;
    }

    
    public PdfReportResult(IPathMapper pathMapper,
        string reportFileName, 
        IEnumerable reportEnumerable, 
        IDictionary<string, object> parameters, 
        IDictionary<string, IEnumerable> subReportsEnumerables)
        : this(pathMapper, reportFileName, reportEnumerable, parameters, subReportsEnumerables, null)
    { }

    
    public PdfReportResult(IPathMapper pathMapper,
        string reportFileName, 
        IEnumerable reportEnumerable, 
        IDictionary<string, object> parameters, 
        IDictionary<string, IEnumerable> subReportsEnumerables, 
        IDictionary<string, IDictionary<string, object>> subReportsParameters)
    {
        if (string.IsNullOrEmpty(reportFileName))
            throw new ArgumentException("Relatório não informado", "reportFileName");

        if (reportEnumerable == null)
            throw new ArgumentException("DataSet não informado", "reportEnumerable");

        this.reportFileName = reportFileName;
        this.reportData = new ReportDataset(reportEnumerable, parameters);
        this.subReportsData = new SubReportDatasets(subReportsEnumerables, subReportsParameters);
        this.pathMapper = pathMapper;
    }

    public PdfReportResult(IPathMapper pathMapper,
      string reportFileName,
      DataSet reportDataset,
      IDictionary<string, object> parameters,
      IDictionary<string, DataSet> subReportsDatasets)
        : this(pathMapper, reportFileName, reportDataset, parameters, subReportsDatasets, null)
    { }


    public PdfReportResult(IPathMapper pathMapper,
        string reportFileName,
        DataSet reportDataset,
        IDictionary<string, object> parameters,
        IDictionary<string, DataSet> subReportsDatasets,
        IDictionary<string, IDictionary<string, object>> subReportsParameters)
    {
        if (string.IsNullOrEmpty(reportFileName))
            throw new ArgumentException("Relatório não informado", "reportFileName");

        if (reportDataset == null)
            throw new ArgumentException("DataSet não informado", "reportDataset");

        this.reportFileName = reportFileName;
        this.reportData = new ReportDataset(reportDataset, parameters);
        this.subReportsData = new SubReportDatasets(subReportsDatasets, subReportsParameters);
        this.pathMapper = pathMapper;
    }

    public MemoryStream geraRelatorio(ControllerContext context)
    {
        if (context == null)
            throw new ArgumentNullException("context");

        if (string.IsNullOrEmpty(this.ViewName))
            this.ViewName = context.RouteData.GetRequiredString("action");

        if (this.returnFilename == null)
            this.returnFilename = reportFileName.Split('.').ElementAt(0);

        // Fazemos um alias pro código ficar mais bonito
        HttpResponseBase response = context.HttpContext.Response;

        // Geramos o relatório!
        ReportDocument relatorio = new ReportDocument();

        string controllerNamespace = context.Controller.GetType().Namespace;
        string filename = pathMapper.MapPath("~/Reports/" + reportFileName);
        string filenameInArea = string.Empty;

        if (controllerNamespace.Contains("Areas"))
        {
            string areaName = controllerNamespace.Split('.').ElementAt(2);
            filenameInArea = pathMapper.MapPath(string.Format("~/Areas/{0}/Reports/{1}", areaName, reportFileName));
        }

        try
        {
            relatorio.Load(filename); // Carregamos o relatório informado
        }
        catch (Exception)
        {
            relatorio.Load(filenameInArea); // Carregamos da pasta das areas
        }

        if (reportData != null)
            reportData.fillReport(relatorio); // Passamos o dataset e parametros para o relatório pai

        if (subReportsData != null)
            subReportsData.fillReport(relatorio); // Passamos os datasets e parametros para os sub-reports

        Stream reportStream = relatorio.ExportToStream(ExportFormatType.PortableDocFormat);
        MemoryStream reportMemoryStream = new MemoryStream();
        reportStream.CopyTo(reportMemoryStream);

        return reportMemoryStream;

        //return relatorio.ExportToStream(ExportFormatType.PortableDocFormat) as MemoryStream;
    }

    /// <summary>
    /// Executa o retorno
    /// </summary>
    /// <param name="context">Contexto do controlador que chamou este tipo de retorno</param>
    public override void ExecuteResult(ControllerContext context)
    {
        // Geramos o PDF e enviamos ao usuário
        using (MemoryStream oStream = geraRelatorio(context))
        {
            // Fazemos um alias pro código ficar mais bonito
            HttpResponseBase response = context.HttpContext.Response;

            response.Clear();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment; filename=" + returnFilename + ".pdf");
            response.ContentType = "application/pdf";
            response.BinaryWrite(oStream.ToArray());
            response.End();
        }
    }
  }
}
