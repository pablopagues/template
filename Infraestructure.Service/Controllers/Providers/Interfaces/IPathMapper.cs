﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Service.Controllers.Providers.Interfaces
{
    public interface IPathMapper
    {
        string MapPath(string relativePath);
    }
}
