﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Service.Controllers.Providers.Interfaces;

namespace Infrastructure.Service.Controllers.Providers
{
    public class DummyPathMapper : IPathMapper
    {
        string basePath;

        public DummyPathMapper(string basePath)
        {
            this.basePath = basePath;
        }

        public string MapPath(string relativePath)
        {
            return basePath + relativePath.Replace("~", "").Replace("/", @"\");
        }
    }
}
