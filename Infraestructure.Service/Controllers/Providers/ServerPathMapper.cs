﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Infrastructure.Service.Controllers.Providers.Interfaces;

namespace Infrastructure.Service.Controllers.Providers
{
    public class ServerPathMapper : IPathMapper
    {
        public string MapPath(string relativePath)
        {
            return HttpContext.Current.Server.MapPath(relativePath);
        }
    }
}
