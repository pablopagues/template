﻿using System;
using System.Linq;
using System.Web.Security;

namespace Infrastructure.Data.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            using (var usersContext = new MVCContext())
            {
                var user = usersContext.Usuarios.SingleOrDefault(u => u.login == username);
                if (user == null)
                    return false;
                return user.papeis.Where(p => p.nome.ToLower() == roleName.ToLower()).Count() > 0;
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            try
            {

                using (var usersContext = new MVCContext())
                {
                    var user = usersContext.Usuarios.Include("Papeis").SingleOrDefault(u => u.login == username);
                    //user.
                    if (user == null)
                        return new string[] { };
                    return user.papeis == null ? new string[] { } :
                      user.papeis.Select(x => x.nome ).ToArray();
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        // -- Snip --

        public override String[] GetAllRoles()
        {
            using (var usersContext = new MVCContext())
            {
                return usersContext.Papeis.Select(p=>p.nome).ToArray();
            }
        }

        // -- Snip --

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }

}
