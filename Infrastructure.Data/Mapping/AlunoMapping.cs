﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class AlunoMapping : EntityTypeConfiguration<Aluno>, IEntityConfiguration
    {
        public AlunoMapping()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("Aluno");

            Property(p => p.celular).HasMaxLength(30);
            Property(p => p.celularMae).HasMaxLength(50);
            Property(p => p.celularPai).HasMaxLength(50);
            Property(p => p.disciplinas).HasMaxLength(50);
            Property(p => p.dtInicioAulas).IsRequired();
            Property(p => p.dtNascimento).IsRequired();
            Property(p => p.dtVencAulas).IsRequired();
            Property(p => p.email).HasMaxLength(50);
            Property(p => p.endereco).HasMaxLength(50);
            Property(p => p.escola).HasMaxLength(50);
            Property(p => p.nome).HasMaxLength(50);
            Property(p => p.nomeMae).HasMaxLength(50);
            Property(p => p.nomePai).HasMaxLength(50);
            Property(p => p.observacao).HasMaxLength(250);
            Property(p => p.serie).HasMaxLength(50);
            Property(p => p.telefone).HasMaxLength(50);
        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
