﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class ContratoMapping : EntityTypeConfiguration<Contrato>, IEntityConfiguration
    {
        public ContratoMapping()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("Contrato");

            Property(p => p.Nome).HasMaxLength(30);
            Property(p => p.Valor);
            Property(p => p.ValorParticular);
            Property(p => p.ValorReforço);

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
