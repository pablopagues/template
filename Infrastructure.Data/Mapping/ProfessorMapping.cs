﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;


namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class ProfessorMapping : EntityTypeConfiguration<Professor>, IEntityConfiguration
    {
        public ProfessorMapping()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("Professor");

            Property(p => p.Celular).HasMaxLength(30);
            Property(p => p.DataNascimento);
            Property(p => p.email).HasMaxLength(50);
            Property(p => p.Endereco).HasMaxLength(50);
            Property(p => p.Login).HasMaxLength(30).IsRequired();
            Property(p => p.Nome).HasMaxLength(50).IsRequired();
            Property(p => p.Telefone).HasMaxLength(30);
            Property(p => p.ValorAula);

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
