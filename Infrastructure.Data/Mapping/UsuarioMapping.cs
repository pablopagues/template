﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class UsuarioMap : EntityTypeConfiguration<Usuario>, IEntityConfiguration
    {
        public UsuarioMap()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("Usuario");
            Property(p => p.login).HasMaxLength(20).IsRequired();
            Property(p => p.dataCriacao).IsRequired();
            Property(p => p.estaBloqueado);
            Property(p => p.estaOnline);
            Property(p => p.ultimoLogin);

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }
    }
}
