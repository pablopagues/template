﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class ContasReceberMapping : EntityTypeConfiguration<ContasReceber>, IEntityConfiguration
    {
        public ContasReceberMapping()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("ContasReceber");

            Property(p => p.Descricao).HasMaxLength(30);
            Property(p => p.Montante);
            Property(p => p.TipoMovimento).HasMaxLength(20);
            Property(p => p.Data);

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
