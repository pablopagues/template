﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class FaturamentoAlunoMapping : EntityTypeConfiguration<FaturamentoAluno>, IEntityConfiguration
    {
        public FaturamentoAlunoMapping()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);

            ToTable("FaturamentoAluno");

            Property(p => p.Data);
            Property(p => p.Descricao).HasMaxLength(50).IsRequired(); ;
            Property(p => p.QtdHoras).IsRequired();
            Property(p => p.ValorHora).IsRequired();

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
