﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class LiquidacaoProfessoresMapping : EntityTypeConfiguration<LiquidacaoProfessores>, IEntityConfiguration
    {
        public LiquidacaoProfessoresMapping()
        {
            Property(p => p.ID).IsRequired();
            HasKey(p => p.ID);

            ToTable("LiquidacaoProfessores");

            //Property(p => p.DataMovimento);
            //Property(p => p.Descricao).HasMaxLength(50).IsRequired(); ;
            //Property(p => p.Grupo).IsRequired();
            //Property(p => p.HoraFinal).IsRequired();
            //Property(p => p.HoraInicio).IsRequired();
            //Property(p => p.QtdHoras).IsRequired();

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
