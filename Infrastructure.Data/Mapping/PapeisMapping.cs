﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models;
using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.Mapping
{
    [Export(typeof(IEntityConfiguration))]
    public class PapelMap : EntityTypeConfiguration<Papel>, IEntityConfiguration
    {
        public PapelMap()
        {
            Property(p => p.id).IsRequired();
            HasKey(p => p.id);
            
            ToTable("Papel");
            Property(p => p.nome).HasMaxLength(20).IsRequired();

        }

        public void AddConfiguration(System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }
    }
}
