﻿using Infrastructure.Data.Contratos;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Models.Escola;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;

namespace Infrastructure.Data
{
    public class MVCContext : DbContext
    {
        public DbSet<Papel> Papeis { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        //Escola
        public DbSet<Aluno> Aluno { get; set; }
        public DbSet<Contrato> Contrato { get; set; }
        public DbSet<MovimentoAula> MovimentoAula { get; set; }
        public DbSet<Parametros> Parametros { get; set; }
        public DbSet<Professor> Professor { get; set; }
        public DbSet<AgendaAula> AgendaAula { get; set; }
        public DbSet<FaturamentoAluno> FaturamentoAluno { get; set; }
        public DbSet<LiquidacaoProfessores> LiquidacaoProfessores { get; set; }

        public DbSet<TipoDespesa> TipoDespesa { get; set; }
        public DbSet<ContasPagar> ContasPagar { get; set; }
        public DbSet<ContasReceber> ContasReceber { get; set; }

        public MVCContext()
            : base("ConnectionString")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            var contextConfiguration = new ContextConfiguration();
            var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            var container = new CompositionContainer(catalog);
            container.ComposeParts(contextConfiguration);

            foreach (var configuration in contextConfiguration.Configurations)
            {
                configuration.AddConfiguration(modelBuilder.Configurations);
            }

            modelBuilder.Entity<Papel>()
                .HasMany(q => q.usuarios)
                .WithMany(p => p.papeis)
                .Map(x =>
                {
                    x.ToTable("UsuarioPapel");
                    x.MapLeftKey("IdPapel");
                    x.MapRightKey("IdUsuario");
                });



            //modelBuilder.Entity<Usuario>()
            //.HasOptional(f => f.professor)
            //.WithRequired(s => s.Usuario);

            base.OnModelCreating(modelBuilder);
        }
    }
}
