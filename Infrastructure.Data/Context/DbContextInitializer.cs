﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Domain;
using System.Data.Entity;
using Infrastructure.Data;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Models.Escola;

namespace Infrastructure.Domain
{
    /// <summary>
    /// Seeding data
    /// </summary>
    public class DBContextInitializer : DropCreateDatabaseAlways<MVCContext>
    {
        protected override void Seed(MVCContext context)
        {
            CreatePapel(context);
            CreateProfessor(context);

            base.Seed(context);
        }

        private void CreatePapel(MVCContext context)
        {
            context.Papeis.Add(new Papel { nome = "Administrador" });            
            context.Papeis.Add(new Papel { nome = "Usuario" });
            context.SaveChanges();
        }

        private void CreateProfessor(MVCContext context)
        {
            Professor prof = new Professor();
            prof.ValorAula = 0;
            prof.DataNascimento = DateTime.Now;
            prof.email = "pablo@gmail.com";
            prof.Nome = "pablo";
            prof.Login = "pablo";

            context.Professor.Add(prof);
            context.SaveChanges();
        }
    }
}

