﻿using System.Data.Entity.ModelConfiguration.Configuration;

namespace Infrastructure.Data.Contratos
{
    public interface IEntityConfiguration
    {
        void AddConfiguration(ConfigurationRegistrar registrar);
    }
}
