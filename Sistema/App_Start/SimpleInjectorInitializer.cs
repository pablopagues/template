[assembly: WebActivator.PostApplicationStartMethod(typeof(Sistema.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace Sistema.App_Start
{
    using Infrastructure.Data;
    using Infrastructure.Data.GenericRepository;
    using Infrastructure.Domain.Base.GenericRepository;
    using Infrastructure.Domain.Repositories;
    using SimpleInjector;
    using SimpleInjector.Integration.Web.Mvc;
    using System.Data.Entity;
    using System.Reflection;
    using System.Web.Mvc;

    public static class SimpleInjectorInitializer
    {
        public static void Initialize()
        {
            var container = new Container();
            container.Register<MVCContext>(() => new MVCContext());
            InitializeContainer(container);
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            DbContext _context = DependencyResolver.Current.GetService<MVCContext>();

            GenericRepository genericRepository = new GenericRepository(_context);

            container.RegisterWithContext<IGenericRepository>(dependencyContext =>
            {
                return new GenericRepository(_context);
            });
            container.RegisterWithContext<IPapelRepositorio>(dependencyContext =>
            {
                return new PapelRepositorio(genericRepository);
            });
            container.RegisterWithContext<IUsuarioRepositorio>(dependencyContext =>
            {
                return new UsuarioRepositorio(genericRepository);
            });

        }
    }
}