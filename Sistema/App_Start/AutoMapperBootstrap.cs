﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AutoMapper;
using System.Diagnostics;

namespace Sistema.App_Start
{
  public class AutoMapperBootstrap
  {
    /// <summary>
    /// Inicializa o processo de mapeamento varrendo o namespace das classes de profile.
    /// </summary>
    public static void Init(Assembly clientMvcApplication)
    {
      Mapper.Initialize(x => GetConfiguration(Mapper.Configuration, clientMvcApplication));
    }

    /// <summary>
    /// Varre o namespace das classes de profile (mapeamentos do AutoMapper) e
    /// as executa para realizar o mapeamento.
    /// </summary>
    /// <param name="configuration">Instância da configuração do AutoMapper</param>
    private static void GetConfiguration(IConfiguration configuration, Assembly clientMvcApplication)
    {
      // Recupero todos os profiles
      IEnumerable<Type> profiles = from t in clientMvcApplication.GetTypes()
            where t.IsClass && t.BaseType == typeof(Profile)
            select t;

      // Adiciono cada um a configuração
      foreach (Type profile in profiles)
      {
        configuration.AddProfile(Activator.CreateInstance(profile) as Profile);
        Debug.WriteLine("Profile carregado: " + profile.Name);
      }
    }

  }
}

