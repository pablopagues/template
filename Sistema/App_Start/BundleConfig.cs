﻿using System.Web;   
using System.Web.Optimization;

namespace Sistema
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //SCRIPTS
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/fullcalendar/lib/jquery.js",
                //"~/Scripts/jquery/jquery-{version}.js",
                "~/Scripts/jquery.validation/jquery.validate*",
                "~/Scripts/jquery.tmpl.js",
                "~/Scripts/jquery.lazy.js",
                "~/Scripts/select2/jquery.select2.js",
                "~/Scripts/jquery.timeago.js",
                "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/fullcalendar/lib/moment.js",
                "~/Scripts/fullcalendar/fullcalendar.js",
                "~/Scripts/fullcalendar/lang/pt-br.js",
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap/bootstrap.js",
                "~/Scripts/bootstrap/bootstrap-datetimepicker.js",
                "~/Scripts/bootstrap/bootstrap-datepicker.js",
                "~/Scripts/bootstrap/bootstrap-switch.js",
                "~/Scripts/knockoutjs/knockout-{version}.js",
                "~/Scripts/application.js"));

            //STYLES
            bundles.Add(new StyleBundle("~/Content/style").Include(
                "~/Content/css/font-awesome.css",
                "~/Content/css/bootstrap/bootstrap.css",
                "~/Content/css/bootstrap/bootstrap-switch.css",
                "~/Content/css/bootstrap/bootstrap-datepicker.css",
                "~/Content/css/bootstrap/bootstrap-datetimepicker.css",
                "~/Content/css/select2/select2.css",
                "~/Content/css/select2/select2-bootstrap.css",
                "~/Content/css/fullcalendar.css",
                //"~/Content/css/fullcalendar.print.css",
                "~/Content/css/application.css",
                "~/Content/css/site.css"));
        }
    }
}