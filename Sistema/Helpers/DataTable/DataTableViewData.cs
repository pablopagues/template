﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Classe a ser repassada com todos os dados necessários à renderização da DataTable na View
    /// </summary>
    public class DataTableViewData<TModel> where TModel : class
    {
        public DataTableViewData()
        {
            renderProperties = new Dictionary<string, object>();
        }

        /// <summary>
        /// Nome desse DataTable. Usado para gerar a URL de post quando existem varios
        /// datatables numa mesma página.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Todas as colunas de dados (não contem agrupadoras)
        /// </summary>
        public IList<DataTableColumn<TModel>> columns { get; set; }

        /// <summary>
        /// Colunas agrupadoras, ou seja, que agrupam outras colunas
        /// </summary>
        public IList<DataTableGroupColumn<TModel>> groupColumns { get; set; }

        /// <summary>
        /// Lista de colunas para facilitar a renderização.
        /// Contém somente colunas simples (DataTableColumn) e colunas agrupadas 
        /// (DataTableGroupColumn). Isto é feito para facilitar a renderização 
        /// da tabela com colunas aninhadas na página Html.
        /// </summary>
        public IList<DataTableGenericColumn> columnsList { get; set; }

        /// <summary>
        /// Ações a serem mostradas na tabela. Por padrão, todas as ações
        /// (editar, detalhes, apagar) são exibidas.
        /// </summary>
        public IList<DataTableRowAction.Default> actionsShowed { get; set; }

        /// <summary>
        /// Ações customizadas pelo usuário mostradas na coluna de ações.
        /// Por padrão são exibidas depois das ações padrão.
        /// </summary>
        public IList<DataTableRowAction> customActions { get; set; }

        /// <summary>
        /// Variável de estado que indica se a filtragem individual de colunas
        /// será usada. Default = false;
        /// </summary>
        public bool withColumnFilter { get; set; }

        /// <summary>
        /// Configurações adicionais de renderização do DataTables
        /// </summary>
        public IDictionary<string, object> renderProperties { get; set; }
    }
}
