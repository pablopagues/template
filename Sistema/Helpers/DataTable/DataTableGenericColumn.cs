﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Interface para colunas do DataTable
    /// </summary>
    public abstract class DataTableGenericColumn
    {
        /// <summary>
        /// Rótulo da coluna
        /// </summary>
        public string label { get; set; }
    }
}
