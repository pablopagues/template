﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace SAAR.Config.DataTable
{
    /// <summary>
    /// Classe usada para facilitar o gerenciamento das colunas do DataTable
    /// </summary>
    /// <typeparam name="TModel">Modelo a ser usado</typeparam>
    public class DataTable<TModel> where TModel : class
    {
        /// <summary>
        /// Nome do identificador a ser usado na geração das rotas
        /// </summary>
        private string nomeIdentificador { get; set; }

        /// <summary>
        /// Colunas a serem repassadas para o DataTable cliente
        /// </summary>
        private IList<DataTableColumn<TModel>> columns { get; set; }

        /// <summary>
        /// Colunas agrupadoras, ou seja, que agrupam outras colunas
        /// </summary>
        private IList<DataTableGroupColumn<TModel>> groupColumns { get; set; }

        /// <summary>
        /// Lista de colunas gerais. Contém somente colunas simples (DataTableColumn) e
        /// colunas agrupadas (DataTableGroupColumn). Isto é feito para facilitar
        /// a renderização da tabela com colunas aninhadas na página Html.
        /// </summary>
        private IList<DataTableGenericColumn> columnsList { get; set; }

        /// <summary>
        /// Variável de estado para informar se um novo agrupamento foi iniciado
        /// </summary>
        private bool startedGrouping { get; set; }

        /// <summary>
        /// Ações a serem mostradas na tabela. Por padrão, todas as ações
        /// (editar, detalhes, apagar) são exibidas.
        /// </summary>
        private IList<DataTableRowAction.Default> actionsShowed { get; set; }

        /// <summary>
        /// Condição de exibição das ações customizadas. Cada ação tem sua condição
        /// </summary>
        private IDictionary<DataTableRowAction.Default, Func<TModel, bool>> actionsShowedCondition { get; set; }

        /// <summary>
        /// Ações customizadas pelo usuário mostradas na coluna de ações.
        /// Por padrão são exibidas depois das ações padrão.
        /// </summary>
        private IList<DataTableRowAction> customActions { get; set; }

        /// <summary>
        /// Condição de exibição das ações customizadas. Cada ação tem sua condição
        /// </summary>
        private IDictionary<DataTableRowAction, Func<TModel, bool>> customActionsShowedCondition { get; set; }

        public enum CustomActionsPosition
        {
            BEFORE, AFTER
        }

        /// <summary>
        /// Ações customizadas pelo usuário mostradas na coluna de ações.
        /// Apos as acoes padroes (Default)
        /// </summary>
        [Obsolete("Please use the method version to set this value, instead.", true)]
        public CustomActionsPosition customActionsPosition { get; set; }

        private CustomActionsPosition _customActionsPosition { get; set; }

        /// <summary>
        /// Variável de estado que indica se a filtragem individual de colunas
        /// será usada. Default = false;
        /// </summary>
        private bool withColumnFilter { get; set; }

        /// <summary>
        /// Cria um novo objeto que se encarregará de montar a tabela de dados a ser enviada
        /// para o DataTable no cliente.
        /// </summary>
        public DataTable() : this("id") { }

        /// <summary>
        /// Lista de todos os registros do datatables obtida atraves do método getAllRecords()
        /// </summary>
        private IQueryable<TModel> records { get; set; }

        /// <summary>
        /// Cria um novo objeto que se encarregará de montar a tabela de dados a ser enviada
        /// para o DataTable no cliente.
        /// </summary>
        /// <param name="nomeIdentificador">Nome do parametro identificador a ser usado para gerar os links das ações</param>
        public DataTable(string nomeIdentificador)
        {
            columns = new List<DataTableColumn<TModel>>();
            columnsList = new List<DataTableGenericColumn>();
            groupColumns = new List<DataTableGroupColumn<TModel>>();

            actionsShowed = new List<DataTableRowAction.Default>();
            actionsShowedCondition = new Dictionary<DataTableRowAction.Default, Func<TModel, bool>>();

            actionsShowed.Add(DataTableRowAction.Default.EDIT);
            actionsShowed.Add(DataTableRowAction.Default.DETAILS);
            actionsShowed.Add(DataTableRowAction.Default.DELETE);

            actionsShowedCondition.Add(DataTableRowAction.Default.EDIT, x => false);
            actionsShowedCondition.Add(DataTableRowAction.Default.DETAILS, x => false);
            actionsShowedCondition.Add(DataTableRowAction.Default.DELETE, x => false);

            customActions = new List<DataTableRowAction>();
            _customActionsPosition = CustomActionsPosition.AFTER;
            customActionsShowedCondition = new Dictionary<DataTableRowAction, Func<TModel, bool>>();

            startedGrouping = false;
            withColumnFilter = false;

            this.nomeIdentificador = nomeIdentificador;
        }

        /// <summary>
        /// Remove o nome do objeto da função lambda e retorna somente o nome do membro informado
        /// </summary>
        /// <param name="memberExpression">Função lambda de acesso ao membro da classe</param>
        /// <returns>Nome do membro sem identificador do objeto</returns>
        /// <example>
        /// removeObjectName("x.municipio.regional.nome") retornará "municipio.regional.nome"
        /// </example>
        private string removeObjectName(Expression<Func<TModel, object>> memberExpression)
        {
            string fullMemberName = "";

            if (memberExpression.Body is UnaryExpression)
            {
                fullMemberName = ((UnaryExpression)memberExpression.Body).Operand.ToString();
            }
            else if (memberExpression.Body is MemberExpression)
            {
                fullMemberName = memberExpression.ToString();
            }

            string[] nomes = fullMemberName.Split('.');
            int identificadores = nomes.Count();

            string nomeSemObjeto = "";

            for (int i = 1; i < identificadores; i++)
            {
                if (i == identificadores - 1)
                    nomeSemObjeto += nomes[i];
                else
                    nomeSemObjeto += nomes[i] + ".";
            }

            return nomeSemObjeto;
        }

        /// <summary>
        /// Gera os links das ações para a tabela de listagem de dados
        /// </summary>
        /// <param name="controllerContext">Contexto do controller</param>
        /// <param name="id">Id do registro</param>
        /// <param name="data">Registro</param>
        /// <returns>String contendo os links de editar, detalhes e apagar</returns>
        private string generateActionLinks(ControllerContext controllerContext, int id, TModel data)
        {
            // Pegamos o nome do controlador para montar os links
            string controllerName = controllerContext.RequestContext.RouteData.GetRequiredString("Controller");

            // Criamos um dicionário para uso na geração de parametros HTML, caso seja aplicável...


            // Criamos um dicionário de rotas usando os parametros acima (sim, eu sei, parece redundante, mas o método pede :P)
            RouteValueDictionary rotasParametros = new RouteValueDictionary();
            rotasParametros.Add(nomeIdentificador, id);

            if (controllerContext.RouteData.DataTokens["area"] != null)
                rotasParametros.Add("area", controllerContext.RouteData.DataTokens["area"]);

            // Ação de detalhes
            IDictionary<string, object> detalhesParametros = new Dictionary<string, object>()
                {
                    {"class", "btn btn-xs btn-default"},
                    {"title", NavigationHelper.detalhesTitle}
                };

            string detalhes = HtmlHelper.GenerateLink(controllerContext.RequestContext,
                System.Web.Routing.RouteTable.Routes,
                "ICONE",
                string.Empty,
                NavigationHelper.detalhesAction,
                controllerName,
                rotasParametros,
                detalhesParametros);

            detalhes = detalhes.Replace("ICONE", "<i class='glyphicon glyphicon-search'></i>");

            // Ação de editar
            IDictionary<string, object> editarParametros = new Dictionary<string, object>() 
            {
                {"class", "btn btn-xs btn-primary"},
                {"title", NavigationHelper.editarTitle}
            };

            string editar = HtmlHelper.GenerateLink(controllerContext.RequestContext,
                System.Web.Routing.RouteTable.Routes,
                "ICONE",
                string.Empty,
                NavigationHelper.editarAction,
                controllerName,
                rotasParametros,
                editarParametros);

            editar = editar.Replace("ICONE", "<i class='glyphicon glyphicon-pencil'></i>");

            // Ação de apagar
            IDictionary<string, object> apagarParametros = new Dictionary<string, object>()
            {
                {"class", "btn btn-xs btn-danger" },
                {"title", NavigationHelper.apagarTitle}
            };

            string apagar = HtmlHelper.GenerateLink(controllerContext.RequestContext,
              System.Web.Routing.RouteTable.Routes,
              "ICONE",
              string.Empty,
              NavigationHelper.apagarAction,
              controllerName,
              rotasParametros,
              apagarParametros);

            apagar = apagar.Replace("ICONE", "<i class='glyphicon glyphicon-trash'></i>");

            // Construindo o campo das ações com os separadores corretos
            StringBuilder sb = new StringBuilder();

            if (actionsShowed.Count == 0)
            {
                if (customActions.Count > 0)
                {
                    sb.Append("<div class=\"actions\">");
                    generateCustomActions(controllerContext, id, sb, data);
                    sb.Append("</div>");
                }
                else
                    sb.AppendLine("Não existem ações disponíveis.");
            }
            else
            {
                sb.Append("<div class=\"actions\">");

                if (_customActionsPosition == CustomActionsPosition.BEFORE)
                {
                    if (customActions.Count > 0)
                        generateCustomActions(controllerContext, id, sb, data);
                }

                if (!actionsShowedCondition[DataTableRowAction.Default.DETAILS](data))
                    sb.Append(detalhes);

                if (!actionsShowedCondition[DataTableRowAction.Default.EDIT](data))
                    sb.Append(editar);

                if (!actionsShowedCondition[DataTableRowAction.Default.DELETE](data))
                    sb.Append(apagar);

                if (_customActionsPosition == CustomActionsPosition.AFTER)
                {
                    if (customActions.Count > 0)
                        generateCustomActions(controllerContext, id, sb, data);
                }

                sb.Append("</div>");
            }

            // Retorna os links das ações
            return (sb.ToString());
        }

        /// <summary>
        /// Renderiza os links das ações customizadas
        /// </summary>
        /// <param name="controllerContext">Contexto do controlador</param>
        /// <param name="id">Id do registro</param>
        /// <param name="sb">String builder dos links de ação</param>
        /// <param name="data">Registro</param>
        private void generateCustomActions(ControllerContext controllerContext, int id, StringBuilder sb, TModel data)
        {
            // Geramos as ações(links) customizados pelo usuário
            foreach (DataTableRowAction acao in customActions)
            {
                RouteValueDictionary routeValues = acao.routeValues as RouteValueDictionary;
                IDictionary<string, object> htmlAttributes = acao.htmlAttributes as IDictionary<string, object>;

                string routename = string.IsNullOrEmpty(acao.routename) ? acao.routename : string.Empty;
                string action = routeValues["action"] as string ?? string.Empty;
                string controller = routeValues["controller"] as string ?? controllerContext.RequestContext.RouteData.GetRequiredString("Controller");
                string area = routeValues["area"] as string ?? (controllerContext.RouteData.DataTokens["area"] as string);

                string identificador = routeValues["nomeIdentificador"] as string ?? nomeIdentificador;

                routeValues[identificador] = id;
                routeValues["area"] = area;

                if (!(htmlAttributes["class"] as string).Contains("btn btn-xs"))
                    htmlAttributes["class"] = "btn btn-xs " + htmlAttributes["class"];

                // Ação de detalhes
                string textoLinkAcao = HtmlHelper.GenerateLink(controllerContext.RequestContext,
                  System.Web.Routing.RouteTable.Routes,
                  "ICONE",
                  routename,
                  action,
                  controller,
                  routeValues,
                  htmlAttributes);

                textoLinkAcao = textoLinkAcao.Replace("ICONE", "<i class='" + htmlAttributes["icon"] + "'></i> " + acao.text);

                if (customActionsShowedCondition[acao](data))
                    sb.Append(textoLinkAcao);
            }
        }
        
        /// <summary>
        /// Método auxiliar para saber se uma expressão é simples (unária ou de membro).
        /// </summary>
        /// <param name="expression">Expressão a ser verificada</param>
        /// <returns>Booleano informando se é expressão simples ou não</returns>
        private bool isSimpleExpression(Expression<Func<TModel, object>> expression)
        {
            return ((expression.Body is UnaryExpression) || (expression.Body is MemberExpression));
        }

        /// <summary>
        /// Faz o processamento dos dados necessários para retornar ao DataTable
        /// </summary>
        /// <param name="dataTablePostData"></param>
        /// <returns></returns>
        public DataTableResult getDataTableResult(ControllerContext controllerContext,
            IQueryable<TModel> query,
            DataTablePostData dataTablePostData)
        {
            // Tabela a ser retornada com os dados
            IList<IList<string>> table = new List<IList<string>>();

            // Iniciamos o processamento com todos os registros
            IQueryable<TModel> todosRegistros = query.ToList().AsQueryable();

            // Tambem precismos da quantidade de registros
            int quantidadeRegistrosTotais = query.Count();

            // FILTRAGEM ( WHERE ) GERAL
            // String recebida do campo "Filtro" é testada com todos os campos em exibição na tabela
            // para realizar a filtragem através do campo de ordenação do método addColumn();
            if (dataTablePostData.sSearch != "")
            {
                ParameterExpression obj = columns.First().orderByExpression.Parameters.First();
                IEnumerable<DataTableColumn<TModel>> searchColumns = columns.Where(c => c.orderByExpression != null);
                string searchString = dataTablePostData.sSearch;

                // Expressão de comparação GERAL
                Expression expressaoComparacao = buildWhereExpression(searchString, searchColumns);

                // Criamos a chamada WHERE do Linq para o NHibernate buscar no banco...
                MethodCallExpression whereCallExpression = Expression.Call(
                        typeof(Queryable),
                        "Where",
                        new Type[] { todosRegistros.ElementType },
                        todosRegistros.Expression,
                        Expression.Lambda<Func<TModel, bool>>(expressaoComparacao, new ParameterExpression[] { obj }));

                // ... e atribuímos para a filtragem!
                todosRegistros = todosRegistros.Provider.CreateQuery<TModel>(whereCallExpression);
                
            }

            // FILTRAGEM ( WHERE ) ESPECIFICA
            // Strings recebidas dos campos de filtragem individual são testadas com os respectivos campos em exibição
            // para realizar a filtragem através do campo de ordenação do método addColumn();
            if (dataTablePostData.sSearchs.All(x => string.IsNullOrEmpty(x)) == false)
            {
                ParameterExpression obj = columns.First().orderByExpression.Parameters.First();

                int count = dataTablePostData.sSearchs.Count;

                if (actionsShowed.Count > 0 || customActions.Count > 0)
                    count = count - 1;

                // Para cada filtro individual...
                for (int i = 0; i < count; i++)
                {
                    IList<DataTableColumn<TModel>> searchColumn = new List<DataTableColumn<TModel>>();

                    // Se existe possibilidade de filtragem, então a usamos!
                    if ((/*columns[i].orderByExpression != null ||*/ columns[i].searchable) && !string.IsNullOrEmpty(dataTablePostData.sSearchs[i]))
                        searchColumn.Add(columns[i]);
                    else
                        continue; // Se não existe, pulamos para o proximo campo...

                    string searchString = dataTablePostData.sSearchs[i];

                    // Expressão de comparação ESPECIFICA a cada coluna
                    Expression expressaoComparacao = buildWhereExpression(searchString, searchColumn);

                    // Criamos a chamada WHERE do Linq para o NHibernate buscar no banco...
                    MethodCallExpression whereCallExpression = Expression.Call(
                            typeof(Queryable),
                            "Where",
                            new Type[] { todosRegistros.ElementType },
                            todosRegistros.Expression,
                            Expression.Lambda<Func<TModel, bool>>(expressaoComparacao, new ParameterExpression[] { obj }));

                    // ... e atribuímos para a filtragem!
                    todosRegistros = todosRegistros.Provider.CreateQuery<TModel>(whereCallExpression);
                }
            }

            // ORDENAÇÃO (ORDER BY)
            // Se existir uma ordenção de colunas a ser usada...
            if (dataTablePostData.iSortingCols > 0)
            {
                // Ordenação padrão: sempre primeira coluna e ascendente
                int indiceColuna = 0;
                DataTableSortDirection sentidoOrdenacao = DataTableSortDirection.Ascending;

                // Para cada coluna...
                for (int i = 0; i < dataTablePostData.iSortingCols; i++)
                {
                    // Pegamos seu indice e ordenação para saber como ordenar...
                    indiceColuna = dataTablePostData.iSortCols[i];

                    // Se o índice de orgenação é igual à ultima coluna, ou seja, das ações, não ordenamos nada.
                    if ((actionsShowed.Count > 0) && (indiceColuna == dataTablePostData.iColumns - 1))
                        break;

                    // Se estiver desabilitado a ordenação não ordena
                    if (columns[indiceColuna].sortable == false)
                        break;

                    // Se não é uma expressão simples, não ordena
                    if (!isSimpleExpression(columns[indiceColuna].orderByExpression))
                        break;

                    sentidoOrdenacao = dataTablePostData.sSortDirs[i];

                    // E ordenamos usando as expressões definidas!
                    if (sentidoOrdenacao == DataTableSortDirection.Ascending)
                        todosRegistros = todosRegistros.OrderBy(columns[indiceColuna].orderByExpression);
                    else
                        todosRegistros = todosRegistros.OrderByDescending(columns[indiceColuna].orderByExpression);
                }
            }

            int registrosFiltrados = todosRegistros != null ? todosRegistros.Count() : 0;
            //int registrosFiltrados = todosRegistros.Count();

            //Lista de todos os registros do datatables obtida atraves do método getAllRecords() para fins de relatório.
            records = todosRegistros;

            // PAGINAÇÃO
            // Depois de tudo, realizamos a paginação
            todosRegistros = todosRegistros.Skip(dataTablePostData.iDisplayStart)
                                           .Take(dataTablePostData.iDisplayLength);

            // Finalmente, para cada registro, preenchemos a tabela
            foreach (TModel registro in todosRegistros.ToList())
            {
                IList<string> colunas = new List<string>();

                // Passamos o registro para a função lambda ler o campo desejado e adicionamos o valor lido na tabela
                foreach (var column in columns)
                {
                    object dado;

                    var nome = column.dataExpression;
                    var jamaica = column.label;

                    // Se algum campo for nulo, exibe 3 hífens
                    try { dado = column.dataExpressionCompiled(registro); }
                    catch (Exception) { dado = "---"; }

                    //try
                    //{
                    //    if (column.dataExpressionCompiled(registro).ToString() == "01/01/0001 00:00:00")
                    //        dado = "";
                    //}
                    //catch { }

                    // Se foi especificado algum template para renderização, repassamos a ele o campo selecionado
                    // e retornamos a view renderizada
                    if (!string.IsNullOrEmpty(column.viewTemplate))
                    {
                        string controllerName = (string)controllerContext.RequestContext.RouteData.Values["controller"];
                        string viewPath = "DataTableTemplates/" + column.viewTemplate;

                        string viewRenderizada = RenderView.toString(dado, controllerContext, viewPath);
                        colunas.Add(viewRenderizada);
                    }
                    else
                        colunas.Add(dado == null ? string.Empty : dado.ToString());
                }

                int id = 0;

                // Se existem ações apra serem mostradas, as processamos
                if (actionsShowed.Count > 0 || customActions.Count > 0)
                {
                    try { id = Convert.ToInt32(colunas[0]); }
                    catch (Exception e) { throw new Exception("A coluna inicial necessita conter o Id do registro. " + e.Message); }
                }

                // Na última coluna, adicionamos as ações (ou uma informação de que não existem) para este registro atual
                if (actionsShowed.Count > 0 || customActions.Count > 0)
                    colunas.Add(this.generateActionLinks(controllerContext, id, registro));

                // Adicionamos na tabela a linha atualmente processada
                table.Add(colunas);
            }

            // Se chegou aqui, deu tudo certo e a tabela está pronta para ser enviada pro cliente, 
            // quentinha como pizza recem saída do forno
            return new DataTableResult(dataTablePostData, quantidadeRegistrosTotais, registrosFiltrados, table);
        }


        /// <summary>
        /// Retorna um MemberExpression A partir da expressão informada
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private MemberExpression getMemberExpression(Expression expression)
        {
            if (expression is UnaryExpression)
            {
                return getMemberExpression(((UnaryExpression)expression).Operand);
            }
            else if (expression is BinaryExpression)
            {
                return getMemberExpression(((BinaryExpression)expression).Left);
            }
            else if (expression is MemberExpression)
            {
                return expression as MemberExpression;
            }
            else if (expression is MethodCallExpression)
            {

                MethodCallExpression methodCallExp = expression as MethodCallExpression;
                var arguments = methodCallExp.Arguments;
                var methods = methodCallExp.Method;





                //PropertyInfo propertyInfo = memberEx.Member as PropertyInfo;

                //// Exemplo:
                //// x.local.nome.ToLower().Contains("AQUI") => Retorno:
                //// Aquidauana, Itaquirai, etc...
                //MethodInfo miTL = typeof(String).GetMethod("ToLower", Type.EmptyTypes);
                //var dynamicExpression = Expression.Call(memberEx, miTL);

                //MethodCallExpression contemEssaString = Expression.Call(
                //  dynamicExpression,
                //  propertyInfo.PropertyType.GetMethod("Contains", new Type[] { typeof(string) }),
                //  Expression.Constant(searchString.ToLower()));





                //TestClass t = new TestClass();
                //MethodInfo info = t.GetType().GetProperty("Name").GetSetMethod();

                //ParameterExpression param = Expression.Parameter(typeof(string), "val");

                //MethodCallExpression call = Expression.Call(Expression.Constant(t), info,
                //    new ParameterExpression[] { param });

                //Action<string> action = Expression.Lambda<Action<string>>(call, param).Compile();
                //action("hi");

                //Console.WriteLine(t.Name);




                return null;

            }
            else
            {
                throw new Exception("Expressão não tratada: " + expression.ToString());
            }
        }

        /// <summary>
        /// Monta expressões de comparação utilizando a string de busca e as colunas informadas.
        /// </summary>
        /// <param name="searchString">String de busca</param>
        /// <param name="searchColumns">Colunas a serem utilizadas para montar expressão where</param>
        /// <returns>Expressão contendo todas as filtragens Where</returns>
        private Expression buildWhereExpression(string searchString, IEnumerable<DataTableColumn<TModel>> searchColumns)
        {
            Expression expressaoComparacao = null;

            // Varremos as colunas em busca da informação digitada pelo usuário
            foreach (var column in searchColumns)
            {
                if (column.orderByExpression.Body is MethodCallExpression)
                {
                    //Converter método para expressão correta
                    //Executar método direto
                    var teste = column.orderByExpression.Body;
                    var moral = column.dataExpression;

                    object result1 = Expression.Lambda(teste).Compile().DynamicInvoke();
                    object result2 = column.orderByExpression.Compile().DynamicInvoke();

                    //expressaoComparacao = Expression.Convert(
                }

                MemberExpression memberEx = getMemberExpression(column.orderByExpression.Body);

                // Se consegue-se usar o membro para busca (é um campo simples, etc) fazemos a filtragem
                if (memberEx != null)
                {
                    // Lado esquerdo da expressão é o campo que será comparado, exemplo:
                    // x.local.nome == ...
                    Expression left = memberEx;
                    Expression right = null;

                    // Tentamos identificar de que tipo é o campo para realizar a conversão certa
                    try { ConvertSearchString(memberEx, searchString, ref left, ref right); }
                    catch (FormatException) { continue; } // Não é um booleano, ignora o campo e não busca where

                    // Se temos somente uma chamada de método (no caso, .Contains("Algo")) otimizamos a query
                    // pegando só o lado esquerdo da comparação...
                    if ((left != null) && (right == null))
                    {
                        // Se for a primeira expressão...
                        if (expressaoComparacao == null)
                            expressaoComparacao = left;
                        else // ... se não, concatenamos com uma expressão OR
                            expressaoComparacao = Expression.OrElse(expressaoComparacao, left);
                    }
                    else // ...se não temos uma chamada de método, fazemos comparação normal: if (X == Y) ...
                    {
                        // Se for a primeira expressão...
                        if (expressaoComparacao == null)
                            expressaoComparacao = Expression.Equal(left, right);
                        else // ... se não, concatenamos com uma expressão OR
                            expressaoComparacao = Expression.OrElse(expressaoComparacao, Expression.Equal(left, right));
                    }
                }
            }
            return expressaoComparacao;
        }

        /// <summary>
        /// Tenta converter uma string para o mesmo tipo da propriedade informada
        /// </summary>
        /// <param name="memberEx">Membro que deseja-se converter</param>
        /// <param name="propertyInfo"></param>
        /// <param name="searchString"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        private void ConvertSearchString(MemberExpression memberEx, string searchString, ref Expression left, ref Expression right)
        {
            PropertyInfo propertyInfo = memberEx.Member as PropertyInfo;

            if (propertyInfo.PropertyType == typeof(bool))
            {
                try { right = Expression.Constant(Convert.ToBoolean(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é um booleano
            }
            else if (propertyInfo.PropertyType == typeof(int))
            {
                try { right = Expression.Constant(Convert.ToInt32(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é um número
            }
            else if (propertyInfo.PropertyType == typeof(decimal))
            {
                try { right = Expression.Constant(Convert.ToDecimal(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é um número
            }
            else if (propertyInfo.PropertyType == typeof(double))
            {
                try { right = Expression.Constant(Convert.ToDouble(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é um número
            }
            else if (propertyInfo.PropertyType == typeof(float))
            {
                try { right = Expression.Constant(Convert.ToSingle(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é um número
            }
            else if (propertyInfo.PropertyType == typeof(DateTime))
            {
                try { right = Expression.Constant(Convert.ToDateTime(searchString)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é uma data
            }
            else if (propertyInfo.PropertyType == typeof(DateTime?))
            {
                DateTime? dateTime = Convert.ToDateTime(searchString);
                try { right = Expression.Constant(dateTime, typeof(System.Nullable<DateTime>)); }
                catch (FormatException e) { throw new FormatException("Não foi possível realizar a conversão.", e); }// Não é uma data
            }
            else // Se nao foi nenhum tipo anterior até aqui, é uma string... bem, é o que nós assumimos :P
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    // Exemplo:
                    // x.local.nome.ToLower().Contains("AQUI") => Retorno:
                    // Aquidauana, Itaquirai, etc...
                    MethodInfo miTL = typeof(String).GetMethod("ToLower", Type.EmptyTypes);
                    var dynamicExpression = Expression.Call(memberEx, miTL);

                    MethodCallExpression contemEssaString = Expression.Call(
                      dynamicExpression,
                      propertyInfo.PropertyType.GetMethod("Contains", new Type[] { typeof(string) }),
                      Expression.Constant(searchString.ToLower()));

                    left = contemEssaString;
                    right = null;
                }
                else // Se não for uma string (ultima checagem de erros, só por desencargo de consciência) usamos constante zero
                    right = Expression.Constant(0);
            }
        }

        /// <summary>
        /// Processa uma expressão de membro e retorna um DisplayName ou nome do membro
        /// </summary>
        /// <param name="methodCallExp">Expressão de membro</param>
        /// <returns>Retorna um atributo DisplayName ou nome do membro</returns>
        private string processMemberExpression(MemberExpression memberExp)
        {
            if (memberExp == null)
                throw new ArgumentException("A expressão não contém um membro.");

            // Pegamos o nome da propriedade informada
            string propertyName = memberExp.Member.Name;

            // Pegamos o tipo do modelo
            Type classType = memberExp.Member.DeclaringType; //typeof(TModel);

            // Pegamos as informações da propriedade desejada
            PropertyInfo propertyInfo = classType.GetProperty(propertyName);

            // Vemos se o atributo [DisplayName] está definido para a propriedade desejada
            bool isDefined = Attribute.IsDefined(propertyInfo, typeof(DisplayNameAttribute));

            // Se está definido...
            if (isDefined)
            {
                DisplayNameAttribute displayNameAttr =
                  (DisplayNameAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(DisplayNameAttribute));

                // ...retornamos a string informada
                if (displayNameAttr != null)
                    return displayNameAttr.DisplayName;
            }

            return propertyName; // Se não está, retornamos o nome da propriedade em si
        }

        /// <summary>
        /// Processa uma expressão de chamada de método e retorna um DisplayName ou nome do método
        /// </summary>
        /// <param name="methodCallExp">Expressão de chamada de método</param>
        /// <returns>Retorna um atributo DisplayName ou nome do método</returns>
        private string processMethodCallExpression(MethodCallExpression methodCallExp)
        {
            if (methodCallExp == null)
                throw new ArgumentException("A expressão não contém um membro.");

            string methodName = methodCallExp.Method.Name;

            DisplayNameAttribute[] displayNameAttr =
              (DisplayNameAttribute[])
              methodCallExp.Method.GetCustomAttributes(typeof(DisplayNameAttribute), false);

            // ...retornamos a string do DisplayName
            if (displayNameAttr != null && displayNameAttr.Count() > 0)
                return displayNameAttr[0].DisplayName;

            // OU NÃO, caso não tenha :)
            return methodName;
        }

        /// <summary>
        /// Retorna uma lista de strings contendo o nome das colunas usadas
        /// </summary>
        /// <returns>Lista de strings contendo o nome das colunas usadas</returns>
        public DataTableViewData<TModel> renderDataTable()
        {
            DataTableViewData<TModel> viewData = new DataTableViewData<TModel>();
            viewData.columns = this.columns;
            viewData.columnsList = this.columnsList;
            viewData.groupColumns = this.groupColumns;
            viewData.actionsShowed = this.actionsShowed;
            viewData.customActions = this.customActions;
            viewData.withColumnFilter = this.withColumnFilter;
            viewData.name = string.Empty;

            return viewData;
        }

        /// <summary>
        /// Retorna uma lista de strings contendo o nome das colunas usadas
        /// </summary>
        /// <returns>Lista de strings contendo o nome das colunas usadas</returns>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataTableViewData<TModel> renderDataTable(string name)
        {
            DataTableViewData<TModel> viewData = this.renderDataTable();
            viewData.name = name;

            return viewData;
        }

        /// <summary>
        /// Define um nome de coluna a partir de um atributo DisplayName ou uma string informada.
        /// </summary>
        /// <param name="expression">Expressão</param>
        /// <param name="label">Nome informado da coluna</param>
        private DataTableColumn<TModel> newColumn(Expression<Func<TModel, object>> dataExpression, string columnLabel, Expression<Func<TModel, object>> orderByExpression)
        {
            DataTableColumn<TModel> newColumn = new DataTableColumn<TModel>();

            string displayName = "";

            // Se usuário definiu label, usamos ele
            if ((columnLabel != null) && (columnLabel.Length > 0))
            {
                displayName = columnLabel;
            }
            else if (dataExpression.Body is UnaryExpression)
            {
                MemberExpression memberEx = ((UnaryExpression)dataExpression.Body).Operand as MemberExpression;
                displayName = processMemberExpression(memberEx);
            }
            else if (dataExpression.Body is MemberExpression)
            {
                MemberExpression memberEx = dataExpression.Body as MemberExpression;
                displayName = processMemberExpression(memberEx);
            }
            else if (dataExpression.Body is MethodCallExpression)
            {
                MethodCallExpression methodCallEx = dataExpression.Body as MethodCallExpression;
                displayName = processMethodCallExpression(methodCallEx);
            }
            else
            {
                // Qualquer outro tipo de expressão mais complexa faz nada
            }

            // Rótulo
            newColumn.label = displayName;

            // Expressões de dados, para popular a coluna com os dados desejados
            newColumn.dataExpression = dataExpression;
            newColumn.dataExpressionCompiled = newColumn.dataExpression.Compile();

            // Se foi passada uma ordenação customizada, usamos sem negociar
            if (orderByExpression != null)
            {
                newColumn.orderByExpression = orderByExpression;
            }
            // Se não foi passada uma expressão de ordenação e a expressão de dados é simples, a usamos
            else if (isSimpleExpression(dataExpression))
            {
                newColumn.orderByExpression = dataExpression;
            }
            else // Se é uma expressão complexa e não foi passada ordenação customizada, não ordenamos
            {
                newColumn.orderByExpression = null;
                newColumn.orderByExpressionCompiled = null;
            }

            // Já pré compilamos a expressão orderby para ganhar performance
            if (newColumn.orderByExpression != null)
                newColumn.orderByExpressionCompiled = newColumn.orderByExpression.Compile();

            // Adicionamos a nova coluna
            columns.Add(newColumn);

            // Se estamos agrupando, não adicionamos a coluna folha na listagem de renderização
            // pois cada coluna agrupadora será responsável pela renderização das colunas que
            // contém.
            if (!startedGrouping)
                columnsList.Add(newColumn);

            return newColumn;
        }

        /// <summary>
        /// Define um template em view para ser renderizado.
        /// </summary>
        /// <param name="viewTemplate">"Nome da view"</param>
        /// <returns></returns>
        public DataTable<TModel> useViewTemplate(string viewTemplate)
        {
            columns.Last().viewTemplate = viewTemplate;

            return this;
        }

        /// <summary>
        /// Desabilita a ordenação de uma coluna
        /// </summary>
        /// <returns></returns>
        public DataTable<TModel> disableSorting()
        {
            columns.Last().sortable = false;

            return this;
        }

        /// <summary>
        /// Desabilita o filtro da coluna
        /// </summary>
        /// <returns></returns>
        public DataTable<TModel> disableSearching()
        {
            columns.Last().searchable = false;

            return this;
        }

        /// <summary>
        /// Define o tamanho da coluna
        /// </summary>
        /// <param name="width">"tamanho para coluna em pixel ou em porcentagem"</param>
        /// <returns></returns>
        public DataTable<TModel> setWidth(string width)
        {
            columns.Last().width = width;

            return this;
        }

        /// <summary>
        /// Define a classe CSS que será atribuída a coluna
        /// </summary>
        /// <param name="cssClass">"classe CSS que será atribuída a coluna"</param>
        /// <returns></returns>
        public DataTable<TModel> setCssClass(string cssClass)
        {
            columns.Last().cssClass = cssClass;

            return this;
        }

        /// <summary>
        /// Define a classe CSS que será atribuída a coluna
        /// </summary>
        /// <param name="cssClass">"classe CSS que será atribuída a coluna"</param>
        /// <returns></returns>
        public DataTable<TModel> setEditable()
        {
            columns.Last().editable = true;

            return this;
        }

        /// <summary>
        /// Define o tipo do campo de busca
        /// </summary>
        /// <param name="type">tipo do campo de busca: "text", "number", "select", por padrão é "text"</param>
        /// <returns></returns>
        public DataTable<TModel> setSearchType(string type)
        {
            columns.Last().searchType = type;

            return this;
        }

        /// <summary>
        /// Define os valores possíveis para o campo de busca
        /// </summary>
        /// <param name="lista">Lista de valores possíveis para a busca</param>
        /// <returns></returns>
        public DataTable<TModel> setSearchValues(List<string> lista)
        {
            columns.Last().searchValues = ", values: [ '" + String.Join(",", lista.ToArray()).Replace(",", "', '") + "' ]";

            return this;
        }

        /// <summary>
        /// Adiciona um campo do modelo como coluna da tabela
        /// </summary>
        /// <param name="memberSelector">Seletor de campo do modelo</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addColumn(Expression<Func<TModel, object>> memberSelector)
        {
            return this.addColumn(memberSelector, null, null);
        }

        /// <summary>
        /// Adiciona um campo do modelo como coluna da tabela
        /// </summary>
        /// <param name="memberSelector">Seletor de campo do modelo</param>
        /// <param name="columnLabel">Rótulo customizado</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addColumn(Expression<Func<TModel, object>> memberSelector, string columnLabel)
        {
            return this.addColumn(memberSelector, null, columnLabel);
        }

        /// <summary>
        /// Adiciona um campo do modelo como coluna da tabela
        /// </summary>
        /// <param name="memberSelector">Seletor de campo do modelo</param>
        /// <param name="columnLabel">Rótulo customizado</param>
        /// <param name="orderByExpression">Expressão usada para ordenação caso o seletor de membro 
        /// seja uma expressão complexa</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addColumn(Expression<Func<TModel, object>> memberSelector, Expression<Func<TModel, object>> orderByExpression)
        {
            return this.addColumn(memberSelector, orderByExpression, null);
        }

        /// <summary>
        /// Adiciona um campo do modelo como coluna da tabela
        /// </summary>
        /// <param name="memberSelector">Seletor de campo do modelo</param>
        /// <param name="columnLabel">Rótulo customizado</param>
        /// <param name="orderByExpression">Expressão usada para ordenação caso o seletor de membro 
        /// seja uma expressão complexa</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addColumn(Expression<Func<TModel, object>> memberSelector, Expression<Func<TModel, object>> orderByExpression, string columnLabel)
        {
            DataTableColumn<TModel> column = newColumn(memberSelector, columnLabel, orderByExpression);

            if (startedGrouping)
            {
                column.groupColumn = groupColumns.Last();
                column.groupColumn.columns.Add(column);
            }

            return this;
        }

        /// <summary>
        /// Não exibe uma ação específica na tabela
        /// </summary>
        /// <param name="action">Ação a não ser exibida</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addAction(DataTableRowAction action)
        {
            return addAction(action, x => true);
        }

        /// <summary>
        /// Não exibe uma ação específica na tabela
        /// </summary>
        /// <param name="action">Ação a não ser exibida</param>
        /// <param name="condition">Condição para a ação ser exibida</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> addAction(DataTableRowAction action, Expression<Func<TModel, bool>> condition)
        {
            customActions.Add(action);

            if (condition != null)
                customActionsShowedCondition.Add(action, condition.Compile());

            return this;
        }

        /// <summary>
        /// Não exibe uma ação específica na tabela
        /// </summary>
        /// <param name="action">Ação a não ser exibida</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> notShowAction(DataTableRowAction.Default action)
        {
            actionsShowed.Remove(action);
            return notShowAction(action, x => true);
        }

        /// <summary>
        /// Não exibe uma ação específica na tabela
        /// </summary>
        /// <param name="action">Ação a não ser exibida</param>
        /// <param name="condition">Condição para a ação não ser exibida</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> notShowAction(DataTableRowAction.Default action, Expression<Func<TModel, bool>> condition)
        {
            if (condition != null)
                actionsShowedCondition[action] = condition.Compile();

            return this;
        }

        /// <summary>
        /// Exibe a barra de filtragem individual por coluna
        /// </summary>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> drawColumnFilter()
        {
            withColumnFilter = true;

            return this;
        }

        /// <summary>
        /// Define em qual posição as ações customizadas serão exibidas: antes ou depois das ações padrões.
        /// </summary>
        /// <param name="position">Posição de exibição</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> setCustomActionsPosition(CustomActionsPosition position)
        {
            _customActionsPosition = position;

            return this;
        }

        /// <summary>
        /// Inicia a definição de mais uma coluna agrupadora. Uma coluna agrupadora
        /// agrupa outras colunas sob um mesmo rótulo. O efeito é somente estético e organizacional.
        /// </summary>
        /// <param name="groupedLabel">Rótulo agrupador</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> startGroupedColumn(string groupedLabel)
        {
            // Se alguém já começou a agrupar e quer aninhar agrupamentos, proibimos
            if (startedGrouping)
                throw new NotSupportedException("Por enquanto não é suportado agrupamento aninhado de colunas.");

            DataTableGroupColumn<TModel> newGroupColumn = new DataTableGroupColumn<TModel>();
            newGroupColumn.label = groupedLabel;

            groupColumns.Add(newGroupColumn);
            columnsList.Add(newGroupColumn);

            startedGrouping = true;

            return this;
        }

        /// <summary>
        /// Termina a definição de mais uma coluna agrupadora. Uma coluna agrupadora
        /// agrupa outras colunas sob um mesmo rótulo. O efeito é somente estético e organizacional.
        /// </summary>
        /// <param name="groupedLabel">Rótulo agrupador</param>
        /// <returns>O mesmo datatable para encadeamento de expressões</returns>
        public DataTable<TModel> endGroupedColumn()
        {
            // Se nem começou a agrupar, como podemos parar?
            if (!startedGrouping)
                throw new Exception("Não foi iniciado um agrupamento de colunas.");

            startedGrouping = false;

            return this;
        }

        public List<TModel> getRecords()
        {
            return this.records.ToList();
        }
    }
}
