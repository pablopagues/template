﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;
using System.ComponentModel;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Ações a serem feitas nos registros em exibição no DataTables
    /// </summary>
    public class DataTableRowAction
    {
        public DataTableRowAction(string text, RouteValueDictionary routeValues, Dictionary<string, object> htmlAttributes)
        {
            this.text = text;
            this._routeValues = routeValues;
            this._htmlAttributes = htmlAttributes;
            this.routename = "Default";
        }

        public DataTableRowAction(string text, object routeValues, object htmlAttributes)
        {
            this.text = text;
            this._routeValues = new RouteValueDictionary(routeValues);
            this.htmlAttributes = htmlAttributes;
            this.routename = "Default";
        }

        public DataTableRowAction(string text, string routename, RouteValueDictionary routeValues, Dictionary<string, object> htmlAttributes)
        {
            this.text = text;
            this.routename = routename;
            this._routeValues = routeValues;
            this._htmlAttributes = htmlAttributes;
        }

        public DataTableRowAction(string text, string routename, object routeValues, object htmlAttributes)
        {
            this.text = text;
            this.routename = routename;
            this._routeValues = new RouteValueDictionary(routeValues);
            this.htmlAttributes = htmlAttributes;
        }

        /// <summary>
        /// Ações que são exibidas por registro na DataTable
        /// </summary>
        public enum Default
        {
            EDIT, DETAILS, DELETE
        }

        /// <summary>
        /// Texto a ser exibido no link
        /// </summary>
        public string text { get; set; }

        /// <summary>
        /// Valores das rotas para os links
        /// </summary>
        private RouteValueDictionary _routeValues;
        public object routeValues
        {
            get
            {
                return _routeValues;
            }
            set
            {
                _routeValues = new RouteValueDictionary(value);
            }
        }

        /// <summary>
        /// Nome da Rota
        /// </summary>
        public string routename;

        /// <summary>
        /// Atributos Html para os links
        /// </summary>
        private IDictionary<string, object> _htmlAttributes { get; set; }
        public object htmlAttributes
        {
            get
            {
                return _htmlAttributes;
            }
            set
            {
                if (_htmlAttributes == null)
                    _htmlAttributes = new Dictionary<string, object>();

                if (value != null)
                {
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(value))
                    {
                        object obj2 = descriptor.GetValue(value);
                        _htmlAttributes.Add(descriptor.Name, obj2);
                    }
                }
            }
        }

    }
}
