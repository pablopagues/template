﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Coluna agrupadora
    /// </summary>
    public class DataTableGroupColumn<TModel> : DataTableGenericColumn where TModel : class
    {
        /// <summary>
        /// Colunas contidas nesta coluna agrupadora
        /// </summary>
        public IList<DataTableColumn<TModel>> columns { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        public DataTableGroupColumn()
        {
            columns = new List<DataTableColumn<TModel>>();
        }
    }
}
