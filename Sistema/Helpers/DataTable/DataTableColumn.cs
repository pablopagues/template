﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Classe usada internamente para definir dados das colunas na renderização do
    /// DataTable
    /// </summary>
    public class DataTableColumn<TModel> : DataTableGenericColumn where TModel : class
    {
        /// <summary>
        /// View template a ser usada na renderização dos dados
        /// </summary>
        public string viewTemplate { get; set; }

        /// <summary>
        /// Desabilitar/Habilitar ordenação
        /// </summary>
        public bool sortable { get; set; }

        /// <summary>
        /// Desabilitar/Habilitar filtro
        /// </summary>
        public bool searchable { get; set; }

        /// <summary>
        /// Desabilitar/Habilitar edição
        /// </summary>
        public bool editable { get; set; }

        /// <summary>
        /// Define o tamanho da coluna em pixel ou em porcentagem
        /// </summary>
        public string width { get; set; }

        /// <summary>
        /// Define a classe CSS que será atribuída a coluna
        /// </summary>
        public string cssClass { get; set; }

        /// <summary>
        /// Define o tipo do campo de busca
        /// </summary>
        public string searchType { get; set; }

        /// <summary>
        /// Define os valores possíveis para o campo de busca
        /// </summary>
        public string searchValues { get; set; }

        /// <summary>
        /// Expressão que montará os dados a serem exibidos na coluna
        /// </summary>
        public Expression<Func<TModel, object>> dataExpression { get; set; }
        public Func<TModel, object> dataExpressionCompiled { get; set; }

        /// <summary>
        /// Expressão que será usada para ordenar os dados da coluna quando a expressão
        /// de dados for muito complexa ou for definida explícitamente pelo usuário
        /// </summary>
        public Expression<Func<TModel, object>> orderByExpression { get; set; }
        public Func<TModel, object> orderByExpressionCompiled { get; set; }

        /// <summary>
        /// Coluna agrupadora à qual pertence
        /// </summary>
        public DataTableGroupColumn<TModel> groupColumn { get; set; }

        public Expression<Func<TModel,object>> FilterExpression { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        public DataTableColumn()
        {
            sortable = true;
            searchable = true;
            editable = false;
            width = String.Empty;
            cssClass = String.Empty;
            searchType = "text";
            searchValues = String.Empty;
        }
    }
}
