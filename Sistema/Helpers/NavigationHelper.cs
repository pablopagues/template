﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;
using System.Web.Mvc.Html;

namespace Sistema.Helpers
{
    /// <summary>
    /// Classe que define os helpers usados para renderizar links e outras informações
    /// frequentes nas páginas do sistema.
    /// </summary>
    public static class NavigationHelper
    {
        public static string voltarTitle { get { return "Retorna a listagem de todos os registros."; } }
        public static string voltarText { get { return "Voltar à listagem"; } }
        public static string voltarAction { get { return "Index"; } }

        public static string criarTitle { get { return "Abre um formulário para criação de novo registro."; } }
        public static string criarText { get { return "Criar novo"; } }
        public static string criarAction { get { return "Create"; } }

        public static string editarTitle { get { return "Editar dados do registro."; } }
        public static string editarText { get { return "Editar"; } }
        public static string editarAction { get { return "Edit"; } }

        public static string apagarTitle { get { return "Apagar o registro."; } }
        public static string apagarText { get { return "Apagar"; } }
        public static string apagarAction { get { return "Delete"; } }

        public static string detalhesTitle { get { return "Mostrar todos os detalhes do registro."; } }
        public static string detalhesText { get { return "Detalhes"; } }
        public static string detalhesAction { get { return "Details"; } }

        #region HtmlHelper extensions

        public static MvcHtmlString LinkTo(this HtmlHelper htmlHelper, string action, string classe, string title, string text, string icon = "")
        {
            UrlHelper url = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            if (!String.IsNullOrEmpty(icon))
                icon = String.Format("<i class=\"icon-white {0}\"></i> ", icon);

            return MvcHtmlString.Create(String.Format("<a href=\"{0}\" class=\"{1}\" title=\"{2}\">{3}{4}</a>", url.Action(action), classe, title, icon, text));
        }

        /// <summary>
        /// Monta um link "Voltar à listagem" apontando para a ação "Index".
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns>Uma string HTML contendo um link "Voltar à listagem" apontando para a ação "Index".</returns>
        public static MvcHtmlString LinkVoltarAListagem(this HtmlHelper htmlHelper, string classe = "btn btn-default")
        {
            return LinkTo(htmlHelper, voltarAction, classe, voltarTitle, voltarText);
        }

        /// <summary>
        /// Monta um link "Criar novo" apontando para a ação "Create".
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns>Uma string HTML contendo um link "Criar novo" aopntando para a ação "Create".</returns>
        public static MvcHtmlString LinkCriarNovo(this HtmlHelper htmlHelper, string classe = "btn btn-success")
        {
            return LinkTo(htmlHelper, criarAction, classe, criarTitle, criarText, "icon-plus");
        }

        /// <summary>
        /// Monta um link "Editar" apontando para a ação "Edit".
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="identificador">Identificador único do registro a ser editado</param>
        /// <returns>Uma string HTML contendo um link "Editar" apontando para a ação "Edit".</returns>
        public static MvcHtmlString LinkEditar(this HtmlHelper htmlHelper, int identificador, string classe = "btn btn-primary")
        {
            return LinkTo(htmlHelper, editarAction + "/" + identificador, classe, editarTitle, editarText, "icon-pencil");
        }

        /// <summary>
        /// Monta um link "Apagar" apontando para a ação "Delete".
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="identificador">Identificador único do registro a ser apagado</param>
        /// <returns>Uma string HTML contendo um link "Apagar" apontando para a ação "Delete".</returns>
        public static MvcHtmlString LinkApagar(this HtmlHelper htmlHelper, int identificador, string classe = "btn btn-danger")
        {
            return LinkTo(htmlHelper, apagarAction + "/" + identificador, classe, apagarTitle, apagarText, "icon-trash");
        }
        
        public static MvcHtmlString LinkDetalhes(this HtmlHelper htmlHelper, int identificador)
        {
            return LinkDetalhes(htmlHelper, identificador, null);
        }

        public static MvcHtmlString LinkDetalhes(this HtmlHelper htmlHelper, int identificador, string classe)
        {
            var htmlAttributes = new { @title = detalhesTitle, @class = classe };

            return LinkExtensions.ActionLink(htmlHelper, detalhesText, detalhesAction, new { id = identificador }, htmlAttributes);
        }

        #endregion
    }
}
