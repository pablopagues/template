﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.IO;

namespace Sistema.Helpers
{
    public static class RenderView
    {
        public static string toString<T>(T model, ControllerContext controllerContext, string viewPath)
        {
            using (var writer = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewPath);
                var viewDictionary = new ViewDataDictionary<T>(model);
                var viewContext = new ViewContext(controllerContext, viewResult.View, viewDictionary, new TempDataDictionary(), writer);
                viewResult.View.Render(viewContext, writer);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return writer.GetStringBuilder().ToString();
            }
        }
    }
}
