﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Sistema.Helpers.Attributes
{
    public class FromJsonAttribute : CustomModelBinderAttribute
    {
        // Padrão da Microsoft, não utilizamos por ser muito lento, preferimos o JSON.Net
        // private readonly static JavaScriptSerializer serializer = new JavaScriptSerializer();

        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder();
        }

        private class JsonModelBinder : IModelBinder
        {
            public class BrazilDateTimeConvertor : DateTimeConverterBase
            {
                public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
                {
                    return DateTime.Parse(reader.Value.ToString());
                }

                public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
                {
                    writer.WriteValue(((DateTime)value).ToString("dd/MM/yyyy"));
                }
            }

            public class BrazilDecimalConvertor : JsonConverter
            {
                public override bool CanConvert(Type objectType)
                {
                    if (objectType == typeof(decimal) || objectType == typeof(decimal?)) return true;
                    return false;
                }

                public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
                {
                    if (!String.IsNullOrEmpty(reader.Value.ToString()))
                        return Decimal.Parse(reader.Value.ToString());
                    else
                        return 0m;
                }

                public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
                {
                    writer.WriteValue(Convert.ToDecimal(value.ToString()));
                }
            }

            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                // O nome do parâmetro do controlador recebendo os dados tem que ser o mesmo
                // nome do objeto json sendo enviado pelo Knockout/view
                string stringified = controllerContext.HttpContext.Request[bindingContext.ModelName];

                if (string.IsNullOrEmpty(stringified))
                    return null;

                var objeto = Newtonsoft.Json.JsonConvert.DeserializeObject(stringified, bindingContext.ModelType, new BrazilDateTimeConvertor(), 
                    new BrazilDecimalConvertor());

                return objeto;
            }
        }
    }
}