﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Sistema.Helpers.Attributes
{
  /// <summary>
  /// Autoriza os usuários especificados
  /// </summary>
  public class AuthorizeUsersAttribute : AuthorizeRedirect
  {
    /// <summary>
    /// Autoriza os usuários especificados
    /// </summary>
    /// <param name="users">Parametros contendo string de usuários</param>
    public AuthorizeUsersAttribute(params string[] users)
    {
      this.Users = string.Join(",", users);
    }
  }
}
