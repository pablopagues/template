﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sistema.Helpers.Attributes
{
  public class AuthorizeRedirect : AuthorizeAttribute
  {
    public override void OnAuthorization(AuthorizationContext filterContext)
    {
      base.OnAuthorization(filterContext);

      if (filterContext.HttpContext.User.Identity.IsAuthenticated && (filterContext.Result is HttpUnauthorizedResult))
        filterContext.Result = new RedirectResult("~/Erro/NaoAutorizado");
    }
  }
}