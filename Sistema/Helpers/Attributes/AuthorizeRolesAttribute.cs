﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Sistema.Helpers.Attributes
{
  /// <summary>
  /// Autoriza os papéis especificados
  /// </summary>
  public class AuthorizeRolesAttribute : AuthorizeRedirect
  {
    /// <summary>
    /// Autoriza os papéis especificados
    /// </summary>
    /// <param name="roles">Parametros contendo string de papéis</param>
    public AuthorizeRolesAttribute(params string[] roles)
    {
      this.Roles = string.Join(",", roles);
    }
  }
}
