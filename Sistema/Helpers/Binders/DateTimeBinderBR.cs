﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace Sistema.Helpers.Binders
{
  public class DateTimeBinderBR<TModel> : IModelBinder
  {
    public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
    {
      string key = bindingContext.ModelName;
      ValueProviderResult result = bindingContext.ValueProvider.GetValue(key);

      if (result != null)
      {
        // the second parameter is the culture to use for conversion.
        // you may want to have a try / catch around the ConvertTo() call,
        // e.g. the user didn't type in a valid date.

        return result.ConvertTo(typeof(TModel), CultureInfo.GetCultureInfoByIetfLanguageTag("pt-BR"));
      }
      else
      {
        // no value found
        return null;
      }

    }
  }
}