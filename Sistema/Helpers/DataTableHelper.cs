﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Sistema.Helpers.DataTable
{
    /// <summary>
    /// Classe para auxiliar a renderização de uma tabela compatível com o DataTable.
    /// </summary>
    public static class DataTableHelper
    {
        #region HtmlHelper extensions

        /// <summary>
        /// Desenha a estrutura inicial de uma jQuery DataTable contendo as colunas especificadas
        /// </summary>
        /// <param name="htmlHelper">HTML Helper</param>
        /// <param name="columns">Colunas a serem renderizadas</param>
        /// <returns>Uma tabela HTML a ser usada pelo jQuery Datatables</returns>
        public static MvcHtmlString DrawDataTable<TModel>(this HtmlHelper htmlHelper, DataTableViewData<TModel> viewData) where TModel : class
        {
            if (viewData.columns.Count == 0)
                throw new Exception("Não foram especificadas colunas para a renderização da DataTable.");

            int groupColumns = viewData.groupColumns.Count;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<table class=\"datatable table\" id=\"datatable_id_" + viewData.name + "\">");
            sb.AppendLine("<thead>");

            // Se existem colunas agrupadas, imprimimos como devem ser impressas
            if (groupColumns > 0)
            {
                sb.AppendLine("<tr>");

                // Caminhamos pela listagem de colunas para descobrir se são agrupadas ou não
                // e imprimimos os rótulos delas (colunas "raízes")
                foreach (var column in viewData.columnsList)
                {
                    if (column is DataTableColumn<TModel>)
                    {
                        sb.AppendLine("<th rowspan=\"2\">" + column.label + "</th>");
                    }
                    else if (column is DataTableGroupColumn<TModel>)
                    {
                        DataTableGroupColumn<TModel> groupColumn = column as DataTableGroupColumn<TModel>;
                        sb.AppendLine("<th colspan=\"" + groupColumn.columns.Count + "\">" + groupColumn.label + "</th>");
                    }
                }

                if (viewData.actionsShowed.Count > 0 || viewData.customActions.Count > 0)
                    sb.AppendLine("<th rowspan=\"2\">Ações</th>");

                sb.AppendLine("</tr>");

                sb.AppendLine("<tr>");

                // Por fim, imprimimos os somente os rótulos das colunas que foram agrupadas (colunas "folhas")
                foreach (var groupColumn in viewData.groupColumns)
                    foreach (var column in groupColumn.columns)
                        sb.AppendLine("<th>" + column.label + "</th>");

                sb.AppendLine("</tr>");
            }
            // Se caso contrário imprimimos da forma simples e habitual de sempre
            else
            {
                sb.AppendLine("<tr>");

                foreach (var column in viewData.columnsList)
                    sb.AppendLine("<th>" + column.label + "</th>");

                if (viewData.actionsShowed.Count > 0 || viewData.customActions.Count > 0)
                    sb.AppendLine("<th class='text-center'>Ações</th>");

                sb.AppendLine("</tr>");
            }

            sb.AppendLine("</thead>");
            sb.AppendLine("<tbody>");
            sb.AppendLine("<tr>");

            // Adicionamos uma linha em branco no conteúdo para fins de parsing do Datatables
            for (int i = 0, columnCount = viewData.columns.Count + 1; i < columnCount; i++)
                sb.AppendLine("<td></td>");

            sb.AppendLine("</tr>");
            sb.AppendLine("</tbody>");

            if (viewData.withColumnFilter)
            {
                sb.AppendLine("<tfoot>");
                sb.AppendLine("<tr>");

                foreach (var column in viewData.columnsList)
                    sb.AppendLine("<th>" + column.label + "</th>");

                if (viewData.actionsShowed.Count > 0 || viewData.customActions.Count > 0)
                    sb.AppendLine("<th>Ações</th>");

                sb.AppendLine("</tr>");
                sb.AppendLine("</tfoot>");
            }

            sb.AppendLine("</table>");

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString DrawDataTableHeader<TModel>(this HtmlHelper htmlHelper, ViewContext viewContext, DataTableViewData<TModel> viewData) where TModel : class
        {
            return htmlHelper.DrawDataTableHeader(viewContext, viewData, 0, DataTableSortDirection.Ascending);
        }

        public static MvcHtmlString DrawDataTableHeader<TModel>(this HtmlHelper htmlHelper, ViewContext viewContext, DataTableViewData<TModel> viewData, int columnSorted, DataTableSortDirection typeSorting) where TModel : class
        {
            if (viewContext == null)
                throw new Exception("Não foi especificado contexto da View para a renderização da DataTable.");

            UrlHelper urlHelper = new UrlHelper(viewContext.RequestContext);

            // Convenção: URL de requisição de dados pelo Datatables: "/Controlador/DataTable[Nome da ação origem][Nome do datatables]"
            string actionName = (string)viewContext.RequestContext.RouteData.Values["Action"];
            string url = urlHelper.Action("DataTable" + actionName + viewData.name, viewContext.RequestContext.RouteData.Values);

            StringBuilder header = new StringBuilder();
            StringBuilder footer = new StringBuilder();

            bool renderizaAcoes = viewData.actionsShowed.Count > 0 || viewData.customActions.Count > 0;

            foreach (var column in viewData.columns)
            {
                bool ultimoItem = column == viewData.columns.Last();

                if (column.sortable == false || column.searchable == false || column.editable == true || !String.IsNullOrEmpty(column.width) || !String.IsNullOrEmpty(column.cssClass))
                {
                    header.Append("{ ");
                    if (!String.IsNullOrEmpty(column.width))
                        header.Append("'sWidth': '" + column.width + "', ");

                    if (!String.IsNullOrEmpty(column.cssClass))
                        header.Append("'sClass': '" + column.cssClass + "', ");

                    if (column.editable == true)
                        header.Append("'mRender': function(data, type, full){ return '<a href=\"#\" data-behaviour=\"editable\" data-type=\"text\" data-name=\"" + column.label + "\" data-pk=\"'+full[0]+'\" data-url=\"" + urlHelper.Action("Atualizar", viewContext.RequestContext.RouteData.Values) + "\" data-title=\"Digite o " + column.label + "\">'+data+'</a>'; },");

                    if (column.sortable == false)
                        header.Append("'bSortable': false, ");
                    else
                        header.Append("'bSortable': true, ");

                    if (column.searchable == false)
                        header.Append("'bSearchable': false");
                    else
                        header.Append("'bSearchable': true");
                    header.Append(" }");

                    if (!ultimoItem || renderizaAcoes) header.AppendLine(","); // Código feio, eu sei, mas é pro IE 7/8/9 não reclamarem da sintaxe

                    if (viewData.withColumnFilter)
                    {
                        if (column.searchable == false)
                            footer.Append("null");
                        else
                            footer.Append("{ type: '" + column.searchType + "' " + column.searchValues + " }");

                        if (!ultimoItem || renderizaAcoes) footer.AppendLine(","); // Código feio, eu sei, mas é pro IE 7/8/9 não reclamarem da sintaxe
                    }
                }
                else
                {
                    header.Append("null");

                    if (!ultimoItem || renderizaAcoes) header.AppendLine(","); // Código feio, eu sei, mas é pro IE 7/8/9 não reclamarem da sintaxe

                    if (viewData.withColumnFilter)
                    {
                        footer.Append("{ type: '" + column.searchType + "'" + column.searchValues + " }");
                        if (!ultimoItem || renderizaAcoes) footer.AppendLine(","); // Código feio, eu sei, mas é pro IE 7/8/9 não reclamarem da sintaxe
                    }
                }
            }

            // Para a coluna de ações
            if (renderizaAcoes)
            {
                header.AppendLine("{ 'bSortable': false, 'bSearchable': false }");

                if (viewData.withColumnFilter)
                    footer.AppendLine("null");
            }

            string columnFilter;

            if (!string.IsNullOrEmpty(footer.ToString()))
                columnFilter = @".columnFilter({ aoColumns: [ " + footer.ToString() + @" ]})";
            else
                columnFilter = "";

            string typeSortingStr = (typeSorting == DataTableSortDirection.Ascending) ? "asc" : "desc";

            /*
             * Renderiza as propriedades customizadas do DataTables
             */
            StringBuilder renderProperties = new StringBuilder();

            foreach (var property in viewData.renderProperties)
            {
                string format = "'{0}': {1},";

                if (property.Value.GetType().Namespace != "System")
                {
                    renderProperties.AppendFormat(format, property.Key, JsonConvert.SerializeObject(property.Value));
                    continue;
                }

                string value = property.Value.ToString();

                if (property.Value is String)
                    if (!property.Value.ToString().Contains("function"))
                        format = "'{0}': '{1}',";

                if (property.Value is Boolean)
                    value = value.ToLower();

                renderProperties.AppendFormat(format, property.Key, value);
            }

            string s =
              @"<link rel='Stylesheet' href='" + urlHelper.Content("~/Content/css/dataTables/jquery.datatables.css") + @"'/>
              <script type='text/javascript' src='" + urlHelper.Content("~/Scripts/dataTables/jquery.datatables.min.js") + @"'></script>
			  <script type='text/javascript' src='" + urlHelper.Content("~/Scripts/dataTables/jquery.datatables.columnfilter.js") + @"'></script>
			  <script type='text/javascript' src='" + urlHelper.Content("~/Scripts/dataTables/jquery.datatables.filterdelay.js") + @"'></script>
			  <script type='text/javascript' src='" + urlHelper.Content("~/Scripts/dataTables/jquery.datatables.ajaxpipelining.js") + @"'></script>
			  <script type='text/javascript' src='" + urlHelper.Content("~/Scripts/dataTables/jquery.datatables.bootstrap.js") + @"'></script>
			  <script type='text/javascript'>
				$(function() {
				  $.extend( $.fn.dataTableExt.oStdClasses, {
					'sWrapper': 'dataTables_wrapper form-inline'
				  });
				  $('table#datatable_id_" + viewData.name + @"').dataTable({" + renderProperties.ToString() +
                        @"'bProcessing': true,
					'bServerSide': true,
					'sAjaxSource': '" + url + @"',
					'sPaginationType': 'bootstrap',
					'sDom': " + "\"<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>\"," + @"
					'aoColumns': [" + header.ToString() + @"],
					'fnServerData': fnDataTablesPipeline,
					'aaSorting': [[ " + columnSorted.ToString() + @", '" + typeSortingStr + @"' ]],
					'oLanguage': { 
						'sProcessing': '',
						'sLengthMenu': 'Mostrar _MENU_ registros',
						'sZeroRecords': 'N&atilde;o foram encontrados resultados',
						'sInfo': 'Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros',
						'sInfoEmpty': 'Mostrando de 0 at&eacute; 0 de 0 registros',
						'sInfoFiltered': '(filtrado de _MAX_ registros no total)',
						'sInfoPostFix': '',
						'sSearch': '',
						'sUrl': '',
						'oPaginate': {
							'sFirst': 'Primeiro',
							'sPrevious': 'Anterior',
							'sNext': 'Pr&oacute;ximo',
							'sLast': '&Uacute;ltimo'
						}
					}
				  })" + columnFilter + @".fnSetFilteringDelay();      
				});
			  </script>";
            return MvcHtmlString.Create(s);
        }
        #endregion
    }
}
