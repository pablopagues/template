﻿using System;
using System.IO;
using System.Net;

namespace Sistema.Helpers
{
    public static class UrlHelpers
    {
        public static string AbreUrl(string url)
        {
            string contents = "";

            try
            {
                // Cria um objeto URL para receber o endereço do site
                Uri myUrl = new Uri(url);
                // Cria uma requisição para a URL informada
                WebRequest myWebRequest = WebRequest.Create(myUrl);
                // Associa o WebRequest à um WebResponse para receber as respostas
            
                WebResponse myWebResponse = myWebRequest.GetResponse();
                //Lê a resposta
                System.IO.Stream stream = myWebResponse.GetResponseStream();
                // Declara uma string para receber o html
                
                // Preenche a String
                using (StreamReader reader = new StreamReader(stream))
                {
                    contents = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                return ex.Message;
            }

            return contents;
        }
    }
}