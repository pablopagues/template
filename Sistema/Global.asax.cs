﻿using Infrastructure.Data;
using Infrastructure.Domain.Models;
using Sistema.App_Start;
using Sistema.Helpers.Binders;
using Sistema.Helpers.DataTable;
using System;
using System.Data.Entity;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Sistema
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Registra os helpers do DataTable
            ModelBinders.Binders[typeof(DataTablePostData)] = new DataTableModelBinder();
            ModelBinders.Binders[typeof(DateTime?)] = new DateTimeBinderBR<DateTime?>();
            ModelBinders.Binders[typeof(DateTime)] = new DateTimeBinderBR<DateTime>();

            AutoMapperBootstrap.Init(Assembly.GetExecutingAssembly());

            Database.SetInitializer<MVCContext>(null);
            //Database.SetInitializer(new DBContextInitializer());

            AuthConfig.RegisterAuth();
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                string encTicket = authCookie.Value;
                if (!String.IsNullOrEmpty(encTicket))
                {
                    var ticket = FormsAuthentication.Decrypt(encTicket);
                    var id = new Usuario(ticket);
                    var userRoles = Roles.GetRolesForUser(id.Name);
                    var prin = new GenericPrincipal(id, userRoles);
                    HttpContext.Current.User = prin;
                }
            }
        }

    }
}