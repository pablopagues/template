﻿/// <reference path="jquery-1.6.2-vsdoc.js" />
/**
* Script para aumentar o delay da busca no search do datatables.
* O objetivo é diminuir o nível de requisições a cada tecla pressionada e evitar sobrecarregar o servidor.
*/

jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function(oSettings, iDelay) {
  /*
  * Inputs:      object:oSettings - dataTables settings object - automatically given
  *              integer:iDelay - delay in milliseconds
  * Usage:       $('#example').dataTable().fnSetFilteringDelay(250);
  * Author:      Zygimantas Berziunas (www.zygimantas.com) and Allan Jardine
  * License:     GPL v2 or BSD 3 point style
  * Contact:     zygimantas.berziunas /AT\ hotmail.com
  */
  var 
		_that = this,
		iDelay = (typeof iDelay == 'undefined') ? 500 : iDelay;

  this.each(function(i) {
    $.fn.dataTableExt.iApiIndex = i;
    var 
			$this = this,
			oTimerId = null,
			sPreviousSearch = null,
			generalControl = anControl = $('input', _that.fnSettings().aanFeatures.f), // General search control
      columnControls = $('.dataTables_wrapper table input'), // Column search controls
      sPreviousColumnsSearches = new Array();

    /* First, we populate the previous searchs array to know when its get updated */
    for (var j = 0; j < columnControls.length; j++) {
      sPreviousColumnsSearches.push($(columnControls[j]).val());
    }

    generalControl.unbind('keyup keypress')
    .bind('keyup', function() {
      var $$this = $this;

      if (sPreviousSearch === null || sPreviousSearch != generalControl.val()) {
        window.clearTimeout(oTimerId);
        sPreviousSearch = generalControl.val();
        oTimerId = window.setTimeout(function() {
          $.fn.dataTableExt.iApiIndex = i;
          _that.fnFilter(generalControl.val());
        }, iDelay);
      }
    });
    //    .bind('keypress', function(key) {
    //      if (key.which == 13) {
    //        key.preventDefault();
    //        $.fn.dataTableExt.iApiIndex = i;
    //        _that.fnFilter($(this).val());
    //      }
    //    });

    columnControls.unbind('keyup keypress')
    .bind('keyup', function() {
      var anControlColumnSearch = $(this);
      var allControls = $('.dataTables_wrapper table input'); // Vector of controls
      var filterIndex = $.inArray(anControlColumnSearch[0], allControls);
      var haveAnyChanges = sPreviousColumnsSearches[filterIndex] === null;

      /* We go through all the inputs and test if at least only of them had any changes */
      for (var j = 0; j < columnControls.length; j++) {
        var value = $(columnControls[j]).val();
        haveAnyChanges = haveAnyChanges || (sPreviousColumnsSearches[j] != value);
      }

      /* If yes... well, this line is self explicative :P */
      if (haveAnyChanges) {
        /* We update our last value */
        sPreviousColumnsSearches[filterIndex] = anControlColumnSearch.val();
        window.clearTimeout(oTimerId);
        oTimerId = window.setTimeout(function() {
          $.fn.dataTableExt.iApiIndex = i;
          _that.fnFilter(anControlColumnSearch.val(), filterIndex);
        }, iDelay);
      }
    });
    //    .bind('keypress', function(key) {
    //      var anControlColumnSearch = $(this);
    //      var allControls = $('.dataTables_wrapper table input'); // Vector of controls
    //      var filterIndex = $.inArray(anControlColumnSearch[0], allControls);
    //      
    //      if (key.which == 13) {
    //        key.preventDefault();
    //        $.fn.dataTableExt.iApiIndex = i;
    //        _that.fnFilter(anControlColumnSearch.val(), filterIndex);
    //      }
    //    });

    return this;
  });
  return this;
}