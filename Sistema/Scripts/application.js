﻿/// <reference path="_references.js" />

/**
* Select2 Brazilian Portuguese translation
*/
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () { return "Nenhum resultado encontrado"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Informe " + n + " caracter" + (n == 1 ? "" : "es"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Apague " + n + " caracter" + (n == 1 ? "" : "es"); },
        formatSelectionTooBig: function (limit) { return "Só é possível selecionar " + limit + " elemento" + (limit == 1 ? "" : "s"); },
        formatLoadMore: function (pageNumber) { return "Carregando mais resultados..."; },
        formatSearching: function () { return "Buscando..."; }
    });
})(jQuery);

/**
* jQuery.browser.mobile (http://detectmobilebrowser.com/)
* jQuery.browser.mobile will be true if the browser is a mobile device
**/
(function (a) { jQuery.browser = Object; jQuery.browser.mobile = /android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)) })(navigator.userAgent || navigator.vendor || window.opera)

/**
* Recursos gerais do layout.
*/
$(function () {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };

    //loading
    $('button[data-loading-text]').on("click", function () {
        $(this).button('loading');
    });

    //select2
    try {
        if (!$.browser.mobile) {
            $('select[data-behaviour="select2"]').each(function (i, el) {
                var $this = $(el); var required = false
                if ($this.data('allowclear')) required = true;
                $this.select2({
                    width: 'off',
                    allowClear: required
                });
            });
        }
    } catch (err) {
        /* Erro */
    }

    //collection
    $('select[data-collection]').change(function () {
        var $this = $(this);
        var value = $this.val();
        var $collection = $($this.data('collection'));
        var $select = $('select', $collection);

        if (!value) {
            //TODO: verificar isso
            $select.empty().attr('disable', true);
            $collection.fadeOut();
        } else {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                data: { id: value },
                url: $select.data('collection-url'),
                beforeSend: function () {
                    $collection.fadeOut('fast');
                    if (!$.browser.mobile) {
                        $select.empty().select2('destroy');
                    }

                    //estrutura da arvore recursiva de dependencia
                    //OBS: se você não sabe estrutura de dados e recursividade nem olhe esse código abaixo u.u
                    var $select_dependent = $select;
                    while ($select_dependent.data('collection')) {
                        var $collection_dependent = $($select_dependent.data('collection'));
                        $select_dependent = $('select', $collection_dependent);

                        $select_dependent.empty().attr('disable', true);
                        $collection_dependent.hide();
                    }
                    loading(true);
                }
            })
            .done(function (data) {
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        $select.append('<option value="' + data[i].Value + '">' + data[i].Text + '</option>');
                    }
                    if (!$.browser.mobile) {
                        $select.select2();
                    }
                    $collection.fadeIn('fast');
                }
            })
            .always(function () {
                loading(false);
            });
        }
    });

    //tooltip
    try {
        $('[data-toggle="tooltip"]').tooltip();
    } catch (err) {
        /* Erro */
    }

    //popover
    try {
        $('[data-toggle="popover"]').popover();
    } catch (err) {
        /* Erro */
    }

    //fancybox
    try {
        $('a[data-fancybox]').fancybox();
    } catch (err) {
        /* Erro */
    }

    //mask
    try {
        $('input[data-mask]').each(function (i, el) {
            var $this = $(el);
            var mask = $this.attr('data-mask');
            if (mask == 'money') {
                $this.maskMoney({ thousands: '', decimal: ',' });
            }
        });
    } catch (err) {
        /* Erro */
    }

    //image lazyload
    try {
        $('img[data-src]').lazy();
    } catch (err) {
        /* Erro */
    }

    //datepicker
    try {
        if (!$.browser.mobile) {
            $('[data-behaviour="datepicker"]').datepicker({
                format: "dd/mm/yyyy"
            });
        }
    } catch (err) {
        if (console) console.log(err);
    }

    ////datepicker
    //try {
    //    if (!$.browser.mobile) {
    //        $('[data-behaviour="datepicker"]').datepicker({
    //            format: "dd/mm/yyyy"
    //        });
    //    }
    //} catch (err) {
    //    /* Erro */
    //}

    //buttons-radio
    $('div.btn-group[data-toggle-element]').each(function () {
        var $group = $(this);
        var form = $group.parents('form').eq(0);
        var $hidden = $('#' + $group.attr('data-toggle-element'), form);
        $(':radio', $group).each(function (i, e) {
            var $radio = $(e);
            $radio.parent().removeClass('active');
            $radio.parent().on('click', function () {
                $hidden.val($radio.val());
            });
            if ($radio.val() == $hidden.val()) {
                $radio.parent().addClass('active');
            }
        });
    });

    //FULLCALENDAR 
    var selectedDate = new Date();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        lang: 'pt-br',
        defaultView: 'month',
        editable: true,
        events: "/Home/GetEvents/",
        dayClick: function (date, allDay, jsEvent, view) {
            $('#eventTitle').val("");
            $('#eventDate').val($.fullCalendar.formatDate(date, 'dd/MM/yyyy'));
            $('#eventTime').val($.fullCalendar.formatDate(date, 'HH:mm'));
            $('#eventDuration').val("");
            $('#eventLocal').val("");
            ShowEventPopup(date);
        },
        eventResize: function (event, dayDelta, minuteDelta, revertFunc) {

            if (confirm("Confirma?")) {
                UpdateEvent(event.id, event.start, event.end);
            }
            else {
                revertFunc();
            }
        },
        eventDrop: function (event, delta, revertFunc) {

            if (!confirm("Confirma alteração de agenda?")) {
                revertFunc();
            }
            else {
                //alert("aqui post " + event.start.format() + " / " + event.id);
                $.post("/Home/PostEvent/",{
                    days: event.start.format(),
                    minutes: 60,
                    id: event.id
                }).fail(function () {
                    alert("error");
                });
            }
        }

    });

    $('#btnPopupCancel').click(function () {
        ClearPopupFormValues();
        $('#popupEventForm').hide();
    });

    $('#btnPopupSave').click(function () {

        $('#popupEventForm').hide();

        var dataRow = {
            'Title': $('#eventTitle').val(),
            'NewEventDate': $('#eventDate').val(),
            'NewEventTime': $('#eventTime').val(),
            'NewEventDuration': $('#eventDuration').val(),
            'NewEventLocal': $('#eventLocal').val()
        }

        ClearPopupFormValues();

        $.ajax({
            type: 'POST',
            url: "/Home/PostNovoEvent",
            data: dataRow,
            success: function (response) {
                if (response == 'True') {
                    $('#calendar').fullCalendar('refetchEvents');
                    alert('Novo compromisso salvo!');
                }
                else {
                    alert('Erro, A agenda não pode ser salva!');
                }
            }
        });
    });

    function UpdateEvent(EventID, EventStart, EventEnd) {
        alert('opa update');
        var dataRow = {
            'ID': EventID,
            'NewEventStart': EventStart,
            'NewEventEnd': EventEnd
        }

        console.log(JSON.stringify(dataRow));

        $.ajax({
            type: 'POST',
            url: "/Home/UpdateEvent",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(dataRow)
        });
    }

    function ShowEventPopup(date) {
        ClearPopupFormValues();
        $('#popupEventForm').show();
        $('#eventTitle').focus();
    }

    function ClearPopupFormValues() {
        $('#eventID').val("");
        $('#eventTitle').val("");
        $('#eventDateTime').val("");
        $('#eventDuration').val("");
        $('#eventLocal').val("");
    }

});

/**
* Modal
*/
function alerta(texto) {
    $obj = $('#modal');
    $('.modal-body', $obj).html(texto);
    $obj.modal();
}

function loading(bool) {
    if (bool) {
        $('#overlay').show();
        $('#ajax-loading').show();
    } else {
        $('#overlay').hide();
        $('#ajax-loading').hide();
    }
}

/**
* Configurações padrões do ajax.
*/
$(document).ajaxSend(function (event, jqXHR, settings) {
    console.log('before send');
    loading(true);
});

$(document).ajaxComplete(function (event, jqXHR, settings) {
    console.log('complete');
    $('button[data-loading-text]').button('reset');
    loading(false);
});