using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class FaturamentoAlunoController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        private DataTable<FaturamentoAluno> dataTable = new DataTable<FaturamentoAluno>();

        public FaturamentoAlunoController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.aluno.nome)
                .addColumn(x => x.Descricao)
                .addColumn(x => x.QtdHoras)
                .addColumn(x => x.ValorHora)
                .addColumn(x => x.Data);
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<FaturamentoAluno>().AsQueryable(), dataTablePostData);
        }
        
        //
        // GET: /FaturamentoAluno/

        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /FaturamentoAluno/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<FaturamentoAluno>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            FaturamentoAlunoViewModelMapper mapper = new FaturamentoAlunoViewModelMapper(genericRepositorio);
            FaturamentoAlunoViewModel model = mapper.exportaViewModel(new FaturamentoAluno());

            model.Data = DateTime.Now;
            return View(model);
        }

        //
        // POST: /FaturamentoAluno/Create
        [HttpPost]
        public ActionResult Create(FaturamentoAlunoViewModel item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    FaturamentoAlunoViewModelMapper mapper = new FaturamentoAlunoViewModelMapper(genericRepositorio);
                    FaturamentoAluno FaturamentoAluno = mapper.importaViewModel(item);

                    FaturamentoAluno.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                    FaturamentoAluno.IdAluno = FaturamentoAluno.aluno.id;

                    genericRepositorio.Adicionar<FaturamentoAluno>(FaturamentoAluno);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    informaErro("Erro ao salvar. Verifique os dados digitados.");
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /FaturamentoAluno/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                FaturamentoAlunoViewModelMapper mapper = new FaturamentoAlunoViewModelMapper(genericRepositorio);

                FaturamentoAluno u = genericRepositorio.EncontrarPor<FaturamentoAluno>(p => p.aluno).Where(p => p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /FaturamentoAluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FaturamentoAlunoViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FaturamentoAlunoViewModelMapper mapper = new FaturamentoAlunoViewModelMapper(genericRepositorio);
                    FaturamentoAluno FaturamentoAluno = mapper.importaViewModel(item);

                    FaturamentoAluno.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                    FaturamentoAluno.IdAluno = FaturamentoAluno.aluno.id;
                    genericRepositorio.UnitOfWork.BeginTransaction();

                    genericRepositorio.Atualizar<FaturamentoAluno>(FaturamentoAluno);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    informaErro("Erro ao salvar. Verifique os dados digitados.");
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /FaturamentoAluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPorID<FaturamentoAluno>(id));
        }

        //
        // POST: /FaturamentoAluno/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FaturamentoAluno u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<FaturamentoAluno>(genericRepositorio.EncontrarPorID<FaturamentoAluno>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
