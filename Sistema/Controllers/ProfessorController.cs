using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class ProfessorController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        private DataTable<Professor> dataTable = new DataTable<Professor>();

        public ProfessorController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.Nome)
                .addColumn(x => x.Endereco)
                .addColumn(x => x.Telefone)
                .addColumn(x => x.Celular)
                .addColumn(x => x.email)
                .addColumn(x => x.Login);
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<Professor>().AsQueryable(), dataTablePostData);
        }
        
        //
        // GET: /Professor/

        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /Professor/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<Professor>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            ProfessorViewModelMapper mapper = new ProfessorViewModelMapper();
            ProfessorViewModel model = mapper.exportaViewModel(new Professor());
            model.DataNascimento = DateTime.Now;
            return View(model);
        }

        //
        // POST: /Professor/Create
        [HttpPost]
        public ActionResult Create(ProfessorViewModel item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    ProfessorViewModelMapper mapper = new ProfessorViewModelMapper();
                    Professor Professor = mapper.importaViewModel(item);

                    genericRepositorio.Adicionar<Professor>(Professor);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Professor/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                ProfessorViewModelMapper mapper = new ProfessorViewModelMapper();

                Professor u = genericRepositorio.EncontrarPor<Professor>().Where(p => p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Professor/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProfessorViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ProfessorViewModelMapper mapper = new ProfessorViewModelMapper();
                    Professor Professor = mapper.importaViewModel(item);

                    genericRepositorio.UnitOfWork.BeginTransaction();

                    genericRepositorio.Atualizar<Professor>(Professor);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {

                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Professor/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPorID<Professor>(id));
        }

        //
        // POST: /Professor/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Professor u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<Professor>(genericRepositorio.EncontrarPorID<Professor>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
