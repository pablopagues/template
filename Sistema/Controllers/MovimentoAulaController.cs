using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Models.Escola;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador", "Professor", "Coordenador")]
    public class MovimentoAulaController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        private DataTable<MovimentoAula> dataTable = new DataTable<MovimentoAula>();

        public MovimentoAulaController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.aluno.nome)
                .addColumn(x => x.Descricao)
                .addColumn(x => x.HoraInicio.ToShortTimeString(), "Hora Inicio").setWidth("10%").setCssClass("text-center")
                .addColumn(x => x.HoraFinal.ToShortTimeString(), "Hora Fim").setWidth("10%").setCssClass("text-center")
                .addColumn(x => x.QtdHoras, "Qtd. Horas").setWidth("8%").setCssClass("text-center")
                .addColumn(x => x.professor.Nome).setWidth("20%")
                .addColumn(x => x.Grupo).setWidth("8%").setCssClass("text-center")
                .addColumn(x => x.DataMovimento, "Data Aula");
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            if (User.IsInRole("Administrador") || User.IsInRole("Coordenador"))
            {
                var movimento = genericRepositorio.Tudo<MovimentoAula>().OrderBy(p => p.DataMovimento).AsQueryable();
                return dataTable.getDataTableResultEF(ControllerContext, movimento, dataTablePostData);
            }
            else if (User.IsInRole("Professor"))
            {
                int usuarioid = Convert.ToInt16(Session["usuarioId"]);
                //var usuario = genericRepositorio.EncontrarPor<Usuario>(x => x.id == usuarioid).IncludeMultiple(p=>p.professor).FirstOrDefault();
                var usuario = genericRepositorio.Tudo<Usuario>(p => p.professor).Where(x => x.id == usuarioid).FirstOrDefault();
                return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<MovimentoAula>().Where(p => p.professor.id == usuario.professor.id).OrderByDescending(p => p.DataMovimento).AsQueryable().AsQueryable(), dataTablePostData);
            }
            else
            {
                return null;
            }
        }

        //
        // GET: /MovimentoAula/

        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /MovimentoAula/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<MovimentoAula>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            MovimentoAulaViewModelMapper mapper = new MovimentoAulaViewModelMapper(genericRepositorio);
            MovimentoAulaViewModel model = mapper.exportaViewModel(new MovimentoAula());

            model.DataMovimento = DateTime.Now;
            return View(model);
        }

        //
        // POST: /MovimentoAula/Create
        [HttpPost]
        public ActionResult Create(MovimentoAulaViewModel item)
        {
            try
            {
                var aulasconsumidas = genericRepositorio.Tudo<MovimentoAula>().Where(p => p.aluno.id == item.idAluno).Sum(p => p.QtdHoras);
                var aulasdisponiveis = genericRepositorio.Tudo<FaturamentoAluno>().Where(p => p.aluno.id == item.idAluno).Sum(p => p.QtdHoras);
                decimal aulas = aulasdisponiveis - aulasconsumidas;
                if (aulas > 0)
                {
                    if (ModelState.IsValid)
                    {
                        
                        MovimentoAulaViewModelMapper mapper = new MovimentoAulaViewModelMapper(genericRepositorio);
                        MovimentoAula MovimentoAula = mapper.importaViewModel(item);

                        MovimentoAula.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                        //MovimentoAula.IdAluno = MovimentoAula.Aluno.id;
                        MovimentoAula.professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.idProfessor).FirstOrDefault();
                        //MovimentoAula.IdProfessor = MovimentoAula.Professor.id;


                        int grupo = ValidarGrupoMetodo(MovimentoAula.DataMovimento, MovimentoAula.HoraInicio, MovimentoAula.professor.id, MovimentoAula.aluno.id);
                        if (grupo == 0 || grupo == -1 || MovimentoAula.Grupo < grupo)
                        {
                            item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                            item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                            informaErro("Erro ao salvar. Verifique o grupo digitado.");
                            return View(item);
                        }

                        genericRepositorio.UnitOfWork.BeginTransaction();
                        genericRepositorio.Adicionar<MovimentoAula>(MovimentoAula);
                        genericRepositorio.UnitOfWork.CommitTransaction();

                        informaSucesso("Registro salvo com sucesso!");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                        item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                        informaErro("Erro ao salvar. Verifique os dados digitados.");
                        return View(item);
                    }
                }
                else
                {
                    item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                    informaErro("Erro ao salvar. Aluno n�o tem aulas disponiveis.");
                    return View(item);
                }
                    
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /MovimentoAula/Edit/5
        [AuthorizeRoles("Administrador", "Coordenador")]
        public ActionResult Edit(int id)
        {
            try
            {
                MovimentoAulaViewModelMapper mapper = new MovimentoAulaViewModelMapper(genericRepositorio);

                MovimentoAula u = genericRepositorio.EncontrarPor<MovimentoAula>(p => p.aluno, p => p.professor).Where(p => p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /MovimentoAula/Edit/5
        [AuthorizeRoles("Administrador", "Coordenador")]
        [HttpPost]
        public ActionResult Edit(int id, MovimentoAulaViewModel item)
        {
            try
            {
                var aulasconsumidas = genericRepositorio.Tudo<MovimentoAula>().Where(p => p.aluno.id == item.idAluno).Sum(p => p.QtdHoras);
                var aulasdisponiveis = genericRepositorio.Tudo<FaturamentoAluno>().Where(p => p.aluno.id == item.idAluno).Sum(p => p.QtdHoras);
                decimal aulas = aulasdisponiveis - aulasconsumidas;

                if (aulas > 0)
                {
                    if (ModelState.IsValid)
                    {
                        MovimentoAulaViewModelMapper mapper = new MovimentoAulaViewModelMapper(genericRepositorio);
                        MovimentoAula MovimentoAula = mapper.importaViewModel(item);

                        MovimentoAula.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                        //MovimentoAula.IdAluno = MovimentoAula.Aluno.id;
                        MovimentoAula.professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.idProfessor).FirstOrDefault();
                        //MovimentoAula.IdProfessor = MovimentoAula.Professor.id;

                        int grupo = ValidarGrupoMetodo(MovimentoAula.DataMovimento,MovimentoAula.HoraInicio,MovimentoAula.professor.id,MovimentoAula.aluno.id);
                        if ( grupo == 0 || grupo == -1 || MovimentoAula.Grupo < grupo)
                        {
                            item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                            item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                            informaErro("Erro ao salvar. Verifique o grupo digitado.");
                            return View(item);
                        }
                        
                        genericRepositorio.UnitOfWork.BeginTransaction();

                        genericRepositorio.Atualizar<MovimentoAula>(MovimentoAula);
                        genericRepositorio.UnitOfWork.CommitTransaction();

                        informaSucesso("Registro salvo com sucesso!");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                        item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                        informaErro("Erro ao salvar. Verifique os dados digitados.");
                        return View(item);
                    }
                }
                else
                {
                    item.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    item.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                    informaErro("Erro ao salvar. Aluno n�o tem aulas disponiveis.");
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /MovimentoAula/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPorID<MovimentoAula>(id));
        }

        //
        // POST: /MovimentoAula/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, MovimentoAula u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<MovimentoAula>(genericRepositorio.EncontrarPorID<MovimentoAula>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        public int ValidarGrupoMetodo(DateTime data, DateTime horainicio, int IdProfessor, int IdAluno)
        {
            try
            {
                //1 - nao pode abrir o grupo 2 se no mesmo horario tem o grupo 1 aberto
                //saber a hora do final do ultimo grupo do professor para saber se o aluno iria neste grupo ou no proximo
                MovimentoAula grupo = genericRepositorio.Tudo<MovimentoAula>(p => p.professor).OrderByDescending(p => p.Grupo)
                                                                    .Where(p => p.professor.id == IdProfessor)
                                                                    .Where(p => p.DataMovimento == data)
                                                                    .FirstOrDefault();

                int grupoDoALuno = 0;

                //primeiro grupo do dia para esse professor
                if (grupo == null)
                    return 1;
                else
                    grupoDoALuno = grupo.Grupo;

                if (grupo.Grupo == -1 || grupo.Grupo == 0)
                    grupoDoALuno = 1;

                //retorna grupo novo ou retorna o grupo aberto
                int final = Convert.ToInt32(grupo.HoraFinal.Hour.ToString() + (grupo.HoraFinal.Minute != 0 ? grupo.HoraFinal.Minute.ToString() : "00"));
                int inicio = Convert.ToInt32(horainicio.Hour.ToString() + (horainicio.Minute != 0 ? horainicio.Minute.ToString() : "00"));
                int g = 0;
                if (final < inicio)
                    g = grupoDoALuno + 1;
                else
                    g = grupoDoALuno;

                //2 - nao pode ter o mesmo aluno no mesmo grupo para o mesmo professor
                var aluno = genericRepositorio.Tudo<MovimentoAula>().Where(p => p.professor.id == IdProfessor)
                                                                  .Where(p => p.DataMovimento == data)
                                                                  .Where(p => p.Grupo == g)
                                                                  .Where(p => p.aluno.id == IdAluno)
                                                                  .FirstOrDefault();
                if (aluno != null)
                    grupoDoALuno = -1;

                return g;
            }
            catch (Exception)
            {
                return 0;
            }
            
        }

        //validar grupo
        public JsonResult ValidarGrupo(DateTime data, DateTime horainicio,Int16 IdProfessor, Int16 IdAluno)
        {
            try
            {
                //1 - nao pode abrir o grupo 2 se no mesmo horario tem o grupo 1 aberto
                //saber a hora do final do ultimo grupo do professor para saber se o aluno iria neste grupo ou no proximo
                MovimentoAula grupo = genericRepositorio.Tudo<MovimentoAula>(p => p.professor).OrderByDescending(p => p.Grupo)
                                                                    .Where(p => p.professor.id == IdProfessor)
                                                                    .Where(p => p.DataMovimento == data)
                                                                    .FirstOrDefault();

                int grupoDoALuno = 0;

                //primeiro grupo do dia para esse professor
                if (grupo == null)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    grupoDoALuno = grupo.Grupo;

                if (grupo.Grupo == -1 || grupo.Grupo == 0)
                    grupoDoALuno = 1;

                //retorna grupo novo ou retorna o grupo aberto
                int final = Convert.ToInt32(grupo.HoraFinal.Hour.ToString() + (grupo.HoraFinal.Minute != 0 ? grupo.HoraFinal.Minute.ToString() : "00"));
                int inicio = Convert.ToInt32(horainicio.Hour.ToString() + (horainicio.Minute != 0 ? horainicio.Minute.ToString() : "00"));
                int g = 0;
                if (final < inicio)
                    g = grupoDoALuno + 1;
                else
                    g = grupoDoALuno;

                //2 - nao pode ter o mesmo aluno no mesmo grupo para o mesmo professor
                var aluno = genericRepositorio.Tudo<MovimentoAula>().Where(p => p.professor.id == IdProfessor)
                                                                  .Where(p => p.DataMovimento == data)
                                                                  .Where(p => p.Grupo == g)
                                                                  .Where(p => p.aluno.id == IdAluno)
                                                                  .FirstOrDefault();
                if (aluno != null)
                    grupoDoALuno = -1;
                else
                    grupoDoALuno = g;

                return Json(grupoDoALuno, JsonRequestBehavior.AllowGet);
                    
            }
            catch (Exception)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
        }

        //validar aula
        public JsonResult ValidarAula(Int16 IdAluno)
        {
            try
            {

                var aulasconsumidas = genericRepositorio.Tudo<MovimentoAula>().Where(p => p.aluno.id == IdAluno).Sum(p => p.QtdHoras);
                var aulasdisponiveis = genericRepositorio.Tudo<FaturamentoAluno>().Where(p => p.aluno.id == IdAluno).Sum(p => p.QtdHoras);                        

                decimal aulas = aulasdisponiveis-aulasconsumidas;

                if(aulas > 0)
                    return Json(1, JsonRequestBehavior.AllowGet);
                else
                    return Json(-1, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
