using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Models.Escola;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador", "Professor", "Coordenador")]
    public class AgendaAulaController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        private DataTable<AgendaAula> dataTable = new DataTable<AgendaAula>();

        public AgendaAulaController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.aluno.nome)
                .addColumn(x => x.Descricao)
                .addColumn(x => x.Hora)
                .addColumn(x => x.professor.Nome)
                .addColumn(x => x.Data);
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            if (User.IsInRole("Administrador") || User.IsInRole("Coordenador"))
            {
                return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<AgendaAula>().AsQueryable(), dataTablePostData);
            }
            else if (User.IsInRole("Professor"))
            {
                int usuarioid = Convert.ToInt16(Session["usuarioId"]);
                //var usuario = genericRepositorio.EncontrarPor<Usuario>(x => x.id == usuarioid).IncludeMultiple(p=>p.professor).FirstOrDefault();
                var usuario = genericRepositorio.Tudo<Usuario>(p => p.professor).Where(x => x.id == usuarioid).FirstOrDefault();

                return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<AgendaAula>().Where(p => p.professor.id == usuario.professor.id).AsQueryable(), dataTablePostData);
            }
            else
            {
                return null;
            }
        }

        //
        // GET: /AgendaAula/

        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /AgendaAula/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<AgendaAula>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            AgendaAulaViewModelMapper mapper = new AgendaAulaViewModelMapper(genericRepositorio);
            AgendaAulaViewModel model = mapper.exportaViewModel(new AgendaAula());

            model.Data = DateTime.Now;
            return View(model);
        }

        //
        // POST: /AgendaAula/Create
        [HttpPost]
        public ActionResult Create(AgendaAulaViewModel item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    AgendaAulaViewModelMapper mapper = new AgendaAulaViewModelMapper(genericRepositorio);
                    AgendaAula AgendaAula = mapper.importaViewModel(item);

                    AgendaAula.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                    //AgendaAula.IdAluno = AgendaAula.Aluno.id;
                    AgendaAula.professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.idProfessor).FirstOrDefault();
                    //AgendaAula.IdProfessor = AgendaAula.Professor.id;

                    genericRepositorio.Adicionar<AgendaAula>(AgendaAula);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /AgendaAula/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                AgendaAulaViewModelMapper mapper = new AgendaAulaViewModelMapper(genericRepositorio);

                AgendaAula u = genericRepositorio.EncontrarPor<AgendaAula>(p => p.aluno, p => p.professor).Where(p => p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /AgendaAula/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AgendaAulaViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AgendaAulaViewModelMapper mapper = new AgendaAulaViewModelMapper(genericRepositorio);
                    AgendaAula AgendaAula = mapper.importaViewModel(item);

                    AgendaAula.aluno = genericRepositorio.EncontrarPor<Aluno>(x => x.id == item.idAluno).FirstOrDefault();
                    //AgendaAula.IdAluno = AgendaAula.Aluno.id;
                    AgendaAula.professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.idProfessor).FirstOrDefault();
                    //AgendaAula.IdProfessor = AgendaAula.Professor.id;
                    genericRepositorio.UnitOfWork.BeginTransaction();

                    genericRepositorio.Atualizar<AgendaAula>(AgendaAula);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {

                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /AgendaAula/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPorID<AgendaAula>(id));
        }

        //
        // POST: /AgendaAula/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, AgendaAula u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<AgendaAula>(genericRepositorio.EncontrarPorID<AgendaAula>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
