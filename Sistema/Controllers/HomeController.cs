﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sistema.Controllers;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.Controllers
{
    public class HomeController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        public HomeController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

        }

        //
        // GET: /Home/
        public ActionResult Index()
        {
            string cumprimento;
            string nomeUsuario = "Colaborador";
            Usuario usuarioLogado;

            ViewBag.AbriuOcorrencia = false;
            ViewBag.RecebeuOcorrencia = false;

            if (!String.IsNullOrEmpty(User.Identity.Name) && !String.IsNullOrEmpty(Request.Cookies["sistema-nome"].Value))
            {
                usuarioLogado = Session["usuario"] as Usuario;
                nomeUsuario = Request.Cookies["sistema-nome"].Value;

                var lst = genericRepositorio.Tudo<Aluno>().Where(p => p.dtNascimento.Day == DateTime.Now.Day && p.dtNascimento.Month == DateTime.Now.Month).ToList();
                ViewBag.lstAniversariantes = genericRepositorio.Tudo<Aluno>().Where(p => p.dtNascimento.Day == DateTime.Now.Day && p.dtNascimento.Month == DateTime.Now.Month).ToList();
            }
            else
            {
                return RedirectToAction("Login", "Conta");
            }

            if (DateTime.Now.Hour > 0 && DateTime.Now.Hour <= 12)
                cumprimento = "Bom dia ";
            else if (DateTime.Now.Hour > 12 && DateTime.Now.Hour < 18)
                cumprimento = "Boa tarde ";
            else
                cumprimento = "Boa noite ";

            ViewData["saudacao"] = cumprimento + nomeUsuario + "!";

            return View();
        }


        public JsonResult GetEvents() //double start, double end)
        {

            //var DataDe = ConvertFromUnixTimestamp(start);
            //var DataAte = ConvertFromUnixTimestamp(end);

            //Tornando padrão a exibição somente do professor caso não seja um dos admins ou coordenadores.
            int usuarioid = Convert.ToInt16(Session["usuarioId"]);
            List<AgendaAula> events = new List<AgendaAula>();

            if (usuarioid > 0)
            {
                //var lst = genericRepositorio.Tudo<Usuario>(p => p.professor).Where(m => m.professor.id == usuarioid).ToList();
                events = genericRepositorio.Tudo<AgendaAula>(p => p.professor).Where(m => m.professor.id == usuarioid).OrderBy(x => x.Hora).ToList();
            }
            if (User.IsInRole("Administrador") || User.IsInRole("Coordenador"))
                events = genericRepositorio.Tudo<AgendaAula>().OrderBy(x => x.Hora).ToList();

            var eventList = from e in events
                            select new
                            {
                                id = e.id,
                                title = e.Descricao + " - " + e.aluno.nome,
                                description = e.Descricao,
                                start = e.Hora,
                                end = e.Hora.AddMinutes(60), 
                                allDay = false
                            };

            var rows = eventList.ToArray();

            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        // data POST service
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostEvent(string days, string minutes, string id)
        {
            AgendaAula age = genericRepositorio.EncontrarPorID<AgendaAula>(Convert.ToInt32(id));

            age.Hora = Convert.ToDateTime(days);
            genericRepositorio.UnitOfWork.BeginTransaction();

            genericRepositorio.Atualizar<AgendaAula>(age);
            genericRepositorio.UnitOfWork.CommitTransaction();

            return Json(true);
        }

        // data POST service
        [HttpPost]
        public JsonResult UpdateEvent(string ID, string NewEventStart, string NewEventEnd)
        {
            AgendaAula age = genericRepositorio.EncontrarPorID<AgendaAula>(ID);

            //var a = Convert.ToDateTime(NewEventEnd);
            //var b = Convert.ToDateTime(NewEventStart);
            //var c = Convert.ToDateTime(NewEventEnd).Subtract(Convert.ToDateTime(NewEventStart));
            //var min = (Convert.ToInt16(c.Hours) * 60) + Convert.ToInt16(c.Minutes);
            age.Hora = Convert.ToDateTime(NewEventStart);
            genericRepositorio.UnitOfWork.BeginTransaction();

            genericRepositorio.Atualizar<AgendaAula>(age);
            genericRepositorio.UnitOfWork.CommitTransaction();

            return Json(true);
        }

        // data POST service
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostNovoEvent(string Title, string NewEventDate, string NewEventTime, string NewEventDuration, string NewEventLocal)
        {
            try
            {
                //Agenda agenda = new Agenda();
                ////UsuarioServico oserv = new UsuarioServico();

                //agenda.Contato = db.Contato.FirstOrDefault();
                //agenda.IdContato = agenda.Contato.IdContato;

                //agenda.Compromisso = Title;
                //int id = Convert.ToInt16(Session["usuarioId"]);
                //agenda.Digitador = db.Usuario.Where(m => m.IdUsuario == id).FirstOrDefault();
                //agenda.Atendido = 0;
                ////agenda.Hora = agenda.Hora.AddMinutes(Convert.ToDouble(minutes));
                //agenda.Hora = Convert.ToDateTime(NewEventTime);
                //agenda.Data = Convert.ToDateTime(NewEventDate);
                //agenda.IdAgendaPara = id;
                //agenda.AgendaPara = db.Usuario.Where(m => m.IdUsuario == id).FirstOrDefault();
                //agenda.IdUsuario = Convert.ToInt16(Session["usuarioId"]);
                //agenda.Duracao = Convert.ToInt16(NewEventDuration);
                //agenda.Local = NewEventLocal;

                //if (ModelState.IsValid)
                //{
                //    db.Agenda.Add(agenda);
                //    db.SaveChanges();
                //}

                return Json("True");
            }
            catch (Exception)
            {

                return Json("false");
            }
        }

    }
}
