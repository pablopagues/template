﻿using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories;
using Sistema.Helpers.DataTable;
using Sistema.Controllers;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Template.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class PapeisController : FeatureController
    {
        private IPapelRepositorio papelRepositorio;
        private IGenericRepository genericRepositorio;

        private DataTable<Papel> dataTable = new DataTable<Papel>();

        public PapeisController(IPapelRepositorio papel_Repositorio,
                               IGenericRepository generic_Repositorio)
        {
            this.papelRepositorio = papel_Repositorio;
            this.genericRepositorio = generic_Repositorio;

            dataTable.addColumn(x => x.id)
                     .addColumn(x => x.nome);
                     //.addColumn(x => x.usuarios);
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<Papel>().AsQueryable(), dataTablePostData);
        }

        //
        // GET: /Papeis/
        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /Papeis/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                return View(genericRepositorio.EncontrarPor<Papel>(p=>p.usuarios).Where(p=>p.id==id).FirstOrDefault());
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Papeis/Create
        public ActionResult Create()
        {
            try
            {
                PapelViewModelMapper mapper = new PapelViewModelMapper(genericRepositorio);
                return View(mapper.exportaViewModel(new Papel()));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Papeis/Create
        [HttpPost]
        public ActionResult Create(PapelViewModel viewmodel, int[] usuariosIds)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    PapelViewModelMapper mapper = new PapelViewModelMapper(genericRepositorio);
                    Papel papel = mapper.importaViewModel(viewmodel);

                    papel.usuarios = genericRepositorio.EncontrarPor<Usuario>(x => usuariosIds.Contains(x.id)).ToList();
                    genericRepositorio.Adicionar<Papel>(papel);

                    genericRepositorio.UnitOfWork.CommitTransaction();
                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(viewmodel);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Papeis/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                PapelViewModelMapper mapper = new PapelViewModelMapper(genericRepositorio);
                return View(mapper.exportaViewModel(genericRepositorio.EncontrarPor<Papel>(p => p.usuarios).Where(p => p.id == id).FirstOrDefault()));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Papeis/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PapelViewModel viewmodel, int[] usuariosIds)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    PapelViewModelMapper mapper = new PapelViewModelMapper(genericRepositorio);
                    Papel papel = mapper.importaViewModel(viewmodel);

                    genericRepositorio.UnitOfWork.BeginTransaction();
                    if (usuariosIds != null)
                    {
                        //recupera os papeis do usuario, apaga eles, adiciona os novos
                        //tenho que fazer isto pq o usuario que vem da view vem com lista de papeis "desconectada"
                        Papel reg1 = genericRepositorio.EncontrarPor<Papel>(p => p.usuarios).Where(p => p.id == papel.id).FirstOrDefault();
                        papel.usuarios = reg1.usuarios;
                        var usuariosnovos = genericRepositorio.EncontrarPor<Usuario>(x => usuariosIds.Contains(x.id)).ToList();
                        papel.usuarios.Clear();
                        foreach (var reg in usuariosnovos)
                        {
                            papel.usuarios.Add(reg);
                        }

                        genericRepositorio.Atualizar<Papel>(papel);
                    }
                    else
                        genericRepositorio.Atualizar<Papel>(papel);

                    genericRepositorio.UnitOfWork.CommitTransaction();
                }
                

                informaSucesso("Registro salvo com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Papeis/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                return View(genericRepositorio.EncontrarPor<Papel>(p => p.usuarios).Where(p => p.id == id).FirstOrDefault());
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Papeis/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<Papel>(genericRepositorio.EncontrarPorID<Papel>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();

                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
