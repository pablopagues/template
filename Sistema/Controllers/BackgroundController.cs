﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    public class BackgroundController : Controller
    {

        public const string IMAGES_DIR = "~/Content/Images/Backgrounds/";

        //
        // GET: /Backgound/

        public FileResult Index()
        {
            string filename = String.Empty;
            bool fileExists = false;

            Random random = new Random();
            int randomNumber;

            while (!fileExists)
            {
                randomNumber = random.Next(0, 10);
                filename = IMAGES_DIR + "background-0" + randomNumber + ".jpg";

                if (System.IO.File.Exists(Server.MapPath(filename)))
                {
                    fileExists = true;
                }
            }

            return File(Server.MapPath(filename), "image/jpeg");
        }

    }
}
