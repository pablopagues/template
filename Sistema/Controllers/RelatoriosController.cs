﻿using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Service.Controllers.Results;
using Sistema.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using Sistema.Helpers.Attributes;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class RelatoriosController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        //Parametros do Relatorio
        //private string path;
        private string deviceInfo;
        private string mimeType;
        private string encoding;
        private string fileNameExtension;
        private string[] streams;
        private Warning[] warnings;
        private byte[] renderedBytes;

        public RelatoriosController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            this.deviceInfo =
                @"<DeviceInfo>
                    <OutputFormat>Excel</OutputFormat>
                    <MarginTop>0.5in</MarginTop>
                    <MarginBottom>0.5in</MarginBottom>
                    <MarginLeft>0.5in</MarginLeft>
                    <MarginRight>0.2in</MarginRight>
                </DeviceInfo>";
        }

        //
        // GET: /Relatorios/

        public ActionResult LiquidacaoProfessor()
        {
            ViewBag.Professores = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult LiquidacaoProfessor(FormCollection form)
        {
            int id = Convert.ToInt16(form["idProfessor"]);
            DateTime dtinicio = Convert.ToDateTime(form["DataInicio"]);
            DateTime dtfim = Convert.ToDateTime(form["DataFim"]);

            if (ModelState.IsValid)
            {

                var lst = genericRepositorio.Tudo<LiquidacaoProfessores>()
                                            .Where(p => p.professor_id == id)
                                            .Where(p => p.datamovimento >= dtinicio)
                                            .Where(p => p.datamovimento <= dtfim)
                                            .Select(p => new
                                            {
                                                p.professor_id,
                                                nome = p.nome,
                                                qtdalunos = p.qtdalunos,
                                                vlhora = p.vlhora,
                                                datamovimento = p.datamovimento,
                                                TimeDiff = p.TimeDiff,
                                                horasgrupo = p.horasgrupo,
                                                grupo = p.grupo,
                                                valorpagar = p.valorpagar
                                            }).OrderBy(p=>p.datamovimento).ToList();

                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Reports/FaturamentoProfessor.rdlc");

                // Gerando Excel
                ReportDataSource reportDataSource = new ReportDataSource("LiquidacaoProfessor", lst);
                localReport.DataSources.Add(reportDataSource);
                renderedBytes = localReport.Render("Excel", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

                string NomeArquivo = "Relatorio.";

                // Enviando para o usuário
                return File(renderedBytes, mimeType, NomeArquivo + fileNameExtension);
            }
            else
            {
                return View();
            }
        }


        /// <summary>
        /// MOVIMENTAÇÃO POR ALUNO
        /// </summary>
        /// <returns></returns>
        public ActionResult MovimentacaoAluno()
        {
            ViewBag.Alunos = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).OrderBy(p=>p.Text).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult MovimentacaoAluno(FormCollection form)
        {
            int id = Convert.ToInt16(form["idAluno"]);
            DateTime dtinicio = Convert.ToDateTime(form["DataInicio"]);
            DateTime dtfim = Convert.ToDateTime(form["DataFim"]);

            if (ModelState.IsValid)
            {

                var lst = genericRepositorio.Tudo<MovimentoAula>(p=>p.professor)
                                            .Where(p => p.aluno.id == id)
                                            .Where(p => p.DataMovimento >= dtinicio)
                                            .Where(p => p.DataMovimento <= dtfim)
                                            .Select(p => new
                                            { 
                                                Aluno = p.aluno.nome,
                                                Professor = p.professor.Nome,
                                                DescricaoMateria = p.Descricao,
                                                HoraInicio = p.HoraInicio.ToShortTimeString(),
                                                HoraFim = p.HoraFinal.ToShortTimeString(),
                                                Minutos = (p.HoraFinal - p.HoraInicio),
                                                DataMovimento = p.DataMovimento
                                            }).ToList();

                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Reports/MovimentoAluno.rdlc");

                // Gerando Excel
                ReportDataSource reportDataSource = new ReportDataSource("Movimentacao", lst);
                localReport.DataSources.Add(reportDataSource);
                renderedBytes = localReport.Render("Excel", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

                string NomeArquivo = "Relatorio.";

                // Enviando para o usuário
                return File(renderedBytes, mimeType, NomeArquivo + fileNameExtension);
            }
            else
            {
                return View();
            }
        }


        /// <summary>
        /// MOVIMENTAÇÃO POR PROFESSOR
        /// </summary>
        /// <returns></returns>
        public ActionResult MovimentacaoProfessor()
        {
            ViewBag.Professores = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).OrderBy(p => p.Text).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult MovimentacaoProfessor(FormCollection form)
        {
            int id = Convert.ToInt16(form["idProfessor"]);
            DateTime dtinicio = Convert.ToDateTime(form["DataInicio"]);
            DateTime dtfim = Convert.ToDateTime(form["DataFim"]);

            if (ModelState.IsValid)
            {

                var lst = genericRepositorio.Tudo<MovimentoAula>()
                                            .Where(p => p.professor.id == id)
                                            .Where(p => p.DataMovimento >= dtinicio)
                                            .Where(p => p.DataMovimento <= dtfim)
                                            .Select(p => new
                                            {
                                                Aluno = p.aluno.nome,
                                                Professor = p.professor.Nome,
                                                DescricaoMateria = p.Descricao,
                                                HoraInicio = p.HoraInicio.ToShortTimeString(),
                                                HoraFim = p.HoraFinal.ToShortTimeString(),
                                                Minutos = (p.HoraFinal - p.HoraInicio),
                                                DataMovimento = p.DataMovimento,
                                                Grupo = p.Grupo.ToString(),
                                                IdMovimento = p.id.ToString()
                                            }).ToList();
                
                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Reports/MovimentoProfessor.rdlc");

                // Gerando Excel
                ReportDataSource reportDataSource = new ReportDataSource("Movimentacao", lst);
                localReport.DataSources.Add(reportDataSource);
                renderedBytes = localReport.Render("Excel", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

                string NomeArquivo = "Relatorio.";

                // Enviando para o usuário
                return File(renderedBytes, mimeType, NomeArquivo + fileNameExtension);
            }
            else
            {
                return View();
            }
        }
    }
}
