﻿using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories;
using Newtonsoft.Json;
using Sistema.Helpers;
using Sistema.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using Infrastructure.Domain;
using Infrastructure.Domain.Base.GenericRepository;
using WebMatrix.WebData;
using Microsoft.Web.WebPages.OAuth;
using Infrastructure.Service.Security;

namespace Sistema.Controllers
{
    public class ContaController : FeatureController
    {
        private IUsuarioRepositorio usuarioRepositorio;
        private IGenericRepository genericRepositorio;

        public ContaController(IUsuarioRepositorio usuario_repositorio, IGenericRepository generic_repositorio)
        {
            this.usuarioRepositorio = usuario_repositorio;
            this.genericRepositorio = generic_repositorio;
        }

        //
        // GET: /Conta/Login
        public ActionResult Login(string ReturnUrl)
        {
            ViewData.Clear(); // Não renderiza a mesma mensagem exibida duas vezes
            return View(new LoginViewModel() { returnUrl = ReturnUrl });
        }

        //
        // GET: /Conta/Trocar
        public ActionResult Trocar()
        {
            //remover cookie
            HttpCookie cookie;

            cookie = Request.Cookies["sistema-login"];
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookie);

            cookie = Request.Cookies["sistema-nome"];
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookie);

            cookie = Request.Cookies["sistema-email"];
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookie);

            return RedirectToAction("Login");
        }

        //
        // GET: /Conta/Login
        [HttpPost]
        public ActionResult Login(string ReturnUrl, LoginViewModel item)
        {
            Usuario usuario;

            try
            {
                //Valida dados de login e senha 
                if (ModelState.IsValid) //  && WebSecurity.Login(item.login, item.senha, persistCookie: true)
                {
                    string username = item.login.ToLower();
                    string password = item.senha;

                    if (usuarioRepositorio.validateUser(username, password))
                    {

                        //criação do cookie
                        HttpCookie cookie;

                        var perfil = genericRepositorio.EncontrarPor<Usuario>(p=>p.professor).Where(p => p.login == username).FirstOrDefault();

                        cookie = new HttpCookie("sistema-login");
                        cookie.Value = item.login;
                        cookie.Expires = DateTime.Now.AddMonths(1);
                        Response.SetCookie(cookie);

                        cookie = new HttpCookie("sistema-nome");
                        cookie.Value = perfil.professor.Nome;
                        cookie.Expires = DateTime.Now.AddMonths(1);
                        Response.SetCookie(cookie);

                        cookie = new HttpCookie("sistema-email");
                        cookie.Value = perfil.professor.email;
                        cookie.Expires = DateTime.Now.AddMonths(1);
                        Response.SetCookie(cookie);

                        // preenchemos a sessão com os dados para ser renderizados no menu
                        usuario = genericRepositorio.FindOne<Usuario>(x => x.login == item.login);
                        Session.Add("usuario", usuario);
                        Session.Add("usuarioId", usuario.id);

                        // Grant cookie and redirect (to admin home if not otherwise specified)
                        FormsAuthentication.SetAuthCookie(item.login, false);

                        genericRepositorio.UnitOfWork.BeginTransaction();
                        usuario.ultimoLogin = DateTime.Now;
                        usuario.estaOnline = true;
                        genericRepositorio.Atualizar<Usuario>(usuario);
                        genericRepositorio.UnitOfWork.CommitTransaction();

                        SetupFormsAuthTicket(usuario, true);

                        //se foi autorizado mais a senha é padrão
                        //redirect mudança de senha
                        if (password == "123456")
                            return RedirectToAction("Manage");

                        // E redirecionamos para o local desejado
                        return Redirect(String.IsNullOrWhiteSpace(ReturnUrl) ? Url.Action("Index", "Home") : ReturnUrl);
                    }
                    else
                    {
                        informaErro("Senha incorreta");
                    }
                }
                else
                {
                    informaErro("Digite os campos obrigatórios e tente novamente.");
                }

                return View(item);
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return View(item);
            }
        }

        //
        // GET: /Conta/Logoff
        public ActionResult Logoff()
        {
            if (Session["usuario"] != null)
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                Usuario usuario = Session["usuario"] as Usuario;
                usuario.estaOnline = false;
                genericRepositorio.Atualizar<Usuario>(usuario);
                genericRepositorio.UnitOfWork.CommitTransaction();
            }

            FormsAuthentication.SignOut();
            Session.RemoveAll();

            informaAviso("Sua sessão foi encerrada!");

            return RedirectToAction("Login");
        }

        private bool SetupFormsAuthTicket(Usuario user, bool persistanceFlag)
        {
            var userData = user.id.ToString();
            var authTicket = new FormsAuthenticationTicket(1, //version
                                                        user.login, // login
                                                        DateTime.Now,             //creation
                                                        DateTime.Now.AddMinutes(30), //Expiration
                                                        persistanceFlag, //Persistent
                                                        userData);

            var encTicket = FormsAuthentication.Encrypt(authTicket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
            return true;
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Sua senha foi alterada com sucesso."
                : message == ManageMessageId.SetPasswordSuccess ? "Sua senha foi definida."
                : message == ManageMessageId.RemoveLoginSuccess ? "Login externo foi removido."
                : "";

            Usuario usr = (Usuario)Session["usuario"];

            ViewBag.Nome = usr.professor.Nome;

            //ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            //bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            //ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            //if (hasLocalAccount)
            //{
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    Usuario usuario = genericRepositorio.EncontrarPor<Usuario>().Where(p => p.login == User.Identity.Name).FirstOrDefault();
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    usuario.Senha = Criptography.HashCode(model.NewPassword);  //usuarioRepositorio.CriptografiaMD5(model.NewPassword);
                    genericRepositorio.Atualizar<Usuario>(usuario);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    //WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);

                    //bool changePasswordSucceeded;
                    //try
                    //{
                    //    changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    //}
                    //catch (Exception)
                    //{
                    //    changePasswordSucceeded = false;
                    //}

                    //if (changePasswordSucceeded)
                    //{
                        return RedirectToAction("Index", "Home", new { Message = ManageMessageId.ChangePasswordSuccess });
                    //}
                    //else
                    //{
                    //    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    //}
                }
            //}
            //else
            //{
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                //ModelState state = ModelState["OldPassword"];
                //if (state != null)
                //{
                //    state.Errors.Clear();
                //}

                //if (ModelState.IsValid)
                //{
                //    try
                //    {
                        //atualizo senha no banco

                //    }
                //    catch (Exception e)
                //    {
                //        ModelState.AddModelError("", e);
                //    }
                //}
            //}

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
