﻿using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Repositories;
using Infrastructure.Service.Security;
using Infrastructure.Service.Tools.Drawing;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class UsuariosController : FeatureController
    {
        private IPapelRepositorio papelRepositorio;
        private IGenericRepository genericRepositorio;
        public const string IMAGES_DIR = "~/Content/Images/Usuarios/";

        private DataTable<Usuario> dataTable = new DataTable<Usuario>();
        
        public UsuariosController(IGenericRepository generic_Repositorio,
            IPapelRepositorio papel_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;
            this.papelRepositorio = papel_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.login, "Login")
                .addColumn(x => x.ultimoLogin).setCssClass("align-center");
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<Usuario>().AsQueryable(), dataTablePostData);
        }

        //
        // GET: /Usuarios/
        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /Usuarios/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<Usuario>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            UsuariosViewModelMapper mapper = new UsuariosViewModelMapper(genericRepositorio);
            return View(mapper.exportaViewModel(new Usuario(genericRepositorio)));
        }

        //
        // POST: /Usuarios/Create
        [HttpPost]
        public ActionResult Create(UsuarioViewModel item, int[] ids)
        {
            try
            {
                if (ids == null)
                {
                    string erro = "Por favor, informe pelo menos um nível de acesso.";
                    informaErro(erro);
                    ModelState.AddModelError("", erro);
                }

                var professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.IdProfessor).FirstOrDefault();
                item.login = professor.Login;
                item.Nome = professor.Nome;
                item.email = professor.email;

                var existelogin = genericRepositorio.EncontrarPor<Usuario>(x => x.login == item.login).FirstOrDefault();

                if (item.Senha == item.ConfirmaSenha && existelogin == null) //ModelState.IsValid && 
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    UsuariosViewModelMapper mapper = new UsuariosViewModelMapper(genericRepositorio);
                    Usuario usuario = mapper.importaViewModel(item);

                    usuario.Senha = Criptography.HashCode(item.Senha);

                    usuario.papeis = genericRepositorio.EncontrarPor<Papel>(x => ids.Contains(x.id)).ToList();
                    usuario.dataCriacao = DateTime.Now;
                    usuario.ultimoLogin = new DateTime(1900, 1, 1);

                    usuario.professor = professor;

                    genericRepositorio.Adicionar<Usuario>(usuario);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    item.papeisList = genericRepositorio.Tudo<Papel>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    item.professorList = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                    informaErro("Faltam informações ou o professor já é usuário do sistema.");
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Usuarios/Edit/5
        public ActionResult Edit(int id, int[] ids)
        {
            try
            {
                UsuariosViewModelMapper mapper = new UsuariosViewModelMapper(genericRepositorio);

                Usuario u = genericRepositorio.EncontrarPor<Usuario>(p=>p.papeis).Where(p=>p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Usuarios/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, UsuarioViewModel item, int[] ids)
        {
            try
            {
                if (ModelState.IsValid && item.Senha == item.ConfirmaSenha)
                {
                    UsuariosViewModelMapper mapper = new UsuariosViewModelMapper(genericRepositorio);
                    Usuario usuario = mapper.importaViewModel(item);

                    if (ids != null)
                    {

                        genericRepositorio.UnitOfWork.BeginTransaction();

                        //recupera os papeis do usuario, apaga eles, adiciona os novos
                        //tenho que fazer isto pq o usuario que vem da view vem com lista de papeis "desconectada"
                        Usuario usr = genericRepositorio.EncontrarPor<Usuario>(p => p.papeis).Where(p => p.id == usuario.id).FirstOrDefault();
                        usuario.papeis = usr.papeis;
                        var papeisnovos = genericRepositorio.EncontrarPor<Papel>(x => ids.Contains(x.id)).ToList();
                        usuario.papeis.Clear();
                        foreach (var reg in papeisnovos)
                        {
                            usuario.papeis.Add(reg);
                        }

                        usuario.Senha = Criptography.HashCode(item.Senha);

                        var professor = genericRepositorio.EncontrarPor<Professor>(x => x.id == item.IdProfessor).FirstOrDefault();
                        usuario.professor = professor;

                        genericRepositorio.Atualizar<Usuario>(usuario);
                        genericRepositorio.UnitOfWork.CommitTransaction();

                    }
                    else
                        genericRepositorio.Atualizar<Usuario>(usuario);

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    item.papeisList = genericRepositorio.Tudo<Papel>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
                    item.professorList = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Usuarios/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPor<Usuario>(p => p.professor, p=>p.papeis).Where(p => p.id == id).FirstOrDefault());
            
        }

        //
        // POST: /Usuarios/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Usuario u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<Usuario>(genericRepositorio.EncontrarPorID<Usuario>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        [AllowAnonymous]
        public FileResult Foto(string login)
        {
            string filename = IMAGES_DIR + "000000.jpg";

            if (login != null)
            {
                //Usuario usuario = genericRepositorio.EncontrarPor<Usuario>(x => x.login.ToLower() == login.ToLower()).IncludeMultiple(p=>p.professor).FirstOrDefault();
                Usuario usuario = genericRepositorio.Tudo<Usuario>(p => p.professor).Where(x => x.login.ToLower() == login.ToLower()).FirstOrDefault();

                if (usuario != null)
                {
                    if (System.IO.File.Exists(Server.MapPath(IMAGES_DIR + usuario.professor.Nome + ".jpg")))
                    {
                        filename = IMAGES_DIR + usuario.professor.Nome + ".jpg";
                    }
                    else if (System.IO.File.Exists(Server.MapPath(IMAGES_DIR + usuario.login + ".jpg")))
                    {
                        filename = IMAGES_DIR + usuario.login + ".jpg";
                    }
                    else
                    {
                        string nome = usuario.professor.Nome;
                        string sigla = nome.ElementAt(0).ToString();

                        int pos = nome.LastIndexOf(' ');
                        if (pos > 0)
                        {
                            sigla += nome.ElementAt(pos + 1).ToString();
                        }
                        else
                        {
                            sigla += nome.ElementAt(1).ToString();
                        }
                        sigla = sigla.ToUpper();

                        //gerar imagem de acordo com as iniciais do nome
                        using (Bitmap image = new Bitmap(256, 256))
                        {
                            using (Graphics g = Graphics.FromImage(image))
                            {
                                Font drawFont = new Font("Verdana", 90);
                                SolidBrush drawBrush = new SolidBrush(Color.White);
                                PointF stringPoint = new PointF(18, 50);

                                g.Clear(ColorProvider.GetColor(nome));
                                g.SmoothingMode = SmoothingMode.AntiAlias;
                                g.TextRenderingHint = TextRenderingHint.AntiAlias;
                                g.DrawString(sigla, drawFont, drawBrush, stringPoint);
                            }

                            MemoryStream ms = new MemoryStream();
                            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            return File(ms.ToArray(), "image/jpeg");
                        }
                    }
                }
            }

            return File(Server.MapPath(filename), "image/jpeg");
        }

        //public JsonResult todos()
        //{
        //    var lst = genericRepositorio.Tudo<Professor>().Select(p => new SelectListItem() { Value = p.id.ToString(), Text = p.Nome }).ToList();
        //    return Json(lst, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult Buscar(int pIdProfessor)
        {
            var professor = genericRepositorio.Tudo<Professor>().Where(x=>x.id == pIdProfessor).FirstOrDefault();
            string markup = "<div class=\"row\" style=\"margin-top:20px\"><div class=\"col-md-8\">";
            markup += "<h5 class=\"media-heading\">" + professor.Nome + "</h5></div></div>";
            markup += "<div class=\"row\"><div class=\"col-md-8\"><p class=\"text-muted\">" + (professor.email != null ? professor.email : "...") + "<br>";
            markup += "</p>";
            markup += "</div></div>";
            return Json(markup, JsonRequestBehavior.AllowGet);
        }

    }
}
