using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;
using Sistema.Helpers.DataTable;
using Sistema.Helpers.Attributes;
using Sistema.Helpers.DataTable;
using Sistema.ViewModels;
using Sistema.ViewModels.Mappers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema.Controllers
{
    [AuthorizeRoles("Administrador")]
    public class AlunoController : FeatureController
    {

        private IGenericRepository genericRepositorio;

        private DataTable<Aluno> dataTable = new DataTable<Aluno>();

        public AlunoController(IGenericRepository generic_Repositorio)
        {
            this.genericRepositorio = generic_Repositorio;

            dataTable
                .addColumn(x => x.id)
                .addColumn(x => x.nome)
                .addColumn(x => x.nomePai)
                .addColumn(x => x.nomeMae)
                .addColumn(x => x.endereco)
                .addColumn(x => x.telefone)
                .addColumn(x => x.celular);
        }

        [HttpPost]
        public DataTableResult DataTableIndex(DataTablePostData dataTablePostData)
        {
            return dataTable.getDataTableResultEF(ControllerContext, genericRepositorio.Tudo<Aluno>().AsQueryable(), dataTablePostData);
        }
        
        //
        // GET: /Aluno/

        public ActionResult Index()
        {
            return View(dataTable.renderDataTable());
        }

        //
        // GET: /Aluno/Details/5
        public ActionResult Details(int id)
        {
            return View(genericRepositorio.EncontrarPorID<Aluno>(id));
        }

        //
        // GET: /Usuarios/Create
        public ActionResult Create()
        {
            AlunoViewModelMapper mapper = new AlunoViewModelMapper();
            AlunoViewModel model = mapper.exportaViewModel(new Aluno());
            model.dtInicioAulas = DateTime.Now;
            model.dtNascimento = DateTime.Now;
            model.dtVencAulas = DateTime.Now;

            return View(model);
        }

        //
        // POST: /Aluno/Create
        [HttpPost]
        public ActionResult Create(AlunoViewModel item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    genericRepositorio.UnitOfWork.BeginTransaction();
                    AlunoViewModelMapper mapper = new AlunoViewModelMapper();
                    Aluno Aluno = mapper.importaViewModel(item);

                    genericRepositorio.Adicionar<Aluno>(Aluno);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Aluno/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                AlunoViewModelMapper mapper = new AlunoViewModelMapper();

                Aluno u = genericRepositorio.EncontrarPor<Aluno>().Where(p => p.id == id).FirstOrDefault();
                return View(mapper.exportaViewModel(u));
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AlunoViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AlunoViewModelMapper mapper = new AlunoViewModelMapper();
                    Aluno Aluno = mapper.importaViewModel(item);

                    genericRepositorio.UnitOfWork.BeginTransaction();

                    genericRepositorio.Atualizar<Aluno>(Aluno);
                    genericRepositorio.UnitOfWork.CommitTransaction();

                    informaSucesso("Registro salvo com sucesso!");
                    return RedirectToAction("Index");
                }
                else
                {

                    return View(item);
                }
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Aluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View(genericRepositorio.EncontrarPorID<Aluno>(id));
        }

        //
        // POST: /Aluno/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Aluno u)
        {
            try
            {
                genericRepositorio.UnitOfWork.BeginTransaction();
                genericRepositorio.Deletar<Aluno>(genericRepositorio.EncontrarPorID<Aluno>(id));
                genericRepositorio.UnitOfWork.CommitTransaction();
                informaSucesso("Registro apagado com sucesso!");
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                informaErro(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
