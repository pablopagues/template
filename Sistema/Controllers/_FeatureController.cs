﻿using System;
using System.Web.Mvc;
using Sistema.Helpers;
using Infrastructure.Service.Controllers.Results;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Infrastructure.Service.Controllers.Providers;

namespace Sistema.Controllers
{
    /// <summary>
    /// Controlador que implementa rotinas auxiliares para prover recursos
    /// comumente usados por outros controladores.
    /// </summary>
    public abstract class FeatureController : Controller
    {
        public enum MessageType
        {
            Error,
            Warning,
            Information,
            Success
        }

        [NonAction]
        private static string getTypeString(MessageType type)
        {
            switch (type)
            {
                case MessageType.Error:
                    return "error";

                case MessageType.Information:
                    return "info";

                case MessageType.Success:
                    return "success";

                case MessageType.Warning:
                    return "warning";
            }

            throw new Exception("Message type not found.");
        }

        [NonAction]
        private void informaMensagem(MessageType type, string mensagem)
        {
            string typeString = getTypeString(type);

            ViewData[typeString] = TempData[typeString] = mensagem;
        }

        /// <summary>
        /// Retorna a mensagem de erro via ajax
        /// </summary>
        /// <param name="mensagem">Objeto Json contendo mensagem de erro e o tipo</param>
        [NonAction]
        protected JsonResult informaMensagemAjax(MessageType type, string mensagem)
        {
            string typeString = getTypeString(type);

            return Json(new { type = typeString, message = mensagem }, JsonRequestBehavior.AllowGet);
        }


        /****** COMMON MESSAGES ******/

        /// <summary>
        /// Exibe na página a mensagem de erro.
        /// </summary>
        /// <param name="mensagem">Mensagem de erro</param>
        [NonAction]
        public void informaErro(string mensagem)
        {
            informaMensagem(MessageType.Error, mensagem);
        }

        /// <summary>
        /// Exibe na página a mensagem de aviso.
        /// </summary>
        /// <param name="mensagem">Mensagem de aviso</param>
        [NonAction]
        public void informaAviso(string mensagem)
        {
            informaMensagem(MessageType.Warning, mensagem);
        }

        /// <summary>
        /// Exibe na página a mensagem de informação.
        /// </summary>
        /// <param name="mensagem">Mensagem de informação</param>
        [NonAction]
        public void informaMensagem(string mensagem)
        {
            informaMensagem(MessageType.Information, mensagem);
        }

        /// <summary>
        /// Exibe na página a mensagem de confirmação.
        /// </summary>
        /// <param name="mensagem">Mensagem de confirmaçaõ</param>
        [NonAction]
        protected void informaSucesso(string mensagem)
        {
            informaMensagem(MessageType.Success, mensagem);
        }


        /****** AJAX MESSAGES ******/

        [NonAction]
        protected JsonResult informaErroAjax(string mensagem)
        {
            return informaMensagemAjax(MessageType.Error, mensagem);
        }

        [NonAction]
        protected JsonResult informaAvisoAjax(string mensagem)
        {
            return informaMensagemAjax(MessageType.Warning, mensagem);
        }

        [NonAction]
        protected JsonResult informaMensagemAjax(string mensagem)
        {
            return informaMensagemAjax(MessageType.Information, mensagem);
        }

        [NonAction]
        protected JsonResult informaSucessoAjax(string mensagem)
        {
            return informaMensagemAjax(MessageType.Success, mensagem);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string error = getTypeString(MessageType.Error);
            string information = getTypeString(MessageType.Information);
            string success = getTypeString(MessageType.Success);
            string warning = getTypeString(MessageType.Warning);

            // Exibe depois do redirect as mensagens de erro.
            if (TempData[error] != null)
                ViewData[error] = TempData[error];

            if (TempData[warning] != null)
                ViewData[warning] = TempData[warning];

            if (TempData[information] != null)
                ViewData[information] = TempData[information];

            if (TempData[success] != null)
                ViewData[success] = TempData[success];

            base.OnActionExecuted(filterContext);
        }

        /// <summary>
        /// Retorna um objeto Json para o requisitante utilizando a biblioteca Json.NET (extremamente mais rápida que a versão nativa do .NET).
        /// </summary>
        /// <param name="data">Dados a serem serializados</param>
        /// <param name="contentType">ContentType do retorno</param>
        /// <param name="contentEncoding">Codificação do retorno</param>
        /// <param name="behavior">Comportamento da requisição JSON</param>
        /// <returns>Dados serializados em JSON</returns>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new NewtonsoftJsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        /// <summary>
        /// Resultado retorna um relatório Crystal Report em formato PDF para o usuário
        /// </summary>
        /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
        /// <param name="reportDataSet">Dataset usado pelo relatório</param>
        protected PdfReportResult PdfReport(string reportFileName, DataSet reportDataSet, string returnFilename = null)
        {
            ServerPathMapper pathMapper = new ServerPathMapper();

            return new PdfReportResult(pathMapper, reportFileName, reportDataSet)
            {
                ViewData = this.ViewData,
                TempData = this.TempData,
                returnFilename = returnFilename
            };
        }

        /// <summary>
        /// Resultado retorna um relatório Crystal Report em formato PDF para o usuário
        /// </summary>
        /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
        /// <param name="reportEnumerable">IEnumerable usado pelo relatório</param>
        protected PdfReportResult PdfReport(string reportFileName, IEnumerable reportEnumerable, string returnFilename = null)
        {
            ServerPathMapper pathMapper = new ServerPathMapper();

            return new PdfReportResult(pathMapper, reportFileName, reportEnumerable)
            {
                ViewData = this.ViewData,
                TempData = this.TempData,
                returnFilename = returnFilename
            };
        }

        /// <summary>
        /// Resultado retorna um relatório Crystal Report em formato PDF para o usuário
        /// </summary>
        /// <param name="reportFileName">Nome do arquivo Crystal Report</param>
        /// <param name="reportDataSet">Dataset usado pelo relatório</param>
        /// <param name="parameters">Parametros usado pelo relatório</param>
        protected PdfReportResult PdfReport(string reportFileName, DataSet reportDataSet, IDictionary<string, object> parameters, string returnFilename = null)
        {
            ServerPathMapper pathMapper = new ServerPathMapper();

            return new PdfReportResult(pathMapper, reportFileName, reportDataSet, parameters)
            {
                ViewData = this.ViewData,
                TempData = this.TempData,
                returnFilename = returnFilename
            };
        }

        ///// <summary>
        ///// Resultado retorna um jsonp com a função de callback
        ///// </summary>
        ///// <param name="data"></param>
        ///// <returns></returns>
        //protected JsonpResult Jsonp(object data)
        //{
        //    JsonpResult result = new JsonpResult();
        //    result.Data = data;
        //    return result;
        //}
    }
}
