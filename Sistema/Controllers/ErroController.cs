﻿using System.Web.Mvc;

namespace Sistema.Controllers
{
    public class ErroController : Controller
    {
        public ActionResult NavegadorIncompativel() { return View("002"); }

        public ActionResult RequisicaoErrada() { return View("400"); }

        public ActionResult NaoAutorizado() { return View("401"); }

        public ActionResult AcessoNegado() { return View("403"); }

        public ActionResult NaoEncontrado() { return View("404"); }

        public ActionResult TempoEsgotado() { return View("408"); }

        public ActionResult ErroInterno() { return View("500"); }

        public ActionResult ServidorIndisponivel() { return View("503"); }
    }
}
