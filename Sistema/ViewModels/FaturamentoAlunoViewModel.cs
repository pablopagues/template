using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Sistema.ViewModels
{
    public class FaturamentoAlunoViewModel
    {
        public int id { get; set; }

        public int idAluno { get; set; }
        [DisplayName("Aluno")]
        public IList<SelectListItem> Aluno { get; set; }
        [DisplayName("Descri��o")]
        public String Descricao { get; set; }
        [DisplayName("Qtd Horas")]
        public decimal QtdHoras { get; set; }
        [DisplayName("Valor Hora")]
        public decimal ValorHora { get; set; }
        [DisplayName("Data")]
        public DateTime Data { get; set; }

    }
}

