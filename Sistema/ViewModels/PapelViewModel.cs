﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Sistema.ViewModels
{
    public class PapelViewModel
    {
        [HiddenInput]
        public virtual int id { get; set; }

        [Required]
        [DisplayName("Papel")]
        public virtual string nome { get; set; }

        [DisplayName("Usuários")]
        public virtual IList<SelectListItem> usuariosList { get; set; }
    }
}
