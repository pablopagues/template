using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sistema.ViewModels
{
    public class AlunoViewModel
    {
        //public Aluno aluno { get; set; }
        public int id { get; set; }

        [DisplayName("Nome Aluno")]
        public string nome { get; set; }
        [DisplayName("Pai")]
        public string nomePai { get; set; }
        [DisplayName("Mãe")]
        public string nomeMae { get; set; }
        [DisplayName("Endereço")]
        public string endereco { get; set; }
        [DisplayName("Telefone")]
        public string telefone { get; set; }
        [DisplayName("Celular")]
        public string celular { get; set; }
        [DisplayName("Dt. Inicio Aulas")]
        public DateTime dtInicioAulas { get; set; }
        [DisplayName("Dt. Venc. Aulas")]
        public DateTime dtVencAulas { get; set; }
        [DisplayName("Dt. Nascimento")]
        public DateTime dtNascimento { get; set; }
        [DisplayName("Disciplinas")]
        public string disciplinas { get; set; }
        [DisplayName("Serie")]
        public string serie { get; set; }
        [DisplayName("Email")]
        public string email { get; set; }
        [DisplayName("Celular Pai")]
        public string celularPai { get; set; }
        [DisplayName("Celular Mãe")]
        public string celularMae { get; set; }
        [DisplayName("Escola")]
        public string escola { get; set; }
        [DisplayName("Observação")]
        public string observacao { get; set; }

    }
}

