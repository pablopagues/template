﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sistema.ViewModels
{
    public class UsuarioViewModel
    {
        //public Usuario usuario { get; set; }
        public int id { get; set; }

        public string email { get; set; }

        [Required]
        [DisplayName("Nome de Login")]
        public string login { get; set; }

        [DisplayName("Nome")]
        public String Nome { get; set; }

        [DisplayName("Último Login")]
        public DateTime ultimoLogin { get; set; }

        [DisplayName("Data de Criação")]
        public DateTime dataCriacao { get; set; }

        [DisplayName("Está Online?")]
        public bool estaOnline { get; set; }

        [DisplayName("Está Bloqueado?")]
        public bool estaBloqueado { get; set; }

        public IList<System.Web.Mvc.SelectListItem> papeisList { get; set; }

        public int IdProfessor { get; set; }

        [DisplayName("Professor")]
        public IList<System.Web.Mvc.SelectListItem> professorList { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "A {0} deve ser de {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirma Senha")]
        [Compare("Senha", ErrorMessage = "As senhas digitadas não coincidem.")]
        public string ConfirmaSenha { get; set; }

    }
}
