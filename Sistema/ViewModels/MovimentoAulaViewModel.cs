using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Sistema.ViewModels
{
    public class MovimentoAulaViewModel
    {
        //public MovimentoAula movimentoaula { get; set; }
        public int id { get; set; }

        public int idAluno { get; set; }
        [DisplayName("Aluno")]
        public IList<SelectListItem> Aluno { get; set; }
        [DisplayName("Descri��o")]
        public String Descricao { get; set; }

        [DisplayName("Hora Inicio")]
        public String HoraInicio { get; set; }

        [DisplayName("Hora Final")]
        public String HoraFinal { get; set; }

        [DisplayName("Qtd Horas")]
        public decimal QtdHoras { get; set; }
        public int idProfessor { get; set; }
        [DisplayName("Professor")]
        public IList<SelectListItem> Professor { get; set; }
        [DisplayName("Grupo")]
        public int Grupo { get; set; }
        [DisplayName("Data Movimento")]
        public DateTime DataMovimento { get; set; }

    }
}

