using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Base.GenericRepository;

namespace Sistema.ViewModels.Mappers
{

    public class FaturamentoAlunoViewModelMapper : MapeadorViewModel<FaturamentoAlunoViewModel, FaturamentoAluno>
    {
        private IGenericRepository genericRepositorio;

        public FaturamentoAlunoViewModelMapper(IGenericRepository generic_Repository)
        {
            this.genericRepositorio = generic_Repository;
        }

        public override FaturamentoAlunoViewModel exportaViewModel(FaturamentoAluno model)
        {
            FaturamentoAlunoViewModel viewmodel = base.exportaViewModel(model);
            viewmodel.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
            if (model.aluno != null)
                viewmodel.idAluno = model.aluno.id;

            return viewmodel;
        }

        public override FaturamentoAluno importaViewModel(FaturamentoAlunoViewModel viewmodel)
        {
            FaturamentoAluno model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}
