using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers
{

    public class AlunoViewModelMapper : MapeadorViewModel<AlunoViewModel, Aluno>
    {

        public override AlunoViewModel exportaViewModel(Aluno model)
        {
            AlunoViewModel viewmodel = base.exportaViewModel(model);

            return viewmodel;
        }

        public override Aluno importaViewModel(AlunoViewModel viewmodel)
        {
            Aluno model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}
