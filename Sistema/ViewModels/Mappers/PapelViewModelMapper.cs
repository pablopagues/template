﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Base.GenericRepository;

namespace Sistema.ViewModels.Mappers
{

    public class PapelViewModelMapper : MapeadorViewModel<PapelViewModel, Papel>
    {
        private IGenericRepository genericRepositorio;

        public PapelViewModelMapper(IGenericRepository generic_Repository)
        {
            this.genericRepositorio = generic_Repository;
        }


        public override PapelViewModel exportaViewModel(Papel model)
        {
            PapelViewModel viewmodel = base.exportaViewModel(model);
            
            #region lista de seleção usuarios
            //criar lista de seleção de usuariso
            IList<SelectListItem> selectListItens = new List<SelectListItem>();
            
            List<Usuario> lst = null;
            lst = genericRepositorio.Tudo<Usuario>().ToList();

            foreach (Usuario item in lst)
            {
                bool selected = false;
                if (model.usuarios != null)
                    selected = model.usuarios.Where(x => x.id == item.id).Count() > 0;
                else
                    selected = false;

                selectListItens.Add(new SelectListItem
                {
                    Value = item.id.ToString(),
                    Selected = selected,
                    Text = item.professor.Nome
                });
            }
            #endregion
            viewmodel.usuariosList = selectListItens;

            return viewmodel;
        }

        public override Papel importaViewModel(PapelViewModel viewmodel)
        {
            Papel model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}