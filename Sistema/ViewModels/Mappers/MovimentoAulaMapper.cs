using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Base.GenericRepository;

namespace Sistema.ViewModels.Mappers
{

    public class MovimentoAulaViewModelMapper : MapeadorViewModel<MovimentoAulaViewModel, MovimentoAula>
    {
        private IGenericRepository genericRepositorio;

        public MovimentoAulaViewModelMapper(IGenericRepository generic_Repository)
        {
            this.genericRepositorio = generic_Repository;
        }

        public override MovimentoAulaViewModel exportaViewModel(MovimentoAula model)
        {
            MovimentoAulaViewModel viewmodel = base.exportaViewModel(model);

            viewmodel.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
            if (model.aluno != null)
                viewmodel.idAluno = model.aluno.id;

            viewmodel.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
            if (model.professor != null)
                viewmodel.idProfessor = model.professor.id;

            return viewmodel;
        }

        public override MovimentoAula importaViewModel(MovimentoAulaViewModel viewmodel)
        {
            MovimentoAula model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}
