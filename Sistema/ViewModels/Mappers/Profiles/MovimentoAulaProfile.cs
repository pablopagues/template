using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class MovimentoAulaProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MovimentoAula, MovimentoAulaViewModel>()
                .ForMember(d => d.Aluno, x => x.Ignore())
                .ForMember(d => d.Professor, x => x.Ignore());

            Mapper.CreateMap<MovimentoAulaViewModel, MovimentoAula>()
                .ForMember(d => d.aluno, x => x.Ignore())
                .ForMember(d => d.professor, x => x.Ignore());
        }
    }
}
