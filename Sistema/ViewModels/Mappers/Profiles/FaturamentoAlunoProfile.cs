using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class FaturamentoAlunoProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<FaturamentoAluno, FaturamentoAlunoViewModel>()
                .ForMember(d => d.Aluno, x => x.Ignore());

            Mapper.CreateMap<FaturamentoAlunoViewModel, FaturamentoAluno>()
                .ForMember(d => d.aluno, x => x.Ignore());
        }
    }
}
