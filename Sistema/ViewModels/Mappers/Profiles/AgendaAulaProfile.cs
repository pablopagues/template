using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class AgendaAulaProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AgendaAula, AgendaAulaViewModel>()
                .ForMember(d => d.Aluno, x => x.Ignore())
                .ForMember(d => d.Professor, x => x.Ignore());

            Mapper.CreateMap<AgendaAulaViewModel, AgendaAula>()
                .ForMember(d => d.aluno, x => x.Ignore())
                .ForMember(d => d.professor, x => x.Ignore());
        }
    }
}
