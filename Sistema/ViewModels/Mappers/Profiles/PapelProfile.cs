﻿using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class PapelProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Papel, PapelViewModel>()
                .ForMember(d => d.usuariosList, x => x.Ignore());

            Mapper.CreateMap<PapelViewModel, Papel>()
                .ForMember(d => d.usuarios, x => x.Ignore());
        }
    }
}