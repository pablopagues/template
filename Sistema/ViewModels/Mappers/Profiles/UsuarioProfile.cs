﻿using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class UsuarioProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Usuario, UsuarioViewModel>()
                .ForMember(d=>d.Nome, x=>x.MapFrom(s=>s.professor.Nome))
                .ForMember(d => d.email, x => x.MapFrom(s => s.professor.email))
                .ForMember(d => d.papeisList, x => x.Ignore());

            Mapper.CreateMap<UsuarioViewModel, Usuario>()
                .ForMember(d => d.papeis, x => x.Ignore());
        }
    }
}