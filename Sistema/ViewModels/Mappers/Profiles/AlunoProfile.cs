using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers.Profiles
{
    public class AlunoProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Aluno, AlunoViewModel>();

            Mapper.CreateMap<AlunoViewModel, Aluno>();
        }
    }
}
