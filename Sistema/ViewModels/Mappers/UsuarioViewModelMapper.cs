﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers
{

    public class UsuariosViewModelMapper : MapeadorViewModel<UsuarioViewModel, Usuario>
    {

        private IGenericRepository genericRepositorio;

        public UsuariosViewModelMapper(IGenericRepository genericRepositorio)
        {
            this.genericRepositorio = genericRepositorio;
        }

        public override UsuarioViewModel exportaViewModel(Usuario model)
        {
            UsuarioViewModel viewmodel = base.exportaViewModel(model);

            IList<SelectListItem> selectListItens = new List<SelectListItem>();
            List<Papel> lst = model.GetAllRoles().ToList();

            bool selected = false;
            foreach (Papel item in lst)
            {
                if (model.papeis != null)
                    selected = model.papeis.Where(x => x.id == item.id).Count() > 0;
                else
                    selected = false;

                selectListItens.Add(new SelectListItem
                {
                    Value = item.id.ToString(),
                    Selected = selected,
                    Text = item.nome
                });
                
            }

            viewmodel.papeisList = selectListItens;
            viewmodel.professorList = genericRepositorio.Tudo<Professor>().Select(p => new SelectListItem() { Text = p.Nome, Value = p.id.ToString() }).ToList();
            
            return viewmodel;
        }

        public override Usuario importaViewModel(UsuarioViewModel viewmodel)
        {
            Usuario model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}