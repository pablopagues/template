using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Models.Escola;

namespace Sistema.ViewModels.Mappers
{

    public class ProfessorViewModelMapper : MapeadorViewModel<ProfessorViewModel, Professor>
    {

        public override ProfessorViewModel exportaViewModel(Professor model)
        {
            ProfessorViewModel viewmodel = base.exportaViewModel(model);

            return viewmodel;
        }

        public override Professor importaViewModel(ProfessorViewModel viewmodel)
        {
            Professor model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}
