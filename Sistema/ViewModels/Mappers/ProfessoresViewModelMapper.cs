﻿using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Repositories.MappersRepository;
using Sistema.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sistema.ViewModels.Mappers
{
    public class ProfessoresViewModelMapper : MapeadorViewModel<ProfessorViewModel, Professor>
    {

        public override ProfessorViewModel exportaViewModel(Professor model)
        {
            ProfessorViewModel viewmodel = base.exportaViewModel(model);

            return viewmodel;
        }

        public override Professor importaViewModel(ProfessorViewModel viewmodel)
        {
            Professor model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}