using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.Domain.Repositories;
using Infrastructure.Domain.Models;
using Infrastructure.Domain.Repositories.MappersRepository;
using System.Web.Mvc;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Base.GenericRepository;

namespace Sistema.ViewModels.Mappers
{

    public class AgendaAulaViewModelMapper : MapeadorViewModel<AgendaAulaViewModel, AgendaAula>
    {
        private IGenericRepository genericRepositorio;

        public AgendaAulaViewModelMapper(IGenericRepository generic_Repository)
        {
            this.genericRepositorio = generic_Repository;
        }

        public override AgendaAulaViewModel exportaViewModel(AgendaAula model)
        {
            AgendaAulaViewModel viewmodel = base.exportaViewModel(model);

            viewmodel.Aluno = genericRepositorio.Tudo<Aluno>().Select(c => new SelectListItem() { Text = c.nome, Value = c.id.ToString() }).ToList();
            if (model.aluno != null)
                viewmodel.idAluno = model.aluno.id;

            viewmodel.Professor = genericRepositorio.Tudo<Professor>().Select(c => new SelectListItem() { Text = c.Nome, Value = c.id.ToString() }).ToList();
            if (model.professor != null)
                viewmodel.idProfessor = model.professor.id;

            return viewmodel;
        }

        public override AgendaAula importaViewModel(AgendaAulaViewModel viewmodel)
        {
            AgendaAula model = base.importaViewModel(viewmodel);
            return model;
        }
    }
}
