using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sistema.ViewModels
{
    public class ProfessorViewModel
    {
        public int id { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }
        [DisplayName("Endereco")]
        public string Endereco { get; set; }
        [DisplayName("Telefone")]
        public string Telefone { get; set; }
        [DisplayName("Celular")]
        public string Celular { get; set; }
        [Required]
        [DisplayName("DataNascimento")]
        public DateTime DataNascimento { get; set; }
        [DisplayName("ValorAula")]
        public double ValorAula { get; set; }
        [DisplayName("email")]
        public string email { get; set; }
        [DisplayName("Login")]
        public String Login { get; set; }

    }
}

