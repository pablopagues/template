using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Sistema.ViewModels
{
    public class AgendaAulaViewModel
    {
        public int id { get; set; }

        public int idAluno { get; set; }
        [DisplayName("Aluno")]
        public IList<SelectListItem> Aluno { get; set; }
        [DisplayName("Descri��o")]
        public String Descricao { get; set; }

        [DisplayName("Hora")]
        public String Hora { get; set; }

        public int idProfessor { get; set; }
        [DisplayName("Professor")]
        public IList<SelectListItem> Professor { get; set; }
        [DisplayName("Data")]
        public DateTime Data { get; set; }

    }
}

