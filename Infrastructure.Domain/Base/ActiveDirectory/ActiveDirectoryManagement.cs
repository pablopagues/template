﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;

namespace Infrastructure.Domain.Base.ActiveDirectory
{
    public class UsuarioRede
    {
        public string nome { get; set; }
        public string email { get; set; }
        public string login { get; set; }
    }

    public class ActiveDirectoryManagement
    {
        private PrincipalContext AD;
        private UserPrincipal user;

        public ActiveDirectoryManagement()
        {
            AD = new PrincipalContext(ContextType.Domain);
            user = new UserPrincipal(AD);
        }

        public IList<IDictionary<string, string>> GetAllUsers(string displayname)
        {
            try
            {
                IList<IDictionary<string, string>> users = new List<IDictionary<string, string>>();

                using (PrincipalSearcher search = new PrincipalSearcher(user))
                {
                    foreach (UserPrincipal result in search.FindAll())
                    {
                        if (result.DisplayName != null && result.DisplayName.StartsWith(displayname, StringComparison.OrdinalIgnoreCase))
                        {
                            IDictionary<string, string> searched = new Dictionary<string, string>();
                            searched.Add("id", result.Sid.ToString());
                            searched.Add("login", result.SamAccountName.ToLower());
                            searched.Add("nome", result.DisplayName);
                            searched.Add("email", result.EmailAddress != null ? result.EmailAddress.ToLower() : null);

                            users.Add(searched);
                        }
                    }
                }

                return users;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// retornar lista de usuários de rede
        /// </summary>
        /// <returns></returns>
        public List<UsuarioRede> GetAllUsers()
        {
            try
            {
                List<UsuarioRede> usuarios = new List<UsuarioRede>();

                using (PrincipalSearcher search = new PrincipalSearcher(user))
                {
                    foreach (UserPrincipal result in search.FindAll())
                    {
                        if (result.DisplayName != null)
                        {
                            UsuarioRede usuario = new UsuarioRede();
                            usuario.login = result.SamAccountName;
                            usuario.nome = result.DisplayName;
                            usuario.email = result.EmailAddress;

                            usuarios.Add(usuario);
                        }
                    }
                }
                return usuarios;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetDisplayName(string samAccountName)
        {
            try
            {
                user.SamAccountName = samAccountName;

                string displayName = "";

                using (PrincipalSearcher search = new PrincipalSearcher(user))
                {
                    UserPrincipal result = (UserPrincipal)search.FindOne();
                    displayName = result.DisplayName;
                }

                return displayName;
            }
            catch(NullReferenceException)
            {
                return null;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetEmailAddress(string samAccountName)
        {
            try
            {
                user.SamAccountName = samAccountName;

                string emailAddress = "";

                using (PrincipalSearcher search = new PrincipalSearcher(user))
                {
                    UserPrincipal result = (UserPrincipal)search.FindOne();
                    emailAddress = result.EmailAddress;
                }

                return emailAddress;
            }
            catch (NullReferenceException)
            {
              return null;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public bool ValidateUser(string samAccountName, string password)
        {
            try
            {
                return AD.ValidateCredentials(samAccountName, password);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
