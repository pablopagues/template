﻿using Infrastructure.Domain.Contratos.Specification;
using Infrastructure.Domain.Contratos.UnityOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Domain.Base.GenericRepository
{
    public enum SortOrder { Ascending, Descending }

    public interface IGenericRepository
    {        
        /// <summary>
        /// Realiza a busca pelo ID
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="keyValue">Valor da Chave</param>
        /// <returns></returns>
        TEntity EncontrarPorID<TEntity>(object keyValue) where TEntity : class;

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <returns></returns>
        IQueryable<TEntity> EncontrarPor<TEntity>() where TEntity : class;

        /// <summary>
        /// Recupera registros baseado na expressão linq passada
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="expressao">Expressão Linq</param>
        /// <returns></returns>
        IQueryable<TEntity> EncontrarPor<TEntity>(Expression<Func<TEntity, bool>> expressao) where TEntity : class;

        /// <summary>
        /// Recupera registros baseado na especificação passada
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="criterio">Specification</param>
        /// <returns></returns>
        IQueryable<TEntity> EncontrarPor<TEntity>(ISpecification<TEntity> criterio) where TEntity : class;

        /// <summary>
        /// Recupera registros incluindo suas referencias por eager loading.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="includes"></param>
        /// <returns></returns>
        IList<TEntity> EncontrarPor<TEntity>(params Expression<Func<TEntity, object>>[] includes) where TEntity : class;

        /// <summary>
        /// Recupera os registros paginando de acordo com a ordenação especificada
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <typeparam name="TOrderBy">Tipo da Ordenação</typeparam>
        /// <param name="orderBy">Expressão de Ordenação</param>
        /// <param name="pageIndex">Índice da Página</param>
        /// <param name="pageSize">Tamanho da Página</param>
        /// <param name="sortOrder">Modo de Ordenação</param>
        /// <returns></returns>
        IEnumerable<TEntity> OrdenarPor<TEntity, TOrderBy>(Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = SortOrder.Ascending) where TEntity : class;

        /// <summary>
        /// Recupera os registros paginando de acordo com o critério estabelecido
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <typeparam name="TOrderBy">Tipo da Ordenação</typeparam>
        /// <param name="expressao">Expressão Linq</param>
        /// <param name="orderBy">Expressão de Ordenação</param>
        /// <param name="pageIndex">Índice da Página</param>
        /// <param name="pageSize">Tamanho da Página</param>
        /// <param name="sortOrder">Modo de Ordenação</param>
        /// <returns></returns>
        IEnumerable<TEntity> OrdenarPor<TEntity, TOrderBy>(Expression<Func<TEntity, bool>> expressao, Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = SortOrder.Ascending) where TEntity : class;

        /// <summary>
        /// Recupera os registros paginando de acordo com a especificação
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <typeparam name="TOrderBy">Tipo da Ordenação</typeparam>
        /// <param name="specification">Specification</param>
        /// <param name="orderBy">Expressão de Ordenação</param>
        /// <param name="pageIndex">Índice da Página</param>
        /// <param name="pageSize">Tamanho da Página</param>
        /// <param name="sortOrder">Modo de Ordenação</param>
        /// <returns></returns>
        IEnumerable<TEntity> OrdenarPor<TEntity, TOrderBy>(ISpecification<TEntity> specification, Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = SortOrder.Ascending) where TEntity : class;

        /// <summary>
        /// Gets one entity based on matching criteria
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        TEntity Single<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;

        /// <summary>
        /// Gets single entity using specification
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        TEntity Single<TEntity>(ISpecification<TEntity> criteria) where TEntity : class;

        /// <summary>
        /// Firsts the specified predicate.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        TEntity First<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;

        /// <summary>
        /// Gets first entity with specification.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        TEntity First<TEntity>(ISpecification<TEntity> criteria) where TEntity : class;

        /// <summary>
        /// Finds entities based on provided criteria.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        IEnumerable<TEntity> Find<TEntity>(ISpecification<TEntity> criteria) where TEntity : class;

        /// <summary>
        /// Finds entities based on provided criteria.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        IEnumerable<TEntity> Find<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;

        /// <summary>
        /// Finds one entity based on provided criteria.
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="specification">Specification</param>
        /// <returns></returns>
        TEntity FindOne<TEntity>(ISpecification<TEntity> specification) where TEntity : class;

        /// <summary>
        /// Finds one entity based on provided criteria.
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="expressao">Expressão Linq</param>
        /// <returns></returns>
        TEntity FindOne<TEntity>(Expression<Func<TEntity, bool>> expressao) where TEntity : class;

        /// <summary>
        /// Recupera todos os registros da entidade especificada
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        IEnumerable<TEntity> Tudo<TEntity>() where TEntity : class;

        IEnumerable<TEntity> Tudo<TEntity>(params Expression<Func<TEntity, object>>[] includes) where TEntity : class;

        /// <summary>
        /// Recupera todos os registros da entidade especificada, usada nos DataTables
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        //IQueryable<TEntity> GetDataForDataTable<TEntity>() where TEntity : class;

        /// <summary>
        /// Retorna a contagem de registros
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <returns>Integer</returns>
        int Count<TEntity>() where TEntity : class;

        /// <summary>
        /// Retorna a contagem de registros baseado no critério estabelecido
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="expressao">Expressão Linq</param>
        /// <returns>Integer</returns>
        int Count<TEntity>(Expression<Func<TEntity, bool>> expressao) where TEntity : class;

        /// <summary>
        /// Retorna a contagem de registros baseada na especificação
        /// </summary>
        /// <typeparam name="TEntity">Tipo da Entidade</typeparam>
        /// <param name="criterio">Specification para realizar a busca</param>
        /// <returns>Integer</returns>
        int Count<TEntity>(ISpecification<TEntity> criterio) where TEntity : class;


        /* ----------------------------- */
        /* ------ CRUD OPERATIONS ------ */
        /* ----------------------------- */

        /// <summary>
        /// Adiciona o registro ao contexto
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="entidade">entity</param>
        void Adicionar<TEntity>(TEntity entidade) where TEntity : class;

        /// <summary>
        /// Anexa o registro ao contexto
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="entidade">entity</param>
        void Anexar<TEntity>(TEntity entidade) where TEntity : class;

        /// <summary>
        /// Atualiza as mudanças do registro atual.
        /// Após utilizar este método é necessário chamar o Salvar() para concluir o processo
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="entidade">entity</param>
        void Atualizar<TEntity>(TEntity entidade) where TEntity : class;

        /// <summary>
        /// Deleta o registro do contexto
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="entidade">entity</param>
        void Deletar<TEntity>(TEntity entidade) where TEntity : class;

        /// <summary>
        /// Deleta um ou mais registros baseado na expressão de seleção
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="expressao">Expressão Linq</param>
        void Deletar<TEntity>(Expression<Func<TEntity, bool>> expressao) where TEntity : class;

        /// <summary>
        /// Deleta os registros que satisfazem a especificação
        /// </summary>
        /// <typeparam name="TEntity">Tipo da entidade</typeparam>
        /// <param name="criterio">Specification para deletar</param>
        void Deletar<TEntity>(ISpecification<TEntity> criterio) where TEntity : class;

        /// <summary>
        /// Comita todas as operações realizadas
        /// </summary>
        void Salvar();

        /// <summary>
        /// Recupera a entidade UnitOfWork
        /// </summary>
        IUnitOfWork UnitOfWork { get; }
    }   
}
