﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Domain.Repositories.MappersRepository
{
  /// <summary>
  /// Realiza o mapeamento de entidades utilizando o Automapper.
  /// </summary>
  /// <typeparam name="TViewModel"></typeparam>
  /// <typeparam name="TModel"></typeparam>
  public interface IMapeadorViewModel<TViewModel, TModel>
    where TViewModel: class
    where TModel: class
  {
    /// <summary>
    /// Copia o conteúdo de um ViewModel para um Modelo.
    /// </summary>
    /// <param name="viewmodel">ViewModel origem</param>
    /// <param name="modelExistente">Modelo existente que receberá os dados do viewmodel</param>
    /// <returns>Modelo destino</returns>
      TModel importaViewModel(TViewModel viewmodel, TModel modelExistente);

    /// <summary>
    /// Copia conteúdo de um ViewModel para uma nova instância de um Modelo.
    /// </summary>
    /// <param name="viewmodel">ViewModel recebido da view</param>
    /// <returns>Modelo do domínio</returns>
    TModel importaViewModel(TViewModel viewmodel);

    /// <summary>
    /// Mapeia uma instancia existente de um Model para um ViewModel respeitando
    /// relacionamentos e agregações.
    /// </summary>
    /// <param name="model">Modelo do domínio</param>
    /// <returns>Viewmodel repassado para a view</returns>
    TViewModel exportaViewModel(TModel model, TViewModel viewModelExistente);

    /// <summary>
    /// Mapeia uma instancia existente de um Model para um ViewModel respeitando
    /// relacionamentos e agregações.
    /// </summary>
    /// <param name="model">Modelo do domínio</param>
    /// <returns>Viewmodel repassado para a view</returns>
    TViewModel exportaViewModel(TModel model);
  }
}
