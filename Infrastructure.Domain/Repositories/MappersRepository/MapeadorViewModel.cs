﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;

namespace Infrastructure.Domain.Repositories.MappersRepository
{
  /// <summary>
  /// Realiza o mapeamento de entidades utilizando o Automapper.
  /// </summary>
  /// <typeparam name="TViewModel"></typeparam>
  /// <typeparam name="TModel"></typeparam>
  public abstract class MapeadorViewModel<TViewModel, TModel> : IMapeadorViewModel<TViewModel, TModel>
    where TViewModel : class
    where TModel : class
  {
    #region IViewModelMapper<TViewModel,TModel> Members

    /// <summary>
    /// Copia o conteúdo de um ViewModel para um Modelo.
    /// </summary>
    /// <param name="viewmodel">ViewModel origem</param>
    /// <param name="modelExistente">Modelo existente que receberá os dados do viewmodel</param>
    /// <returns>Modelo destino</returns>
    public virtual TModel importaViewModel(TViewModel viewmodel, TModel modelExistente)
    {
      return Mapper.Map<TViewModel, TModel>(viewmodel, modelExistente);
    }

    /// <summary>
    /// Copia o conteúdo de um ViewModel para um Modelo.
    /// </summary>
    /// <param name="viewmodel">ViewModel origem</param>
    /// <returns>Modelo destino</returns>
    public virtual TModel importaViewModel(TViewModel viewmodel)
    {
      return Mapper.Map<TViewModel, TModel>(viewmodel);
    }

    /// <summary>
    /// Mapeia uma instancia existente de um Model para um ViewModel respeitando
    /// relacionamentos e agregações.
    /// </summary>
    /// <param name="model">Modelo do domínio</param>
    /// <param name="viewModelExistente">Viewmodel existente que receberá os dados do modelo</param>
    /// <returns>Viewmodel repassado para a view</returns>
    public virtual TViewModel exportaViewModel(TModel model, TViewModel viewModelExistente)
    {
      return Mapper.Map<TModel, TViewModel>(model, viewModelExistente);
    }

    /// <summary>
    /// Copia conteúdo de um Modelo para um ViewModel.
    /// </summary>
    /// <param name="model">Modelo origem</param>
    /// <returns>ViewModel destino</returns>
    public virtual TViewModel exportaViewModel(TModel model)
    { 
      return Mapper.Map<TModel, TViewModel>(model);
    }

    #endregion
  }
}
