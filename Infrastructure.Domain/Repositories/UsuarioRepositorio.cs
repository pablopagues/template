﻿using Infrastructure.Domain.Base;
using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Base.ActiveDirectory;
using Infrastructure.Domain.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Service.Security;

namespace Infrastructure.Domain.Repositories
{
    public interface IUsuarioRepositorio
    {
        Usuario encontrarPorLogin(string login);
        Usuario gerarUsuarioPadrao(string login);
        bool validateUser(string username, string password);
        //string CriptografiaMD5(string Valor);
    }

    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        private IGenericRepository genericRepository;

        public UsuarioRepositorio(IGenericRepository pgenericRepository)
        {
            this.genericRepository = pgenericRepository;
        }

        public Usuario encontrarPorLogin(string login)
        {
            return genericRepository.EncontrarPor<Usuario>().Where(c => c.login == login).FirstOrDefault();
        }

        public Usuario encontrarPorLogin(string login, string senha)
        {
            //senha = this.CriptografiaMD5(senha);
            senha = Criptography.HashCode(senha);
            return genericRepository.EncontrarPor<Usuario>().Where(c => c.login == login).Where(p=>p.Senha == senha).FirstOrDefault();
        }

        public bool validateUser(string username, string password)
        {
            
            Usuario usuario = encontrarPorLogin(username, password);

            //usuário autenticado
            if (usuario != null) 
            {
                if (usuario.estaBloqueado)
                {
                    throw new Exception("Este usuário está Bloqueado!");
                }
                else
                {
                    return true;
                }
            }
            else
            {
                //este codigo se executa somente uma vez quando recria o banco
                var qtdusuarios = genericRepository.Tudo<Usuario>().Count();
                if (qtdusuarios == 0)
                {
                    Professor prof = new Professor();
                    prof.ValorAula = 0;
                    prof.DataNascimento = DateTime.Now;
                    prof.email = username + "@gmail.com";
                    prof.Nome = username;
                    prof.Login = username;
                    genericRepository.Adicionar<Professor>(prof);

                    //adicionar novo usuário
                    Usuario user = gerarUsuarioPadrao(username);
                    genericRepository.Adicionar(user);
                    genericRepository.Salvar();
                    return true;
                }

                return false;

                throw new Exception("teste.");
            }
        }

        public Usuario gerarUsuarioPadrao(string login)
        {
            return new Usuario(genericRepository)
            {
                login = login,
                Senha = Criptography.HashCode("123456"),
                dataCriacao = DateTime.Now,
                ultimoLogin = DateTime.Now,
                estaBloqueado = false,
                estaOnline = true,
                professor = genericRepository.Tudo<Professor>().Where(x=>x.Login == login).FirstOrDefault()
            };
        }
    }
}
