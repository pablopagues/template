﻿using Infrastructure.Domain.Base.GenericRepository;
using Infrastructure.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;

namespace Infrastructure.Domain.Repositories
{
    public interface IPapelRepositorio
    {
        Papel encontrarPorId(int id);

        IQueryable<Papel> GetTudo();

        List<Papel> encontrarPorVariosIds(int[] ids);
    }

    public class PapelRepositorio : IPapelRepositorio
    {
        private IGenericRepository genericRepository;

        public PapelRepositorio(IGenericRepository pgenericRepository)
        {
            this.genericRepository = pgenericRepository;
        }

        public Papel encontrarPorId(int id)
        {
            return genericRepository.EncontrarPor<Papel>(c=>c.usuarios).Where(c => c.id == id).FirstOrDefault();
        }

        public IQueryable<Papel> GetTudo()
        {
            var lstPapel = genericRepository.Tudo<Papel>().AsQueryable();
            return lstPapel;
        }

        public List<Papel> encontrarPorVariosIds(int[] ids)
        {
            return genericRepository.EncontrarPor<Papel>(x => ids.Contains(x.id)).ToList();
        }
    }
}
