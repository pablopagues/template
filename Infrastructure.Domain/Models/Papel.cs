﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Domain.Models.Contracts;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Domain.Models
{
    public class Papel : PrimaryKey
    {
        [Required]
        [DisplayName("Nome do Papel")]
        public virtual string nome { get; set; }

        [DisplayName("Usuários")]
        public virtual ICollection<Usuario> usuarios { get; set; }
    }
}
