﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Infrastructure.Domain.Models.Contracts;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Web.Security;
using System.Web;
using AutoMapper;
using Infrastructure.Domain.Models.Escola;
using Infrastructure.Domain.Base.GenericRepository;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure.Domain.Models
{
    public class Usuario : PrimaryKey, IIdentity, IPrincipal
    {
        [IgnoreMapAttribute]
        private IGenericRepository genericRepositorio;

        public Usuario()
        {
                
        }

        public Usuario(IGenericRepository genericRepository)
        {
            //para os casos onde é impossível passar o ticket para o construtor
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                string encTicket = authCookie.Value;
                if (!String.IsNullOrEmpty(encTicket))
                {
                    var ticket = FormsAuthentication.Decrypt(encTicket);
                    this._ticket = ticket;
                }
            }

            this.genericRepositorio = genericRepository;
        }

        public Usuario(FormsAuthenticationTicket ticket)
        {

            _ticket = ticket;
        }

        [Required]
        [DisplayName("Nome de Login")]
        public virtual string login { get; set; }

        [Required]
        [DisplayName("Senha")]
        public virtual String Senha { get; set; }

        [DisplayName("Último Login")]
        public virtual DateTime ultimoLogin { get; set; }

        [DisplayName("Data de Criação")]
        public virtual DateTime dataCriacao { get; set; }

        [DisplayName("Está Online?")]
        public virtual bool estaOnline { get; set; }

        [DisplayName("Está Bloqueado?")]
        public virtual bool estaBloqueado { get; set; }

        [DisplayName("Papeis")]
        public virtual ICollection<Papel> papeis 
        { 
            get; set; 
        }

        [IgnoreMapAttribute]
        public virtual string AuthenticationType
        {
            get { return "User"; }
        }

        [IgnoreMapAttribute]
        public virtual bool IsAuthenticated
        {
            get { return true; }
        }

        [IgnoreMapAttribute]
        public virtual string Name
        {
            get { return _ticket.Name; }
        }

        [IgnoreMapAttribute]
        public virtual IIdentity Identity
        {
            get { return this; }
        }

        public virtual bool IsInRole(string role)
        {
            return Roles.IsUserInRole(role);
        }

        public virtual IList<Papel> GetAllRoles()
        {
            string[] papeis = Roles.GetAllRoles();
            //using (var usersContext = new MVCContext())
            //{
                //return usersContext.Papeis.Include("usuarios").Where(x => papeis.Contains(x.nome)).ToList();
                return genericRepositorio.Tudo<Papel>(p => p.usuarios).Where(x => papeis.Contains(x.nome)).ToList();
            //}
        }

        //public int professorid { get; set; }

        public virtual Professor professor { get; set; }

        [IgnoreMapAttribute]
        private readonly FormsAuthenticationTicket _ticket;
        
    }
}
