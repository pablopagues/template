﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Domain.Models.Contracts;

namespace Infrastructure.Domain.Models.Escola
{
    public class AgendaAula : PrimaryKey
    {
        public virtual Aluno aluno { get; set; }
        public String Descricao { get; set; }
        public DateTime Hora { get; set; }

        public Professor professor { get; set; }
        public DateTime Data { get; set; }

    }
}
