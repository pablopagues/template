﻿using Infrastructure.Domain.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Models.Escola
{
    public class FaturamentoAluno : PrimaryKey
    {
        public virtual int IdAluno { get; set; }
        public virtual Aluno aluno { get; set; }
        public virtual String Descricao { get; set; }
        public virtual decimal QtdHoras { get; set; }
        public virtual decimal ValorHora { get; set; }
        public virtual DateTime Data { get; set; }
    }
}
