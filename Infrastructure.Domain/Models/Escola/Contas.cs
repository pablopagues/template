﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Domain.Models.Contracts;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Domain.Models.Escola
{
    public class TipoDespesa : PrimaryKey
    {
        public string Nome { get; set; }
    }

    public class ContasPagar : PrimaryKey
    {
        public String Descricao { get; set; }
        public double Montante { get; set; }
        public string TipoMovimento { get; set; }
        public DateTime Data { get; set; }

    }

    public class ContasReceber : PrimaryKey
    {
        public String Descricao { get; set; }
        public double Montante { get; set; }
        public string TipoMovimento { get; set; }
        public DateTime Data { get; set; }

        public virtual Aluno aluno { get; set; }
    }

}
