﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Domain.Models.Contracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Infrastructure.Domain.Models.Escola
{
    public class Professor : PrimaryKey
    {
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public DateTime DataNascimento { get; set; }
        public double ValorAula { get; set; }
        public string email { get; set; }
        public String Login { get; set; }

        //[Key, ForeignKey("Usuario")]
        //public int usuarioid { get; set; }

        //public virtual Usuario Usuario { get; set; }

    }
}
