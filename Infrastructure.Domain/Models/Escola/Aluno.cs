﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Infrastructure.Domain.Models.Contracts;

namespace Infrastructure.Domain.Models.Escola
{
    public class Aluno : PrimaryKey
    {
        [Required(ErrorMessage = "Campo não pode ser vazio.")]
        [DisplayName("Nome")]
        public virtual string nome { get; set; }

        [DisplayName("Nome Pai")]
        public virtual string nomePai { get; set; }

        [DisplayName("Nome Mãe")]
        public virtual string nomeMae { get; set; }

        [DisplayName("Endereço")]
        public virtual string endereco { get; set; }

        [DisplayName("Telefone")]
        public virtual string telefone { get; set; }

        [DisplayName("Celular")]
        public virtual string celular { get; set; }

        [DisplayName("Dt Inicio Aulas")]
        public virtual DateTime dtInicioAulas { get; set; }

        [DisplayName("Dt Venc. Aulas")]
        public virtual DateTime dtVencAulas { get; set; }

        [DisplayName("Dt Nascimento")]
        public virtual DateTime dtNascimento { get; set; }

        [DisplayName("Disciplinas")]
        public virtual string disciplinas { get; set; }

        [DisplayName("Serie")]
        public virtual string serie { get; set; }

        [DisplayName("Email")]
        public virtual string email { get; set; }

        [DisplayName("Celular Pai")]
        public virtual string celularPai { get; set; }

        [DisplayName("Celular Mãe")]
        public virtual string celularMae { get; set; }

        [DisplayName("Escola")]
        public virtual string escola { get; set; }

        [DisplayName("Observação")]
        public virtual string observacao { get; set; }
    }
}
