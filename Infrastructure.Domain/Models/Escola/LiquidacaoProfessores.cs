﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Domain.Models.Contracts;

namespace Infrastructure.Domain.Models.Escola
{
    public class LiquidacaoProfessores
    {
        public Int64 ID { get; set; }
        public int professor_id { get; set; }
        public String nome { get; set; }
        public DateTime datamovimento { get; set; }
        public int qtdalunos { get; set; }
        public double vlhora { get; set; }
        public decimal horasreais { get; set; }
        public string TimeDiff { get; set; }
        public DateTime h1 { get; set; }
        public DateTime h2 { get; set; }
        public double horasgrupo { get; set; }
        public int minutosgrupo { get; set; }
        public double valorpagar { get; set; }
        public int grupo { get; set; }
    }
}
