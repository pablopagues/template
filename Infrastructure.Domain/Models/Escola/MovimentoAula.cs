﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Domain.Models.Contracts;

namespace Infrastructure.Domain.Models.Escola
{
    public class MovimentoAula : PrimaryKey
    {
        public virtual Aluno aluno { get; set; }
        public String Descricao { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFinal { get; set; }
        public decimal QtdHoras { get; set; }
        public Professor professor { get; set; }
        public int Grupo { get; set; }
        public DateTime DataMovimento { get; set; }

    }
}
