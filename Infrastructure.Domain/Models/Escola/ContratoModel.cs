﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Domain.Models.Contracts;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Domain.Models.Escola
{
    public class Contrato : PrimaryKey
    {
        public string Nome { get; set; }
        public double Valor { get; set; }
        public double ValorReforço { get; set; }
        public double ValorParticular { get; set; }
    }
}
