﻿using Infrastructure.Domain.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Domain.Models.Escola
{
    public class Parametros : PrimaryKey
    {
        public String Nome { get; set; }
        public double Valor { get; set; }
    }
}
