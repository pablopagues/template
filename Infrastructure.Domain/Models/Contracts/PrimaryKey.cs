﻿using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace Infrastructure.Domain.Models.Contracts
{
    public abstract class PrimaryKey
    {
        [HiddenInput]
        [DisplayName("ID")]
        public virtual int id { get; set; }
    }
}
